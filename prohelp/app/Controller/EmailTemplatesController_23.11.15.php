<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class EmailTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');




	public function admin_index() {		
		$title_for_layout = 'EmailTemplate List';
		 $this->paginate = array(
			'order' => array(
				'EmailTemplate.id' => 'asc'
			)
		);
		$this->EmailTemplate->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('emailtemplate', $this->Paginator->paginate());
		$this->set(compact('title_for_layout'));
	}

	public function admin_edit($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid Email Template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
                              $this->Session->setFlash('The email template has been saved.', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
	}

}
