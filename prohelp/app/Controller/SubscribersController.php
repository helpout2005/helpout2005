<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SubscribersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
   public $components = array('Paginator');
   public $uses = array('Subscriber','Group');
	public function admin_index() {		
		$title_for_layout = 'Subscribers List';
		 $this->paginate = array(
			'order' => array(
				'Subscriber.id' => 'asc'
			)
		);
		$this->Subscriber->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('Subscribers', $this->Paginator->paginate());
		$this->set(compact('title_for_layout'));
	}

	public function admin_edit($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->Subscriber->exists($id)) {
			throw new NotFoundException(__('Invalid Email Template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Subscriber->save($this->request->data)) {
                              $this->Session->setFlash('The Subscriber has been saved.', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash(__('The Subscriber could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Subscriber.' . $this->Subscriber->primaryKey => $id));
			$this->request->data = $this->Subscriber->find('first', $options);
		}
		$groups = $this->Group->find('list',array('conditions' => array('Group.is_active' => '1')));
		$this->set(compact('groups'));
	}
public function admin_add() {			
		$title_for_layout = 'Subscribers Add';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if ($this->request->is('post')) {
			$options = array('conditions' => array('Subscriber.email'  => $this->request->data['Subscriber']['email']));
			$name = $this->Subscriber->find('first', $options);
			
				if ($this->Subscriber->save($this->request->data)) {
					$this->Session->setFlash('The Subscriber has been saved.', 'default', array('class' => 'success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Subscriber could not be saved. Please, try again.'));
				}

			} else {
				$this->Session->setFlash(__('The Subscriber name already exists. Please, try again.'));
			}
			
			$groups = $this->Group->find('list',array('conditions' => array('Group.is_active' => '1')));
			//pr($groups);
		//}
		$this->set(compact('title_for_layout','groups'));
	}
	public function admin_delete($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Subscriber->id = $id;
		if (!$this->Subscriber->exists()) {
			throw new NotFoundException(__('Invalid Subscriber'));
		}
		
	
		if ($this->Subscriber->delete($id)) {
			$this->Session->setFlash('The Subscriber has been deleted.', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The Subscriber could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

}
