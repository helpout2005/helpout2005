<div class="users index">
<div class="span9" id="content">
	<div class="row-fluid">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left"><h3><?php if ($this->request->data['Category']['parent_id']!=0) {   echo 'Edit Sub category';  } else { echo __('Edit Category'); } ?></h3></div>
			</div>
                       <?php echo $this->Form->create('Category', array('type' => 'file')); ?>
			<fieldset>
			   <?php
				echo $this->Form->input('id',array('type' => 'hidden'));
				echo $this->Form->input('name',array('required'=>'required'));
				if ($this->request->data['Category']['parent_id']==0)
			        {
				  echo $this->Form->input('new_category_image', array('type' => 'file'));
				  echo $this->Form->input('hid_category_image',array('type' => 'hidden','value'=>$this->request->data['Category']['category_image']));
				}
				echo $this->Form->input('is_active', array('type'=>'checkbox'));
				echo $this->Form->input('parent_id',array('type' => 'hidden'));        
			   ?>
			</fieldset>
			<?php echo $this->Form->end(__('Submit')); ?>
		</div>
	</div>
</div>
</div>
<?php echo $this->element('admin_sidebar');?>
