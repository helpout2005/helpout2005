<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          Users
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <!--<li>
                <a href="#">Metro Lab</a>
                <span class="divider">/</span>
            </li>-->
            <li class="active">
                Users
            </li>
            <!--<li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>-->
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    <!-- BEGIN EXAMPLE TABLE widget-->
    <div class="widget red">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i> Users</h4>
                <!--<span class="tools">
                    <?php echo $this->Html->link(__('Add New User'), array('controller' => 'users', 'action' => 'add')); ?>
                </span>-->
        </div>
        <div class="widget-body">
            <table class="table table-striped table-bordered" id="sample_1">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('username'); ?></th>
                    <th><?php echo $this->Paginator->sort('email'); ?></th>
                    <!--<th><?php echo $this->Paginator->sort('user_image'); ?></th>-->
                    <th><?php echo $this->Paginator->sort('login_status'); ?></th>
                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                    <th><?php echo $this->Paginator->sort('last_login'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php 
	#pr($users);                    
                    foreach ($users as $user):?>
                        <tr>
                            <td>
                            <?php if(isset($user['User']['name']) && $user['User']['name']!='') {echo h($user['User']['name']);} ?>
                            </td>
                            <td>
                            <?php if(isset($user['User']['username']) && $user['User']['username']!='') {echo h($user['User']['username']);} ?>
                            </td>
                            <td>
                            <?php if(isset($user['User']['email']) && $user['User']['email']!='') {echo h($user['User']['email']);} ?>
                            </td>
                            <td>
                            <?php if(isset($user['User']['login_status']) && $user['User']['login_status']!='') {if($user['User']['login_status']=='0' || $user['User']['login_status']=='4'){ ?><img src="<?php echo $this->webroot;?>img/offline.png" title="offline" width="10" height="12">&nbsp;OFFLINE<?php } if($user['User']['login_status']=='2') { ?><img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;ONLINE <?php } if($user['User']['login_status']=='3') {?><img src="<?php echo $this->webroot;?>img/away.png" title="online" width="10" height="12">&nbsp;AWAY <?php } if($user['User']['login_status']=='1'){?><img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;INSTANT SESSION<?php }}?>
                            </td>
                            <td><?php echo h($user['User']['is_active']==1?'Yes':'No'); ?>&nbsp;</td>
                            <td>
                            <?php if(isset($user['User']['last_login']) && $user['User']['last_login']!='') {echo $user['User']['last_login'].'<br>'.$user['User']['ip'];}?>
                            </td>
                            <td class="actions actionbtn">
                            <!--<a href="<?php echo $this->webroot;?>admin/users/addsubcategory/<?php echo $user['User']['id'];?>"><img src="<?php echo $this->webroot;?>img/subcat_add.png" title="Add Sub User" width="22" height="21"></a>-->

                             <a href="<?php echo $this->webroot;?>admin/users/edit/<?php echo $user['User']['id'];?>"><img src="<?php echo $this->webroot;?>img/edit.png" title="Edit User" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/users/delete/<?php echo $user['User']['id'];?>" onclick="return confirm('Are you sure to delete this user?')"><img src="<?php echo $this->webroot;?>img/delete.png" title="Delete User" width="24" height="24"></a>
                            </td>
                        </tr>
                   
                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul>
<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
