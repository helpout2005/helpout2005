var prohelpApp = angular.module("prohelp", ["ngRoute", "ngAnimate", "myUtils", "ngCookies", "ngSanitize", "ngTagsInput", "angular-ladda"]), mycontrollers = {};
prohelpApp.config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(false);
    $routeProvider
            .when("/", {controller: "homeController", templateUrl: "app/webroot/ng/partials/home.html"})
            .when("/account-activation/:id", {controller: "activationController", templateUrl: "app/webroot/ng/partials/account-activation.html"})
            .when("/dashboard", {controller: "dashboardController", templateUrl: "app/webroot/ng/partials/dashboard.html"})
            .when("/reset-password/:id", {controller: "resetController", templateUrl: "app/webroot/ng/partials/reset-password.html"})
            .otherwise({redirectTo: "/"});
});

/*==============================Header Controller========================================*/

mycontrollers.headerController = function($scope, $http, $location, myAuth, $sce, $cookies, $cookieStore) {
    $scope.siteurl = myAuth.baseurl;
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
    $scope.$on("update_parent_controller", function(event, message) {
        $scope.loggedindetails = message;
        if ($scope.loggedindetails)
        {
            $scope.loggedin = true;
            $scope.notloggedin = false;
        }
        else
        {
            $scope.loggedin = false;
            $scope.notloggedin = true;
        }
    });
    if ($scope.loggedindetails)
    {
        $scope.loggedin = true;
        $scope.notloggedin = false;
    }
    else
    {
        $scope.loggedin = false;
        $scope.notloggedin = true;
    }

    $scope.showdropdownmenu = function() {
        $('.dropdown-menu').toggle();
    };

    $scope.openModal = function(divId) {
        $('.modal').modal('hide');
        setTimeout(function()
        {
            $('#' + divId).modal('show');
        }, 500);
    }

    $scope.login = function() {
$cookieStore.put('isactivation', 1);
        if ($scope.user_username == '' || $scope.user_username === undefined || $scope.user_password == '' || $scope.user_password === undefined)
        {
            $scope.loginalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your login details.');
        }
        else
        {
            $scope.loggedindetails = '';
            $scope.loading = true;
            $http({
                method: "POST",
                url: myAuth.baseurl + "users/loginuser",
                data: $.param({
                    'username': $scope.user_username,
                    'password': $scope.user_password
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                if (data.msg_type == '0')
                {
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                    $scope.user_password = '';
                } else {
                   
                    $cookieStore.put('users', data.userdetails);
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.user_username = '';
                    $scope.user_password = '';
                    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                    $scope.loggedindetails = myAuth.getUserNavlinks();
                    $scope.loggedin = true;
                    $scope.notloggedin = false;
                    $('#myModal').modal('hide');
                    $location.path("/dashboard");
                }
            });
        }
    }


    $scope.forgotpassword = function() {
        if ($scope.forgotemail == '' || $scope.forgotemail === undefined)
        {
            $scope.forgetpasswordalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
        }
        else
        {
            $scope.loggedindetails = '';
            $scope.loading = true;
            $http({
                method: "POST",
                url: myAuth.baseurl + "users/forgotpassword",
                data: $.param({
                    'forgotemail': $scope.forgotemail

                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                if (data.msg_type == '0')
                {
                    $scope.forgetpasswordalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                } else {
                    $scope.forgetpasswordalertmessage = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.forgotemail = '';
                }

            });
        }
    }



    $scope.signup = function() {
        if ($scope.email == '' || $scope.email === undefined)
        {
            $scope.signupalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
        }
        else
        {
            var emailfilter = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
            if (!(emailfilter.test($scope.email)))
            {
                $scope.signupalertmessage = true;
                $scope.alert = myAuth.addAlert('danger', 'Please provide valid email address.');
            }
            else
            {
                $scope.loading = true;
                $http({
                    method: "POST",
                    url: myAuth.baseurl + "users/signupuser",
                    data: $.param({'email': $scope.email}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.signupalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    } else {
                        $scope.signupalertmessage = true;
                        $scope.alert = myAuth.addAlert('success', data.msg);
                        $scope.email = '';
                    }

                });
            }
        }
    }

    $scope.logout = function() {
        $('.dropdown-menu').hide();
        $scope.loginalertmessage = false;
        $cookieStore.put('users', null);
        $scope.loggedindetails = '';
        $scope.loggedin = false;
        $scope.notloggedin = true;
        $location.path("/");
    }

}

/*============================Activation Controller=====================================*/
mycontrollers.activationController = function($scope, $http, $location, $routeParams, myAuth, $cookies, $cookieStore) {
    $scope.loaddetails = function() {
        $scope.showactivationdiv = true;
        $scope.showactivationdivbig = true;
        $http({
            method: "POST",
            url: myAuth.baseurl + "users/getactivationuserdetails",
            data: $.param({'id': $routeParams.id, 'type': 'activation'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 2) {
                $scope.showactivationdiv = false;
                $scope.showactivationdivbig = true;

                $scope.alert = myAuth.addAlert('warning', data.msg);
            }
            else if (data.msg_type == 1) {
                $scope.showactivationdiv = true;
                $scope.showactivationdivbig = false;
                $scope.useremail = data.email;
                $scope.timezones = data.timezone;
                var tz = jstz.determine(); // Determines the time zone of the browser client
                $scope.timezone = tz.name();
            } else {
                $location.path("/");
            }
        });
    }
    $scope.loaddetails();

    $scope.activate = function() {
        $scope.loading = true;
        var FormData = {
            'id': $routeParams.id,
            'name': $scope.name,
            'username': $scope.username,
            'password': $scope.password,
            'confpassword': $scope.confpassword,
            'timezone': $scope.timezone
        };


        if ($scope.name == '' || $scope.name === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your name.');
        }
        else if ($scope.username == '' || $scope.username === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your username.');
        }
        else if ($scope.password == '' || $scope.password === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter password.');
        }
        else if ($scope.timezone == '' || $scope.timezone === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please select a timezone.');
        }
        else if ($scope.password != $scope.confpassword) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password does not match.');
        } else {
            $http({
                method: "POST",
                url: myAuth.baseurl + "users/activateuser",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $cookieStore.put('users', data.userdetails);
                    $cookieStore.put('isactivation', 1);
                    setTimeout(function()
                    {
                        $scope.$apply(function() {
                            myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                            $scope.loggedindetails = myAuth.getUserNavlinks();
                            $scope.$emit('update_parent_controller', $scope.loggedindetails);
                            $location.path("/dashboard");
                        });
                    }, 2000);
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }

}

/*============================Reset password============================================*/
mycontrollers.resetController = function($scope, $http, $location, $routeParams, myAuth, $cookies, $cookieStore) {

    $scope.loaddetailsreset = function() {

        $http({
            method: "POST",
            url: myAuth.baseurl + "users/getactivationuserdetails",
            data: $.param({'id': $routeParams.id, 'type': 'resetpassword'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 1) {
                $scope.resetuseremail = data.email;

            } else {
                $location.path("/");
            }
        });
    }


    $scope.resetpassword = function() {
        $scope.loading = true;
        var FormData = {
            'id': $routeParams.id,
            'password': $scope.newpassword,
            'confpassword': $scope.newconfirmpassword,
        };
        if ($scope.newpassword == '' || $scope.newpassword === undefined) {
            $scope.loading = false;
            $scope.resetalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter password.');
        }
        else if ($scope.newpassword != $scope.newconfirmpassword) {
            $scope.loading = false;
            $scope.resetalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password does not match.');
        } else {
            $http({
                method: "POST",
                url: myAuth.baseurl + "users/resetpassword",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.resetalertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                } else {
                    $scope.resetalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }

    $scope.loaddetailsreset();
}


/*=================================Dashboard Controller=================================*/
mycontrollers.dashboardController = function($scope, $http, $location, $routeParams, myAuth, $cookies, $cookieStore, $sce) {
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    if ($scope.isUserLoggedIn)
    {

    }
    else
    {
        $location.path("/");
    }
    $scope.dashboardload = function() {
        var isactivation = $cookieStore.get('isactivation');
        if (isactivation == 1) {
            $('#account-activation-step-1').modal('show');
            $scope.getallcategoriesstep1();
            $scope.whytolike();
        }
      // $cookieStore.put('isactivation', 0);
    }
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {

        var idx = $scope.selection.indexOf(cheboxkey);

        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };



    $scope.whytolike = function() {

        

            $http({
                method: "POST",
                url: myAuth.baseurl + "users/whytolikelist",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.whytolikelists = data.whytolike;

            });
        
    }


    $scope.getallcategoriesstep1 = function() {
        $http({
            method: "POST",
            url: myAuth.baseurl + "categories/getcategories",
            data: $.param({'type': 'child'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allcategories = data.allcategories;
        });
    }

    $scope.next = function(divID, type) {
        if (type == '1')
        {
            $scope.loading = true;
            if (($scope.selectedcats == '' || $scope.selectedcats === undefined))
            {
                $scope.loading = false;
            }
            else
            {
                $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: myAuth.baseurl + "users/updateuserrequirementcats",
                    data: $.param({'userid': $scope.loggedindetails.id, 'selectedcats': $scope.selectedcats, 'chosencats': $scope.tags}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        setTimeout(function()
                        {
                            $('#' + divID).modal('show');
                        }, 500);
                    }
                });
            }
        }
        else if (type == '2')
        {
            if ($scope.expertlabel == '' || $scope.expertlabel === undefined)
            {
            }
            else
            {
                $scope.loading = true;
                $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: myAuth.baseurl + "users/updateexpertlabel",
                    data: $.param({'userid': $scope.loggedindetails.id, 'lavel': $scope.expertlabel}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        setTimeout(function()
                        {
                            $('#' + divID).modal('show');
                        }, 500);
                    }
                });
            }
        }
        else if (type == '')
        {
            $('.modal').modal('hide');
            setTimeout(function()
            {
                $('#' + divID).modal('show');
            }, 500);
        }
    }

    $scope.done = function() {
        if ($scope.selectedwhy == '' || $scope.selectedwhy === undefined) {

            
        }else{
            $scope.loading = true;
           $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: myAuth.baseurl + "users/updatewhytolike",
                    data: $.param({'userid': $scope.loggedindetails.id, 'updatewhytolike': $scope.selectedwhy }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        setTimeout(function()
                        {
                            $('#step4').modal('show');
                        }, 500);
                    }
                });
        }


    }



    $scope.skipstep1 = function(divID) {
        $('.modal').modal('hide');
    }

    $scope.active = function(aID, catName) {
        var array = [];
        var values = $("#categories").val();
        var splitted = values.split(',');
        if ($("#" + aID).hasClass('act')) {
            $("#" + aID).removeClass('act');
            for (var i = 0; i < splitted.length; i++) {
                if (splitted[i] != catName) {
                    array.push(splitted[i]);
                }
            }
        } else {
            $("#" + aID).addClass('act');
            splitted.push(catName);
            array = splitted;
        }
        $("#categories").val(array.join());
        $scope.selectedcats = array.join();
    }

    $scope.selectCategory = function(value) {

    }

    $scope.loadTags = function(query) {
        return $http.get(myAuth.baseurl + "categories/getsubcategories/" + query);
    }

    $scope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
    };

    $scope.dashboardload();
}

/*=================================Home Controller=================================*/
mycontrollers.homeController = function($scope, $http, $location, myAuth, $sce) {
    $scope.siteurl = myAuth.baseurl;
    $scope.getallcategories = function() {
        $http({
            method: "POST",
            url: myAuth.baseurl + "categories/getcategories",
            data: $.param({'type': 'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.is_categories_exist == 0)
            {
                $scope.showcategories = false;
            }
            else
            {
                $scope.showcategories = true;
            }
            $scope.allcategories = data.allcategories;
        });
    }
    $scope.getallcategories();
}
prohelpApp.controller(mycontrollers);
