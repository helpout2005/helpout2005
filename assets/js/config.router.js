'use strict';

/**
 * Config for the router
 */

app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires,$authProvider,$locationProvider) {
    
    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    // LAZY MODULES
    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });
    //$locationProvider.html5mode({ enabled: true, requireBase: false });
    //$locationProvider.html5Mode(true).hashPrefix('!');
 
    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
   
    //
    // Set up the states
    //Admin state
    $stateProvider.state('admin', {
        url: "/admin",
        templateUrl: "assets/views/app.html",
        resolve: loadSequence('modernizr', 'moment', 'angularMoment', 'uiSwitch', 'perfect-scrollbar-plugin', 'perfect_scrollbar', 'toaster', 'ngAside', 'vAccordion', 'sweet-alert', 'chartjs', 'tc.chartjs', 'oitozero.ngSweetAlert', 'chatCtrl','admin','adminheaderCtrl'),
        abstract: true
       }).state('admin.dashboard', {
          url: "/dashboard",
          templateUrl: "assets/views/dashboard.html",
          resolve: loadSequence('jquery-sparkline', 'sparkline', 'dashboardCtrl'),
          title: 'Dashboard',
          ncyBreadcrumb: {
              label: 'Dashboard'
          }
       }).state('admin.category', {
            url: "/category",
            templateUrl: "assets/views/category.html",
            resolve: loadSequence('ngTable', 'categoryCtrl'),
            title: 'Category',
            ncyBreadcrumb: {
                label: 'Category'
            }
        }).state('admin.subcategory', {
            url: "/sub-category/:id",
            templateUrl: "assets/views/subcategory.html",
            resolve: loadSequence('ngTable', 'subcategoryCtrl'),
            title: 'Category',
            controller: function($scope,$stateParams){
           $scope.catid=$stateParams.id;
            },
            ncyBreadcrumb: {
                label: 'Sub Category'
            }
        })
       .state('admin.editcategory', {
            url: "/edit-category/:id",
            templateUrl: "assets/views/edit-category.html",
            resolve: loadSequence('ngTable', 'editcategoryCtrl'),
            title: 'Edit Category',
            controller: function($scope,$stateParams){
           $scope.catid=$stateParams.id;
            },
            ncyBreadcrumb: {
                label: 'Edit Category'
            }
        })
        .state('admin.editsubcategory', {
            url: "/edit-sub-category/:id",
            templateUrl: "assets/views/edit-subcategory.html",
            resolve: loadSequence('ngTable', 'editsubcategoryCtrl'),
            title: 'Edit Sub Category',
            controller: function($scope,$stateParams){
           $scope.subcatid=$stateParams.id;
            },
            ncyBreadcrumb: {
                label: 'Edit Sub Category'
            }
        }).state('admin.addsubcategory', {
            url: "/add-sub-category/:id",
            templateUrl: "assets/views/add-subcategory.html",
            resolve: loadSequence('ngTable', 'addsubcategoryCtrl'),
            title: 'Add Sub Category',
           controller: function($scope,$stateParams){
           $scope.parent_id=$stateParams.id;
            },
            ncyBreadcrumb: {
                label: 'Add Sub Category'
            }
        }).state('admin.addcategory', {
            url: "/add-category",
            templateUrl: "assets/views/add-category.html",
            resolve: loadSequence('ngTable', 'addcategoryCtrl'),
            title: 'Add Category',           
            ncyBreadcrumb: {
                label: 'Add Category'
            }
        }).state('admin.helper', {
            url: "/helper",
            templateUrl: "assets/views/helper.html",
            resolve: loadSequence('ngTable', 'helperCtrl'),
            title: 'Helpers',           
            ncyBreadcrumb: {
                label: 'List Helpers'
            }
        }).state('admin.helpee', {
            url: "/helpee",
            templateUrl: "assets/views/helpee.html",
            resolve: loadSequence('ngTable', 'helpeeCtrl'),
            title: 'Helpee',           
            ncyBreadcrumb: {
                label: 'List Helpee'
            }
        })
     //Admin Login routes
     .state('adminlogin', {
	    url: '/adminlogin',	   
	    template: '<div ui-view class="fade-in-right-big smooth"></div>',
	    abstract: true,
            resolve: loadSequence('admin'),
     }).state('adminlogin.signin', {
	    url: '/signin',
 	    resolve: loadSequence('adminloginCtrl','ladda','angular-ladda'),
	    title:'Admin Login',
	    templateUrl: "assets/views/login_login.html"
     }).state('adminlogin.forgot', {
	    url: '/forgot',
	    title:'Admin Forgot Password',
	    templateUrl: "assets/views/login_forgot.html"
     })
	
     //error    
     .state('error', {
        url: '/error',
        resolve: loadSequence('admin'),
        template: '<div ui-view class="fade-in-up"></div>'
     }).state('error.404', {
        url: '/404',
        templateUrl: "assets/views/utility_404.html",
     }).state('error.500', {
        url: '/500',
        templateUrl: "assets/views/utility_500.html",
     })
    
     //Front end state
     .state('frontend',{
        url:'/frontend',
        resolve: loadSequence('frontend', 'headerCtrl', 'ladda', 'angular-ladda', 'frontend','perfect-scrollbar-plugin','perfect_scrollbar'),
        templateUrl:'assets/views/frontend/frontend.html',
        abstract:true
     }).state('frontend.home',{
        url:'/home',
         resolve: loadSequence('indexCtrl'),
        templateUrl:'assets/views/frontend/home.html',
         title: 'Home'
     }).state('frontend.dashboard',{
        url:'/dashboard',
        resolve: loadSequence('userdashboardCtrl'),
        templateUrl:'assets/views/frontend/dashboard.html',
        title: 'Dashboard'
     }).state('frontend.activation',{
        url:'/account-activation/:id',
        resolve: loadSequence('useractivationCtrl','jstz'),
        controller: function($scope,$stateParams){
           $scope.userid=$stateParams.id;
        },
        templateUrl:'assets/views/frontend/account-activation.html',
        title: 'Activation'
     }).state('frontend.mysetting',{
        url:'/my-setting',
        resolve: loadSequence('usersettingCtrl','settingleftpanelCtrl','jstz'),
        templateUrl:'assets/views/frontend/my-setting.html',
        title: 'My Settings'
     }).state('frontend.accountsetting',{
        url:'/account-setting',
        resolve: loadSequence('accountsettingCtrl','settingleftpanelCtrl','jstz'),
        templateUrl:'assets/views/frontend/account-setting.html',
        title: 'Account Settings'
     }).state('frontend.taxinfo',{
        url:'/tax-info',
        resolve: loadSequence('taxinfoCtrl','settingleftpanelCtrl','jstz'),
        templateUrl:'assets/views/frontend/tax-info.html',
        title: 'Tax Information'
     }).state('frontend.expertise',{
        url:'/expertise',
        resolve: loadSequence('expertCtrl','settingleftpanelCtrl','jstz'),
        templateUrl:'assets/views/frontend/expertise.html',
        title: 'Expertise & Rate'
     }).state('frontend.resetpassword',{
        url:'/reset-password/:id',
        resolve: loadSequence('resetpasswordCtrl'),
        controller: function($scope,$stateParams){
	   $scope.uid=$stateParams.id;
        },
        templateUrl:'assets/views/frontend/reset-password.html',
        title: 'Reset Password'
     }).state('frontend.mentor-signup',{
        url:'/mentor-signup',
        resolve: loadSequence('mentorsignupCtrl'),       
        templateUrl:'assets/views/frontend/mentor-signup.html',
        title: 'Mentor Signup'
     }).state('frontend.mentor-signup-step1',{
        url:'/mentor-signup-step1/:id',
        controller: function($scope,$stateParams){
	  $scope.userid=$stateParams.id;
        },
        resolve: loadSequence('mentorsignupstep1Ctrl','jstz'),       
        templateUrl:'assets/views/frontend/mentor-signup_step1.html',
        title: 'Mentor Signup Step'
     }).state('frontend.mentor-dashboard',{
        url:'/mentor-dashboard',
        resolve: loadSequence('mentordashboardCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/mentor-dashboard.html',
        title: 'Mentor Dashboard'
     }).state('frontend.open-requests',{
        url:'/open-requests',
        resolve: loadSequence('openrequestCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/open-requests.html',
        title: 'Open Requests'
     }).state('frontend.profile',{
        url:'/profile/:username',
        resolve: loadSequence('userprofileCtrl'), 
        controller: function($scope,$stateParams){
	   $scope.username=$stateParams.username;
        },      
        templateUrl:'assets/views/frontend/user-profile.html',
        title: 'Profile'
     }).state('frontend.mentor',{
        url:'/mentor/:username',
        resolve: loadSequence('mentorprofileCtrl'), 
        controller: function($scope,$stateParams){
	   $scope.username=$stateParams.username;
        },      
        templateUrl:'assets/views/frontend/mentor-profile.html',
        title: 'Mentor Profile'
     }).state('frontend.live-feed',{
        url:'/live-feed',
        resolve: loadSequence('livefeedCtrl','mentorleftpanelCtrl'), 
        templateUrl:'assets/views/frontend/live-feed.html',
        title: 'Live feed'
     }).state('frontend.availability',{
        url:'/availability',
        resolve: loadSequence('availabilityCtrl','mentorleftpanelCtrl','ladda', 'angular-ladda'), 
        templateUrl:'assets/views/frontend/availability.html',
        title: 'Availability'
     }).state('frontend.search-result',{
        url:'/search-result/:id',
        resolve: loadSequence('searchResultCtrl'), 
        controller: function($scope,$stateParams){
	   $scope.keyword=$stateParams.id;
        },
        templateUrl:'assets/views/frontend/search-result.html',
        title: 'Live feed'
     }).state('frontend.buy_credit',{
        url:'/buy_credit',
        resolve: loadSequence('buycreditCtrl','angularPayments'),       
        templateUrl:'assets/views/frontend/buy_credit.html',
        title: 'Buy Credit'
     }).state('frontend.payment_return',{
        url:'/payment_return?token&PayerID',
        resolve: loadSequence('paymentreturnCtrl'),       
        templateUrl:'assets/views/frontend/payment_return.html',
        title: 'Please Wait...'
     }).state('frontend.payment_success',{
        url:'/payment_success',
        resolve: loadSequence('paymentsuccessCtrl'),       
        templateUrl:'assets/views/frontend/payment_success.html',
        title: 'Payment Successfull'
     }).state('frontend.payment_cancel',{
        url:'/payment_cancel',
        resolve: loadSequence('paymentcancelCtrl'),       
        templateUrl:'assets/views/frontend/payment_cancel.html',
        title: 'Payment Failed'
     })/*.state('frontend.helpees_helps',{
        url:'/helpees_helps',
        resolve: loadSequence('helpeeshelpsCtrl'),       
        templateUrl:'assets/views/frontend/helpees_helps.html',
        title: 'Payment Failed'
     })*/.state('frontend.helpees_assigns',{
        url:'/helpees_assigns',
        resolve: loadSequence('helpeesassignsCtrl'),       
        templateUrl:'assets/views/frontend/helpees_assigns.html',
        title: 'Assigned Requests'
     }).state('frontend.helpers_active',{
        url:'/helpers_active',
        resolve: loadSequence('helpersactiveCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/helpers_active.html',
        title: 'Active Requests'
     }).state('frontend.helpers_transactions',{
        url:'/helpers_transactions',
        resolve: loadSequence('helperstransactionsCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/helpers_transactions.html',
        title: 'Helpers Transactions'
     }).state('frontend.test_page',{
        url:'/test_page',
        resolve: loadSequence('testpageCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/test_page.html',
        title: 'Active Requests'
     }).state('frontend.help_dashboard',{
        url:'/help_dashboard/:tab?',
        resolve: loadSequence('helpdashboardCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/help_dashboard.html',
        title: 'Help Dashboard'
     }).state('frontend.transactions',{
        url:'/transactions',
        resolve: loadSequence('transactionsCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/transactions.html',
        title: 'Transactions'
     }).state('frontend.help_details',{
        url:'/help_details/:id',
        resolve: loadSequence('helpdetailsCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/help_details.html',
        title: 'Help Details'
     }).state('frontend.withdraw',{
        url:'/withdraw',
        resolve: loadSequence('withdrawCtrl','mentorleftpanelCtrl','angularTransfer'),       
        templateUrl:'assets/views/frontend/withdraw.html',
        title: 'Withdraw'
     }).state('frontend.helper_transactions',{
        url:'/helper_transactions',
        resolve: loadSequence('helpertransactionsCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/helper_transactions.html',
        title: 'Transactions'
     }).state('frontend.helper_dashboard',{
        url:'/helper_dashboard',
        resolve: loadSequence('helperdashboardCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/helper_dashboard.html',
        title: 'Help Dashboard'
     }).state('frontend.help_detail',{
        url:'/help_detail/:id',
        resolve: loadSequence('helpdetailCtrl','mentorleftpanelCtrl'),       
        templateUrl:'assets/views/frontend/help_detail.html',
        title: 'Help Details'
     }).state('frontend.video_call',{
        url:'/video_call/:id',
        resolve: loadSequence('videocallCtrl'),       
        templateUrl:'assets/views/frontend/video_call.html',
        title: 'Video Call'
     });
     
     $urlRouterProvider.otherwise("/frontend/home");
   
    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
           };
      }
}]);
