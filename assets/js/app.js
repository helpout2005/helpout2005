/** 
  * declare 'Live-Help-out' module with dependencies
*/
'use strict';
angular.module("Live-Help-out", [
        'ngRoute',
	'ngAnimate',
	'ngCookies',
	'ngStorage',
	'ngSanitize',
	'ngTouch',
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
	'cfp.loadingBar',
	'ncy-angular-breadcrumb',
	'duScroll',
	'pascalprecht.translate',
	'authFront',
	'ngTagsInput',
        'SocketModule',
        'ngFacebook',
        'ngLinkedIn',
        'googleplus',
        'djds4rce.angular-socialshare',
	'blueimp.fileupload',
	'ngPhotoGrid',
	'angularUtils.directives.dirPagination',
        'ui.calendar'
]);

