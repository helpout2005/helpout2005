'use strict';
/** 
  * controllers used for the open request
*/
app.controller('testpageCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.listView=false;
     $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
     }
     
    $scope.assignedhelps = {};
    $scope.getAssignedHelps = function()
    {
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "assigned_helps/getAssignedOfHelper/" + $scope.loggedindetails.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(res) {
              if(res.status == 'success')
              {
                    $scope.assignedhelps = res.data;
                    console.log("Helps ======= ",res.data);
              }
          })
    }
    $scope.getAssignedHelps();
    $scope.transView = false;
    $scope.transactions = false;
    $scope.paymentViewData = {};
    $scope.paymentView = function(obj)
    {
        $scope.transView = !$scope.transView;
        $scope.paymentViewData = obj;
        $scope.listView = !$scope.listView;
        $scope.transactions = false;
        
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/gethelptransfers/" + obj.help_id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.transactions = trans.data;
            }
        })
    }
    
    $scope.requestRelease = function(tranfer_id){
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/request_release/" + tranfer_id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.transactions = trans.data;
            }
        })
    }
});


