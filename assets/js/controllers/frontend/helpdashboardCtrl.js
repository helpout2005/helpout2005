'use strict';
/** 
  * controllers used for the open request
*/
app.controller('helpdashboardCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket, $stateParams) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     if($stateParams.tab)
	 {
		 $scope.active_tab = $stateParams.tab - 1;
	 }
	 else
	 {
		$scope.active_tab = 0;
	 }
     
     $scope.change_tab = function($ind){
         
         if($ind==0)
         {
             $scope.getOpenHelps();
         }
         if($ind==1)
         {
             $scope.getActiveHelps();
         }
         if($ind==2)
         {
             $scope.getCompletedHelps();
         }
         if($ind==3)
         {
             $scope.getCanceledHelps();
         }
         
         $scope.active_tab = $ind;
     } 
     
     $scope.listView=false;
     $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
     }
     
     $scope.openhelps = false;
     $scope.getOpenHelps = function(){
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/my_opens",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.openhelps = trans.data;
            }
        })
     }
     $scope.getOpenHelps();
    
	 
     $scope.canceledhelps = false;
     $scope.getCanceledHelps = function(){
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/my_canceled",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.canceledhelps = trans.data;
            }
        })
     }
     
     $scope.activehelps = false;
     $scope.getActiveHelps = function(){
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/my_actives",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.activehelps = trans.data;
            }
        })
     }
	  $scope.getActiveHelps();
	 
	 $scope.releaseEscrow = function(help,$event){
		 if ($event.stopPropagation) $event.stopPropagation();
		if ($event.preventDefault) $event.preventDefault();
		$event.cancelBubble = true;
		$event.returnValue = false;
		 help.ladda_stat = true;
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/release_escrow",
            data: $.param({'help_id' : help.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                //$scope.transactions = trans.data;
                $scope.getActiveHelps();
            }
            $scope.changerequeststatusmessage = true;
			 setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
            $scope.alert = myAuth.addAlert(trans.status, trans.message);
        })
    }
     
     $scope.completedhelps = false;
     $scope.getCompletedHelps = function(){
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/my_completed",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.completedhelps = trans.data;
            }
        })
     }
     
     $scope.viewdetails = function(obj){
         $location.path('/frontend/help_details/' + obj.id);
         //console.log(obj);
     }
     
     $scope.mark_as_solved=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/mark_as_solved",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
           if(data.type==0)
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('danger', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
           }
           else
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('success', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
             $scope.getOpenHelps();
           }
       });
    }
    
    $scope.mark_as_cancel=function(requestid){
        if(confirm("Are you sure, you want to cancel this request?"))
        {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "helps/mark_as_cancel",
                data: $.param({'request_id': requestid}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                if(data.type==0)
                {
                  $scope.changerequeststatusmessage=true;
                  $scope.alert = myAuth.addAlert('danger', data.msg);
                  setTimeout(function()
                  {
                    $scope.changerequeststatusmessage=false;
                  }, 1000);
                }
                else
                {
                  $scope.changerequeststatusmessage=true;
                  $scope.alert = myAuth.addAlert('success', data.msg);
                  setTimeout(function()
                  {
                    $scope.changerequeststatusmessage=false;
                  }, 1000);
                  socket.emit("update_request_feed", requestid);
                  $scope.getOpenHelps();
                }
            });
        } 
    }
});


