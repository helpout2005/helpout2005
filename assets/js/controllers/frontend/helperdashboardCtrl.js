'use strict';
/** 
  * controllers used for the open request
*/
app.controller('helperdashboardCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.active_tab = 1;
     
     $scope.change_tab = function($ind){
         
         
         if($ind==1)
         {
             $scope.getActiveHelps();
         }
         if($ind==2)
         {
             $scope.getCompletedHelps();
         }
        
         
         $scope.active_tab = $ind;
     } 
     
     $scope.listView=false;
     $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
     }
     
    
     
     $scope.activehelps = false;
     $scope.getActiveHelps = function(){
         $http({
            method: "POST", 
            url: $rootScope.serviceurl + "helps/helper_actives",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.activehelps = trans.data;
            }
        })
     }
     $scope.getActiveHelps();
     
     $scope.completedhelps = false;
     $scope.getCompletedHelps = function(){
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/helper_completed",
            data: $.param({'userid' : $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.completedhelps = trans.data;
            }
        })
     }
     
     $scope.viewdetails = function(obj){
         $location.path('/frontend/help_detail/' + obj.id);
         //console.log(obj);
     }
     
     $scope.mark_as_solved=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/mark_as_solved",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
           if(data.type==0)
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('danger', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
           }
           else
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('success', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
             $scope.getOpenHelps();
           }
       });
    }
    
    $scope.mark_as_cancel=function(requestid){
        if(confirm("Are you sure, you want to cancel this request?"))
        {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "helps/mark_as_cancel",
                data: $.param({'request_id': requestid}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                if(data.type==0)
                {
                  $scope.changerequeststatusmessage=true;
                  $scope.alert = myAuth.addAlert('danger', data.msg);
                  setTimeout(function()
                  {
                    $scope.changerequeststatusmessage=false;
                  }, 1000);
                }
                else
                {
                  $scope.changerequeststatusmessage=true;
                  $scope.alert = myAuth.addAlert('success', data.msg);
                  setTimeout(function()
                  {
                    $scope.changerequeststatusmessage=false;
                  }, 1000);
                  socket.emit("update_request_feed", requestid);
                  $scope.getOpenHelps();
                }
            });
        } 
    }
});


