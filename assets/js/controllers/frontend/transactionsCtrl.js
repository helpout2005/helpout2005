'use strict';
/** 
  * controllers used for the open request
*/
app.controller('transactionsCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.listView=false;
     $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
     }
     
    
    $scope.getTransactions = function()
    {
         
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/my_tranfers/" + $scope.loggedindetails.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.transactions = trans.data;
            }
        })
    }
    $scope.getTransactions();
    
    $scope.userDetails = function()
    {
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getuserdetails/",
            data:$.param({userid:$scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(userdata){
            if(userdata && userdata.msg_type)
            {
                myAuth.updateUserinfo(userdata.userdetails);
                $cookieStore.put('users',userdata.userdetails);
                $scope.loggedindetails = myAuth.getUserNavlinks();
            }
        })
    }
    //$scope.userDetails();
    
    
    
});


