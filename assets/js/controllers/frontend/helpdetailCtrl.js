'use strict';
/** 
  * controllers used for the open request
*/
app.controller('helpdetailCtrl', function ($rootScope, $stateParams,$scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     $scope.helpid = $stateParams.id
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.activetab = 0;
     $scope.change_tab = function($ind){
         $scope.activetab = $ind;
     }
     
     $scope.help_details = {};
     $scope.getHelpDetails = function(){
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "assigned_helps/assigned_help_details/" + $scope.helpid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.help_details = trans.data;
            }
        })
     }
     $scope.getHelpDetails();
     
     
    $scope.transaction = false;
    $scope.getTransactions = function(help_id){
        $scope.transView = !$scope.transView;
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/gethelptransfer/" + $scope.helpid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                $scope.transaction = trans.data;
            }
        })
    }
    $scope.getTransactions();
    
    $scope.requestRelease = function(tranfer){
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/request_release/" + tranfer.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            $scope.changerequeststatusmessage = true;
            $scope.alert = myAuth.addAlert(trans.status, trans.message);
            if(trans.status == 'success')
            {
                
                //$scope.paymentView(tranfer);
                //$scope.transactions = trans.data;
            }
        })
    }
    
    $scope.transfer_cash = function(pay_details){
        
        /************ Escrow Amount **************/
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/escrow_credit",
            data: $.param({payer_id : $scope.loggedindetails.id, receiver_id : pay_details.helper_id, amount:pay_details.help_budget,help_id:pay_details.help_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {             
                $scope.changerequeststatusmessage = true;
                $scope.alert = myAuth.addAlert(data.status, data.message);
                if(data.status == 'success')
                {
                    $http({
                            method: "POST",
                            url: $rootScope.serviceurl + "users/getuserdetails",
                            data: 'userid=' + $scope.loggedindetails.id ,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function(userdata){
                            if(userdata && userdata.msg_type)
                            {
                                myAuth.updateUserinfo(userdata.userdetails);
                                $cookieStore.put('users',userdata.userdetails);
                                $scope.listView = !$scope.listView;
                                //$scope.getAssignedHelps();
                                $scope.changerequeststatusmessage = false;
                                $scope.loggedindetails = myAuth.getUserNavlinks();
                                $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                                $scope.getHelpDetails();
                                //$location.path('/frontend/payment_success');
                            }
                        })
                }
          })
    }
    
    $scope.releaseEscrow = function(id){
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/release_escrow",
            data: $.param({'transfer_id' : id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(trans){
            if(trans.status == 'success')
            {
                //$scope.transactions = trans.data;
                $scope.getHelpDetails();
            }
            $scope.changerequeststatusmessage = true;
            $scope.alert = myAuth.addAlert(data.status, data.message);
        })
    }
    
    $scope.mark_as_solved=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/mark_as_solved",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
           if(data.type==0)
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('danger', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
           }
           else
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('success', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
             $location.path('/frontend/help_dashboard');
           }
       });
    }
     
});


