'use strict';
/** 
  * controllers used for the user live feed
*/
app.controller('availabilityCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket,filterFilter){
     $rootScope.$on("updatelivefeedonpage", function(event, message){
     var hid=$('#hidden_activetab').val();
     if(hid==1){
        $scope.getalllivefeeds();
     }else{
        $scope.filterresult();
     }
    });
    
    $rootScope.$on("updaterequestlivefeedonpage", function(event, message){
     var hid=$('#hidden_activetab').val();
     if(hid==1){
        $scope.getalllivefeeds();
     }else{
        $scope.filterresult();
     }
    });
    
    $('.dropdown-menu2').hide();
    $scope.siteurl = myAuth.baseurl;
    $scope.youtubevideo='';
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    if ($scope.isUserLoggedIn)
    {

    }
    else
    {
       $location.path("/frontend/home");
    }    
    $scope.tags = [];
    $scope.eventSources = [];
    $scope.cal_events = [];
    
    $scope.time = {"12:00 am":"12:00 am","12:15 am": "12:15 am", "12:30 am": "12:30 am","12:45 am" : "12:45 am",
    			    "01:00 am": "01:00 am","01:15 am": "01:15 am", "01:30 am": "01:30 am","01:45 am": "01:45 am",
    			    "02:00 am": "02:00 am","02:15 am": "02:15 am", "02:30 am": "02:30 am","02:45 am": "02:45 am",
    			    "03:00 am": "03:00 am","03:15 am": "03:15 am", "03:30 am": "03:30 am","03:45 am": "03:45 am",
    			    "04:00 am": "04:00 am","04:15 am": "04:15 am", "04:30 am": "04:30 am","04:45 am": "04:45 am",
    			    "05:00 am": "05:00 am","05:15 am": "05:15 am", "05:30 am": "05:30 am","05:45 am": "05:45 am",
    			    "06:00 am": "06:00 am","06:15 am": "06:15 am", "06:30 am": "06:30 am","06:45 am": "06:45 am",
    			    "07:00 am": "07:00 am","07:15 am": "07:15 am", "07:30 am": "07:30 am","07:45 am": "07:45 am",
    			    "08:00 am": "08:00 am","08:15 am": "08:15 am", "08:30 am": "08:30 am","08:45 am": "08:45 am",
    			    "09:00 am": "09:00 am","09:15 am": "09:15 am", "09:30 am": "09:30 am","09:45 am": "09:45 am",
    			    "10:00 am": "10:00 am","10:15 am": "10:15 am", "10:30 am": "10:30 am","10:45 am": "10:45 am",
    			    "11:00 am": "11:00 am","11:15 am": "11:15 am", "11:30 am": "11:30 am","11:45 am": "11:45 am",
    			    "12:00 pm": "12:00 pm","12:15 pm": "12:15 pm", "12:30 pm": "12:30 pm","12:45 pm": "12:45 pm",
    			    "01:00 pm": "01:00 pm","01:15 pm": "01:15 pm", "01:30 pm": "01:30 pm","01:45 pm": "01:45 pm",
    			    "02:00 pm": "02:00 pm","02:15 pm": "02:15 pm", "02:30 pm": "02:30 pm","02:45 pm": "02:45 pm",
    			    "03:00 pm": "03:00 pm","03:15 pm": "03:15 pm", "03:30 pm": "03:30 pm","03:45 pm": "03:45 pm",
    			    "04:00 pm": "04:00 pm","04:15 pm": "04:15 pm", "04:30 pm": "04:30 pm","04:45 pm": "04:45 pm",
    			    "05:00 pm": "05:00 pm","05:15 pm": "05:15 pm", "05:30 pm": "05:30 pm","05:45 pm": "05:45 pm",
    			    "06:00 pm": "06:00 pm","06:15 pm": "06:15 pm", "06:30 pm": "06:30 pm","06:45 pm": "06:45 pm",
    			    "07:00 pm": "07:00 pm","07:15 pm": "07:15 pm", "07:30 pm": "07:30 pm","07:45 pm": "07:45 pm",
    			    "08:00 pm": "08:00 pm","08:15 pm": "08:15 pm", "08:30 pm": "08:30 pm","08:45 pm": "08:45 pm",
    			    "09:00 pm": "09:00 pm","09:15 pm": "09:15 pm", "09:30 pm": "09:30 pm","09:45 pm": "09:45 pm",
    			    "10:00 pm": "10:00 pm","10:15 pm": "10:15 pm", "10:30 pm": "10:30 pm","10:45 pm": "10:45 pm",
    			    "11:00 pm": "11:00 pm","11:15 pm": "11:15 pm", "11:30 pm": "11:30 pm","11:45 pm": "11:45 pm",};
    			  
    
    $scope.days = [{
            "id": "monday",
            "value": "Monday",
            "selected": true,
        }, {
            "id": "tuesday",
            "value": "Tuesday",
            "selected": true,
        }, {
            "id": "wednesday",
            "value": "Wednesday",
            "selected": true,
        },{
            "id": "thursday",
            "value": "Thursday",
            "selected": true,
        }, {
            "id": "friday",
            "value": "Friday",
            "selected": true,
        }, {
            "id": "saturday",
            "value": "Saturday"
        },{
            "id": "sunday",
            "value": "Sunday"
        }];
		$scope.selectedDays = function selectedDays() {
			return filterFilter($scope.days, { selected: true });
		};


		$scope.$watch('days|filter:{selected:true}', function (nv) {
		    $scope.selection = nv.map(function (item) {
			 return item.id;
		    });
		}, true);
    $scope.getSchedules = function (){
        $http({
                    method: "POST",
                    dataType: 'json',
                    url: $rootScope.serviceurl + "schedules/schedules_by_id/" + $scope.loggedindetails.id,
                    data:'',
                    headers: {'Content-Type': 'application/json'},
                }).success(function(data) 
                {
                    console.log(data);
                    if(data)
                    {
                        $scope.uiConfig.calendar.events = data;
                    }
                    console.log('=========== Schedule ==========',data);
                });
    }
    $scope.getSchedules();
    
     $scope.uiConfig = {
      calendar:{
		height: 650,
		defaultView:'agendaWeek',
		timeFormat: 'HH:mm',
		timezone:'local',
		ignoreTimezone:false,
		selectOverlap:true,
		slotEventOverlap:false,
		allDaySlot:false,
		axisFormat:'H:mm ',
		slotDuration: '00:30:00',
		editable: true,
		disableDragging:true,
		columnFormat: {
			month: 'ddd', 
			week: 'ddd M.D',
			day: 'dddd' 
		},
          eventLimit: true, // for all non-agenda views
	     views: {
		   agenda: {
		       eventLimit: 2 // adjust to 6 only for agendaWeek/agendaDay
		   }
	     },
        header:{
          left: 'title',
          center: '',
          right: 'today month agendaWeek prev next'
        },
        selectable:true,
        dayRender: function( date, cell) {
             cell.addTouch();
		  /* $(cell).on("touchend",function(event){
		       var startDate = date;
		       var x= event.originalEvent.changedTouches[0].clientX;
		       var y = event.originalEvent.changedTouches[0].clientY
		       var endDate = moment($(document.elementFromPoint(x, y)).attr("data-date"),"YYYY-MM-DD");
		       if(endDate>startDate){
		           calendar.fullCalendar( 'select', startDate, endDate.add('days', 1));
		       }else{
		           calendar.fullCalendar( 'select', endDate, startDate.add('days', 1));
		       }
		   });*/
	    },
        select:function(start,end,jsEvent, view){
             alert('Hi'); 
              $('#calenderEventPopup').modal('show');

              /*$scope.start_date = moment(start).format('YYYY MM DD hh:mm:ss a');
              $scope.end_date = moment(end).format('YYYY MM DD hh:mm:ss a');
              $("#sttm_div").html(moment(start).format('MM/DD hh:mm a')+" - "+moment(end).format('MM/DD hh:mm a'));
              var f =new Date(start);
              $scope.day = f.getDay();*/
              
              $scope.start_date = moment(start).format('YYYY MM DD');
              $scope.end_date = moment(end).format('YYYY MM DD');
              $scope.start_time = moment(start).format('hh:mm a');
              $scope.end_time = moment(end).format('hh:mm a');
              
              $("#sttm_div").html(moment(start).format('MM/DD'));
              var f =new Date(start);
              $scope.day = f.getDay();
              //$('#startDate').val('{{'+moment(start).format('YYYY-MM-DD H:mm:ss a')+'}}');
              //$('#endDate').val('{{'+moment(end).format('YYYY-MM-DD H:mm:ss a')+'}}');
              /* $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "schedules/add",
                    data: $.param({'user_id': $scope.loggedindetails.id,'start':moment(start).format('YYYY MM DD H:mm:ss a'),'end':moment(end).format('YYYY MM DD H:mm:ss a')}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                    if(data)
                    {
                        $scope.uiConfig.calendar.events.push(data);
                    }
                });*/
            //console.log(moment(start).format('YYYY MM DD H:mm:ss a'),'================',moment(end).format('YYYY MM DD H:mm:ss a'));
        },
        eventResize:function(event){
             $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "schedules/update",
                    data: $.param({'id': event.id,'start':moment(event.start).format('YYYY MM DD hh:mm:ss a'),'end':moment(event.end).format('YYYY MM DD hh:mm:ss a')}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                    if(data)
                    {
                       // $scope.uiConfig.calendar.events.push(data);
                    }
                });
            console.log(event);
        },
        eventClick: function(calEvent, jsEvent, view) {
		    
		    $('#calenderUpdateEventPopup').modal('show');
		    
		    $scope.update_event_title = calEvent.title;
		    $scope.update_event_description = calEvent.description;
		    $("#sttm").html(moment(calEvent.start).format('MM/DD hh:mm a')+" - "+moment(calEvent.end).format('MM/DD hh:mm a'));
		    $scope.event_id = calEvent.id;
		    $scope.update_start_date = moment(start).format('YYYY MM DD hh:mm:ss a');
              $scope.update_end_date = moment(end).format('YYYY MM DD hh:mm:ss a');
              
              $scope.apply();
              /*$scope.start_date = moment(start).format('YYYY MM DD hh:mm:ss a');
              $scope.end_date = moment(end).format('YYYY MM DD hh:mm:ss a');
              $("#sttm_div").html(moment(start).format('MM-DD hh:mm a')+" - "+moment(end).format('MM-DD hh:mm a'));*/
        //alert('Event: ' + calEvent.title);
        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        //alert('View: ' + view.name);

        // change the border color just for fun
        //$(this).css('border-color', 'red');

    },
    dayClick: function(date, jsEvent, view) {

        //alert('Clicked on: ' + date.format());

        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

       // alert('Current view: ' + view.name);

        // change the day's background color just for fun
        //$(this).css('background-color', 'red');

    },
         eventRender: function(event, element) {
            var s = 'delete_schedule(\"' + event.id + '\")';
            //element.append( "<a href='javascript:void()' class='closeon' ng-click='" + s + "'>X</a>" );
            /*element.find(".closeon").click(function() {
                console.log('hiiiiiiii');
                 $http({
                    method: "POST",
                    dataType: 'json',
                    url: $rootScope.serviceurl + "schedules/delete/" + event.id,
                    data:'',
                    headers: {'Content-Type': 'application/json'},
                }).success(function(data) 
                {
                    if(data)
                    {
                         $scope.uiConfig.removeEvents = event._id;
                    }
                    //console.log('=========== Schedule ==========',data);
                });
              
            });*/
        },
        //dayClick: $scope.alertEventOnClick,
        eventDrop: $scope.alertOnDrop
      }
    };
    $scope.new_event = function(){
    		 $('#calenderNewPopup').modal('show');

              /*$scope.start_date = moment(start).format('YYYY MM DD hh:mm:ss a');
              $scope.end_date = moment(end).format('YYYY MM DD hh:mm:ss a');
              $("#sttm_div").html(moment(start).format('MM/DD hh:mm a')+" - "+moment(end).format('MM/DD hh:mm a'));
              var f =new Date(start);
              $scope.day = f.getDay();*/
              
              $scope.start_date = moment(Date.now()).format('YYYY-MM-DD');
              //$scope.end_date = moment(end).format('YYYY MM DD');
              $scope.start_time = moment(Date.now()).format('hh:mm a');
              $scope.end_time = moment(Date.now()).format('hh:mm a');
              
              
              //var f =new Date(start);
              //$scope.day = f.getDay();
    }
    $scope.post_event = function(){
     	console.log($scope.start_date+' '+$scope.start_time);
     	var eventStartDate = $scope.start_date+' '+$scope.start_time;
     	var startDate = moment(eventStartDate).format('YYYY-MM-DD HH:mm:ss');
     	console.log($scope.start_date+' '+$scope.end_time);
     	var eventEndDate = $scope.start_date+' '+$scope.end_time;
     	var endDate = moment(eventEndDate).format('YYYY-MM-DD HH:mm:ss');
     	
     	
     	var startTimeObject = new Date(startDate);
     	
		var endTimeObject = new Date(endDate);
		console.log(startTimeObject+'|'+endTimeObject);
		if(startTimeObject > endTimeObject)
		{
			$scope.alertPostEvent = true;
               $scope.alert = myAuth.addAlert('danger', 'End time should be greater than start time.');
		}
		else
		{
			//alert($scope.event_title);
			$scope.loading = true;
			console.log($scope.day+'|'+$scope.event_recurring+'|'+startDate+'|'+endDate+'|'+'|'+$scope.event_title);
			$http({
		          method: "POST",
		          url: $rootScope.serviceurl + "schedules/add",
		         // data: $.param({'user_id': $scope.loggedindetails.id,'start':moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss'),'end':moment($scope.end_date).format('YYYY-MM-DD HH:mm:ss'),'title':$scope.event_title,'description':$scope.event_description,'recur':$scope.event_recurring,'days':$scope.selection}),
		          data: $.param({'user_id': $scope.loggedindetails.id,'start':startDate,'end':endDate,'title':$scope.event_title,'description':$scope.event_description,'recur':$scope.event_recurring,'days':$scope.selection}),
		          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		          
		      }).success(function(data) 
		      {
		          $scope.loading = false;
		          if(data)
		          {
		             console.log(data);
		              //$scope.uiConfig.calendar.events.push(data);
		              $('#startDate').val('');
		          	//$('#endDate').val('');
		          	$('#start_time').val('');
		          	$('#end_time').val('');
		          	//$('#day').val('');
		          	$('#title').val('');$('#description').val('');//$('#sttm_div').html('');
		          	$('#calenderNewPopup').modal('hide');
		          	$scope.getSchedules();
		          }
		      });
		} 
		
     	
    }
    $scope.add_event = function(){
     	console.log($scope.start_date+' '+$scope.start_time);
     	var eventStartDate = $scope.start_date+' '+$scope.start_time;
     	var startDate = moment(eventStartDate).format('YYYY-MM-DD HH:mm:ss');
     	console.log($scope.end_date+' '+$scope.end_time);
     	var eventEndDate = $scope.end_date+' '+$scope.end_time;
     	var endDate = moment(eventEndDate).format('YYYY-MM-DD HH:mm:ss');
     	//alert($scope.event_title);
     	var startTimeObject = new Date(startDate);
     	
		var endTimeObject = new Date(endDate);
		console.log(startTimeObject+'|'+endTimeObject);
		if(startTimeObject > endTimeObject)
		{
			$scope.alertAddEvent = true;
               $scope.alert = myAuth.addAlert('danger', 'End time should be greater than start time.');
		}
		else
		{
     	     $scope.loading = true;
			console.log($scope.day+'|'+$scope.event_recurring+'|'+startDate+'|'+endDate+'|'+'|'+$scope.event_title);
			$http({
		          method: "POST",
		          url: $rootScope.serviceurl + "schedules/add",
		         // data: $.param({'user_id': $scope.loggedindetails.id,'start':moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss'),'end':moment($scope.end_date).format('YYYY-MM-DD HH:mm:ss'),'title':$scope.event_title,'description':$scope.event_description,'recur':$scope.event_recurring,'days':$scope.selection}),
		          data: $.param({'user_id': $scope.loggedindetails.id,'start':startDate,'end':endDate,'title':$scope.event_title,'description':$scope.event_description,'recur':$scope.event_recurring,'days':$scope.selection}),
		          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		          
		      }).success(function(data) 
		      {
		          $scope.loading = false;
		          if(data)
		          {
		             console.log(data);
		              //$scope.uiConfig.calendar.events.push(data);
		              $('#startDate').val('');
		          	$('#endDate').val('');
		          	$('#start_time').val('');
		          	$('#end_time').val('');
		          	$('#day').val('');
		          	$('#title').val('');$('#description').val('');//$('#sttm_div').html('');
		          	$('#calenderEventPopup').modal('hide');
		          	$scope.getSchedules();
		          }
		      });
           }
    }
    
     $scope.edit_event = function(){
     	//alert($scope.event_title);
     	console.log($scope.update_start_date+'|'+$scope.update_end_date+'|'+moment($scope.update_start_date).format('YYYY MM DD hh:mm:ss a')+'|'+moment($scope.update_end_date).format('YYYY MM DD hh:mm:ss a')+'|'+$scope.update_event_title);
     	$http({
               method: "POST",
               url: $rootScope.serviceurl + "schedules/edit_event",
               data: $.param({'id': $scope.event_id,'title':$scope.update_event_title,'description':$scope.update_event_description}),
               headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               
           }).success(function(data) 
           {
               if(data)
               {
                  console.log(data);
                   //$scope.uiConfig.calendar.events.push(data);
                   $scope.update_event_title = '';
		    	    $scope.update_event_description = '';
		    	    $scope.event_id = '';
			    $scope.update_start_date = '';
		         $scope.update_end_date = '';
		         
                   /*$('#update_startDate').val('');
               	$('#update_endDate').val('');
               	$('#update_title').val('');$('#update_description').val('');//$('#sttm').html('');*/
               	$('#calenderUpdateEventPopup').modal('hide');
               	$scope.getSchedules();
               	 //$scope.uiConfig.calendar.events.push(data);
               	//$scope.uiConfig.calendar( 'refetchEvents' );
               	//$route.reload();
               	//$location.path("/frontend/availability");
               }
           });
    }
    
     $scope.delete_event = function(){
     	//alert($scope.event_title);
     	console.log($scope.event_id);
     	$http({
               method: "POST",
               url: $rootScope.serviceurl + "schedules/delete_event",
               data: $.param({'id': $scope.event_id}),
               headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               
           }).success(function(data) 
           {
               if(data)
               {
                  console.log(data);
                   $scope.update_event_title = '';
		    	    $scope.update_event_description = '';
		    	    $scope.event_id = '';
			    $scope.update_start_date = '';
		         $scope.update_end_date = '';
		         
                   /*$('#update_startDate').val('');
               	$('#update_endDate').val('');
               	$('#update_title').val('');$('#update_description').val('');//$('#sttm').html('');*/
               	$('#calenderUpdateEventPopup').modal('hide');
               	$scope.getSchedules();
               }
           });
    }
    $scope.alertEventOnClick = function(){
        alert('hi');
    }
    $scope.alertOnDrop = function(){
        alert('hi');
    }
    $scope.delete_schedule = function(id)
    {
        console.log(id);
    }
    
    $scope.getUserDetails = function(){
          $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/getdetails",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                    if(data.userdetails && data.userdetails.tags)
                    {
                        $scope.tags = data.userdetails.tags;
                    }
                });
    }
    $scope.getUserDetails();
     
    
    $scope.getalllivefeeds = function() {
          $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/getopenrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                   $("#loader").hide();
                   if(data.related_request_exist==1)
                   {
                        $scope.relatedrequestsdiv=false;
                        $scope.relatedrequests=data.relatedrequests;
                        $('.tab-pane').removeClass('in');
                        $('.tab-pane').removeClass('active');
                        $('#related_request').addClass('in');
                        $('#related_request').addClass('active');
                   }else{
                        $('.tab-pane').removeClass('in');
                        $('.tab-pane').removeClass('active');
                        $('#related_request').addClass('in');
                        $('#related_request').addClass('active');
                        $scope.relatedrequestsdiv=true;
                  }
          });
   }
   
   $scope.getalllivefeedsother = function() {
        $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/allotherrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
             }).success(function(data) {
                  $("#loader").hide();
                  if(data.related_request_exist==1){
                    $scope.allotherdiv=false;
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherrequests=data.relatedrequests;
                 }else{
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherdiv=true;
                 }
         });
    }
    
  $scope.openrequesthistorytab=function(divid){
   $("#loader").show();
    if(divid=='allother'){
        $scope.allotherdiv=true;
        $scope.relatedrequestsdiv=true;
        $scope.filterresult();
        $('#hidden_activetab').val(2);
    }
    if(divid=='related_request'){
        $scope.relatedrequestsdiv=true;
        $scope.getalllivefeeds();
        $('#hidden_activetab').val(1);
    }
     $('.requesthistoryli').removeClass('in');
     $('.requesthistoryli').removeClass('active');
     $(".tab-pane").hide();
     $("#"+divid).fadeIn();

     $('.tab-pane').removeClass('in');
     $('.tab-pane').removeClass('active');

     $('#'+divid+"-tab").addClass('in');
     $('#'+divid+"-tab").addClass('active');
   }
   $scope.getalllivefeeds();
   
   $scope.openfilter=function(){
     var filtertagstring=$('#filtertagstring').val();
     if(filtertagstring != '')
     {
      $('#filter_tag').slideToggle('slow');
     }
      $('#filter-cat').slideToggle('slow');
   }
   
   $scope.getallcategories = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allparentcategories = data.allcategories;
        });
    }
    $scope.getallcategories();
    
    $scope.getsubcats = function(parentid,catname) {
       $('#filtertagstring').val('1');
       $('#filter_tag').show();
       var flag=false;
       $( ".selectedmaincat" ).each(function( index ) {
         if($(this).attr('id') == 'selectedmaincat_'+parentid)
         {
           flag=true;
         }
       });
       if(!flag)
       {
        $('#selectedtags').prepend($compile('<li id="selectedmaincat_'+parentid+'" class="selectedmaincat"><a href="javascript:void(0);">'+catname+' <span class="glyphicon glyphicon-remove" aria-hidden="true" ng-click="removemaincat(\''+catname+'\',\''+parentid+'\')"></span></a><input type="hidden" value="'+parentid+'" class="hidden_main_cat"></li>')($scope));
       
        $('#maincat'+parentid).addClass('act');
        $('#maincat'+parentid).children('a').append('&nbsp;&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        $scope.filterresult();
        $scope.getsubcategories(parentid);
      }
    }
    
    $scope.filterresult=function(){
       var filtermaincatstring='';
       var filtersubcatstring='';
       $( ".hidden_main_cat" ).each(function( index ) {
           filtermaincatstring+=$(this).val()+',';
       });
       $( ".hidden_sub_cat" ).each(function( index ) {
           filtersubcatstring+=$(this).val()+',';
       });
       if(filtermaincatstring!='')
       {
         $("#loaderfilter").show();
         $('.filterresdiv').hide();
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getfilterresults",
            data: $.param({'main_cats': filtermaincatstring,'sub_cats':filtersubcatstring,'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         }).success(function(data) {
            $("#loaderfilter").hide();
            $("#loader").hide();
            $('.filterresdiv').show();
            if(data.filter_result_exist==1){
                $scope.allotherdiv=false;
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#allother').addClass('in');
                $('#allother').addClass('active');
                $scope.allotherrequests=data.relatedrequests;
            }else{
                 $('.tab-pane').removeClass('in');
                 $('.tab-pane').removeClass('active');
                 $('#allother').addClass('in');
                 $('#allother').addClass('active');
                 $scope.allotherdiv=true;
            }
         });
       }
       else
       {
         $scope.getalllivefeedsother();
       } 
    }
    
    $scope.gridOptions = {
     urlKey      :     "original_url",
     sortKey     :     "nth",
     onClicked   :     function(image) {
                        $rootScope.$emit('showbigimageslidemain', image);                        
                      },
     onBuilded   :     function() {                       
                        $scope.$apply()
                      },
     margin      :     2
   }
   
   $scope.likerequest = function(requestid){
       $rootScope.$emit('likerequestemit', requestid);
   }
    
    $scope.getsubcategories=function(parentid){
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child','parent_id':parentid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           if(data.is_categories_exist==1)
           {
              data.allcategories.forEach(function(categorydata,index){ 
               $('#subcategories').prepend($compile('<li class="parentid_'+parentid+'" id="sub_'+categorydata.Category.id+'"><a href="javascript:void(0);" ng-click="selectsubcattags(\''+categorydata.Category.id+'\',\''+categorydata.Category.name+'\',\''+parentid+'\');">'+categorydata.Category.name+'</a></li>')($scope));
             });
           }
        });
    }
    
    $scope.selectsubcattags=function(id,name,parentid){
       var flag=false;
       $( ".selectedmaincat" ).each(function( index ) {
         if($(this).attr('id') == 'selectedmaincat_'+id)
         {
           flag=true;
         }
       });
       if(!flag)
       {
        $('#selectedtags').prepend($compile('<li id="selectedmaincat_'+id+'" class="selectedmaincat selectedsubcat_'+parentid+'"><a href="javascript:void(0);">'+name+' <span class="glyphicon glyphicon-remove" aria-hidden="true" ng-click="removesubcat(\''+name+'\',\''+id+'\')"></span></a><input type="hidden" value="'+id+'" class="hidden_sub_cat"></li></li>')($scope));
      
        $('#sub_'+id).addClass('act');
        $('#sub_'+id).children('a').append('&nbsp;&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        $scope.filterresult();
      }
    }
    
    $scope.removemaincat=function(name,parentid){
       $('#selectedmaincat_'+parentid).remove();
       $('.selectedsubcat_'+parentid).remove();
       $('#maincat'+parentid).removeClass('act');
       $('#maincat'+parentid).children('a').children('span').remove();
       $('.parentid_'+parentid).remove();
       $scope.filterresult();
       if($('.selectedmaincat').length==0)
       {
        $('#filtertagstring').val('');
        $('#filter_tag').hide();
       }
    }
   
    $scope.removesubcat=function(name,id){
       $('#selectedmaincat_'+id).remove();
       $('#sub_'+id).removeClass('act');
       $('#sub_'+id).children('a').children('span').remove();
       $scope.filterresult();
    }
   
});


