'use strict';
/** 
  * controllers used for the user dashboard
*/
app.controller('taxinfoCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket) {
     var tagc=0;
     $scope.siteurl = myAuth.baseurl;
     $scope.youtubevideo='';
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     } 
        console.log($scope.loggedindetails.id);   
        $scope.getuserdetails = function() {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.userdata=data.userdetails;
                $scope.usertimezone=data.userdetails.timezone;
                $scope.alltimezones = data.userdetails.alltimezones;
                
            });
            
        }
        $scope.getuserdetails();
        
        $scope.getusertaxinfo = function() {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getusertaxinfo",
                data: $.param({'id': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.userdata=data.userdetails;
                
                if(data.msg_type==1)
                {
                	console.log('settax');
                	$scope.settax = true;
                	$scope.tax_info = true;
                	$scope.us = $scope.userdata.us_citizen;
                	console.log($scope.tax_info+'|'+$scope.us);
                }else{
                	$scope.settax = false;
                	$scope.forEdit=false;$scope.tax_info=false;$scope.change=false;$scope.us=undefined;
                }
                
            });
            
        }
        $scope.getusertaxinfo();
        
        $scope.setus = function(usid){
        	$scope.userdata.us_citizen = usid;
        }
        
     $scope.update_tax_info = function(){
        $scope.loading = true;
        console.log($scope);
        var FormData = {
            'id' : $scope.loggedindetails.id,
            'full_name' : $scope.userdata.full_name,
            'business_name' : $scope.userdata.business_name,
            'address' : $scope.userdata.address,
            'city' : $scope.userdata.city,
            'state' : $scope.userdata.city,
            'zip_code' : $scope.userdata.zip_code,
            'classification' : $scope.userdata.classification,
            'ssn' : $scope.userdata.ssn,
            'ein' : $scope.userdata.ein,
            'us_citizen' : $scope.userdata.us_citizen,
        };
        
        if ($scope.userdata.full_name == '' || $scope.userdata.full_name === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your full_name.');
        }
        else if ($scope.userdata.ssn == '' || $scope.userdata.ssn === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter SSN Number.');
        }
        else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/updateTaxInfo",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
               $scope.getusertaxinfo();
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.getusertaxinfo();
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }
    
    $scope.edit_tax_info = function(){
        $scope.loading = true;
        console.log($scope);
        var FormData = {
            'id' : $scope.loggedindetails.id,
            'full_name' : $scope.userdata.full_name,
            'business_name' : $scope.userdata.business_name,
            'address' : $scope.userdata.address,
            'city' : $scope.userdata.city,
            'state' : $scope.userdata.city,
            'zip_code' : $scope.userdata.zip_code,
            'classification' : $scope.userdata.classification,
            'ssn' : $scope.userdata.ssn,
            'ein' : $scope.userdata.ein,
            'us_citizen' : $scope.userdata.us_citizen,
        };
        
        if ($scope.userdata.full_name == '' || $scope.userdata.full_name === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your full_name.');
        }
        else if ($scope.userdata.ssn == '' || $scope.userdata.ssn === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter SSN Number.');
        }
        else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/updateTaxInfo",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
               $scope.getusertaxinfo();
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.getusertaxinfo();
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }
    
    $scope.update = function(){
        $scope.loading = true;
        var FormData = {
            'id' : $scope.loggedindetails.id,
            'username' : $scope.userdata.username,
            'email' : $scope.userdata.email,
            'password' : $scope.password
        };
        
        if ($scope.userdata.username == '' || $scope.userdata.username === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your username.');
        }
        else if ($scope.userdata.email == '' || $scope.userdata.email === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter email.');
        }
        else if ($scope.password != '' && ($scope.password!=$scope.confpassword) ) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password and confirm password not matched.');
        }
        else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/updateAccountSetting",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.userdata=data.userdetails;
                    $scope.usertimezone=data.userdetails.timezone;
                    $cookieStore.put('users', data.userdetails);
                    $cookieStore.put('isactivation', 1);
                    setTimeout(function()
                    {
                        $scope.$apply(function() {
                            myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                            $scope.loggedindetails = myAuth.getUserNavlinks();
                            $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                            
                        });
                    }, 2000);
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }
    
    
    $scope.$on('fileuploaddone', function(e, data){
       var beforefiles=$('#hiddenfiles').val();
       if(beforefiles==1){
           var filejson=[data.result.files[0].name];
       }
       else
       {
           var filejson;
           var filearray=$.parseJSON(beforefiles);
           var arr = $.map(filearray, function(el) { return el; });
           arr.push(data.result.files[0].name);
           filejson=arr;
      }
      $('#hiddenfiles').val(JSON.stringify(filejson));
   }); 
   
   $scope.$on("deleterequestfile", function(event, message){
      var filejson=[];
      var beforefiles=$.parseJSON($('#hiddenfiles').val());
      for (var i = 0; i < beforefiles.length; i++) {
          if (beforefiles[i] != message) {
              filejson.push(beforefiles[i]);
          }
      }
      if(filejson.length==0)
      {
        $('#hiddenfiles').val('1');
      }
      else
      {
        $('#hiddenfiles').val(JSON.stringify(filejson));
      }
   });
   
   $scope.switchToMentor= function(){
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               $http({
                 method: "POST",
                 url: $rootScope.serviceurl + "users/changerole",
                 data: $.param({'userid': $scope.loggedindetails.id,'role':'mentor'}),
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               }).success(function(datauser) {
               });
                if(data.msg_type==1 && data.is_profile_completed==1)
                {
                  data.userdetails.role=2;
                  $cookieStore.put('users', data.userdetails);
                  myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                  $scope.loggedindetails = myAuth.getUserNavlinks();
                  $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                  $location.path("/frontend/mentor-dashboard");
                }
                else if(data.msg_type==1 && data.is_profile_completed==0)
                {
                  $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/logoutuser",
                        data: $.param({'userid': $scope.loggedindetails.id}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function(logoutdata) {
                        $cookieStore.put('users', null);
                        $scope.loggedindetails = '';
                        $scope.loggedin = false;
                        $scope.onlinestat=false;
                        $scope.notloggedin = true;
                        $location.path("/frontend/mentor-signup-step1/"+data.encoded_id); 
                  });
                }
        });
    }
    
    /*$scope.dashboardload = function(){
        var isactivation = $cookieStore.get('isactivation');
        if (isactivation == 1) {
            $scope.getallcategoriesstep1();
            $scope.whytolike();
        }
       $('#dropdown-menu').hide();
       $cookieStore.put('isactivation', 0);
    }*/
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {
        var idx = $scope.selection.indexOf(cheboxkey);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };

    
   // $scope.dashboardload();
});


