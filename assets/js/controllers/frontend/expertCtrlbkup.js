'use strict';
/** 
  * controllers used for the user dashboard
*/
app.controller('expertCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket) {
    
    
     var tagc=0;
     $scope.siteurl = myAuth.baseurl;
     $scope.youtubevideo='';
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     } 
     
        console.log($scope.loggedindetails.id);   
        $scope.getuserdetails = function() {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.userdata=data.userdetails;
                $scope.usertimezone=data.userdetails.timezone;
                $scope.alltimezones = data.userdetails.alltimezones;
                
            });
            
        }
        $scope.getuserdetails();
     $scope.loadTags = function(query) {

        return $http.get($rootScope.serviceurl + "categories/getsubcategories/" + query);
    }
    
    $scope.getallcategoriesstep1 = function() {
        
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
console.log(data);           
            $scope.allcategories = data.allcategories;

        });
    }
    
    $scope.fetchexpertise = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getexpert",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
console.log(data);
            $scope.expertisetabs = data;

        });

    }
    $scope.getallcategoriesstep1();
    $scope.fetchexpertise();
    
   
   
   $scope.values = function(text, id) {

        var previdx = $('.active').children('a').attr('id');
        var tprevid = previdx.split('__');
        var previd = tprevid[tprevid.length - 1];
        $scope.formattedjson =
                {
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating,
                    "tagname": previd
                    , "expertise_related_tags": $scope.expertise_related_tags};
console.log($scope.expertise_related_tags);
        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));


        var autopopulate = $("#hid_" + text + "_hidden").val();

        if (autopopulate != '') {
            var parsed = jQuery.parseJSON(autopopulate);

            $scope.expertise_related_tags = parsed.expertise_related_tags;
            $scope.expertise_experience = parsed.expertise_experience;
            $scope.expertise_moredetails = parsed.expertise_moredetails;
            $scope.expertise_rating = parsed.expertise_rating;
        } else {
            $scope.expertise_related_tags = [];
            $scope.expertise_experience = '';
            $scope.expertise_moredetails = '';
            $scope.expertise_rating = '';
        }




        $("#h2tag").html(text);
        $("#spantag").html(text);
        $(".tabs").removeClass('active');
        $("#maintag__" + id).parent('li').addClass('active');

    }
    
    $scope.addtobodyexpertise = function() {
        $(".foradd").removeClass('active');

        var tags = $scope.expertise_tags;
        $(".tabs").removeClass('active');
        $(".foradd").removeClass('active');
        $("#addexperisemodal").modal('hide');
        var exist = $('.tabs').length;


        tags.forEach(function(locationdata, index) {
            var flg = true;
            $(".tabs").each(function(index) {

                if (locationdata.text.toUpperCase() == $(this).children('a').attr('id').toUpperCase()) {
                    flg = false;
                }
            });

            if (flg) {
                if (index == 0) {
                    var val = '{"tagname":"' + locationdata.text + '","expertise_related_tags":[],"expertise_experience":"","expertise_moredetails":"","expertise_rating":""}';
                    var html = '<li role="presentation" class="tabs active" >';
                    html += ' <a  ng-click="values(\'' + locationdata.text + '\',' + exist + ');" id="maintag__' + locationdata.text + '" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" class="ng-binding" style="cursor:pointer;">' + locationdata.text + '</a>';
                    html += '<input type="hidden" id="hid_' + locationdata.text + '_hidden" class="hiddens"  >';
                    html += '</li>';
                    $("#h2tag").html(locationdata.text);
                    $("#spantag").html(locationdata.text);
                } else {
                    var val = '{"tagname":"' + locationdata.text + '","expertise_related_tags":[],"expertise_experience":"","expertise_moredetails":"","expertise_rating":""}';
                    var html = '<li role="presentation" class="tabs">';
                    html += ' <a  ng-click="values(\'' + locationdata.text + '\',' + exist + ');" id="maintag__' + locationdata.text + '" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" class="ng-binding" style="cursor:pointer;">' + locationdata.text + '</a>';
                    html += '<input type="hidden" id="hid_' + locationdata.text + '_hidden" class="hiddens"  >';
                    html += '</li>';
                }

            } else {
                $(".tabs").each(function(index) {
                    if (index == 0) {
                        $(this).addClass('active');

                        $("#h2tag").html($(this).children('a').attr('id'));
                        $("#spantag").html($(this).children('a').attr('id'));
                    }

                });
            }

            $("#myTab li:last").before($compile(html)($scope));
            exist = exist + 1;
        });
        $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';


    }

    $scope.openaddmodal = function() {
        $(".foradd").removeClass('active');

        var previd = $('.active').children('a').attr('id');

        $scope.formattedjson =
                {"tagname": previd
                    , "expertise_related_tags": $scope.expertise_related_tags,
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating};



        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));



        $scope.expertise_tags = '';
        $("#addexperisemodal").modal('show');



    }

    $scope.closemodal = function() {
        $(".foradd").removeClass('active');
        $("#addexperisemodal").modal('hide');
        $(".tabs").each(function(index) {
            if (index == 0) {
                $(this).addClass('active');

                $("#h2tag").html($(this).children('a').attr('id'));
                $("#spantag").html($(this).children('a').attr('id'));
            }

        });

    }
    $scope.remove = function() {
        $('.active').remove();
        $(".foradd").removeClass('active');
        $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';
        var thisid;
        $(".tabs").each(function(index) {
            if (index == 0) {
                $(this).addClass('active');
                thisid = $(this).children('a').attr('id');
                $("#h2tag").html($(this).children('a').attr('id'));
                $("#spantag").html($(this).children('a').attr('id'));
            }

        });




        var autopopulate = $("#hid_" + thisid + "_hidden").val();

        if (autopopulate != '') {
            var parsed = jQuery.parseJSON(autopopulate);

            $scope.expertise_related_tags = parsed.expertise_related_tags;
            $scope.expertise_experience = parsed.expertise_experience;
            $scope.expertise_moredetails = parsed.expertise_moredetails;
            $scope.expertise_rating = parsed.expertise_rating;
        } else {
            $scope.expertise_related_tags = [];
            $scope.expertise_experience = '';
            $scope.expertise_moredetails = '';
            $scope.expertise_rating = '';
        }


    }
    
    $scope.expertsubmit = function() {
        console.log($scope);
        /*$scope.loading = true;
        $(".foradd").removeClass('active');
        var arr = [];
        var previd = $('.active').children('a').attr('id');

        $scope.formattedjson =
                {"tagname": previd,
                    "expertise_related_tags": $scope.expertise_related_tags,
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating
                };

        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));

        $(".hiddens").each(function(index) {
            arr.push($(this).val());
        });
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/updateExpert",
            data: $.param({'userid': $scope.loggedindetails.id, 'tagdetails': arr}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 1)
            {
                $scope.loading = false;
                $scope.step1alertmesage = true;
                $scope.loading = false;
                $scope.alert = myAuth.addAlert('success', data.msg);
                
            } else {
                $scope.loading = false;
                $scope.step1alertmesage = true;
                $scope.loading = false;
                $scope.alert = myAuth.addAlert('danger', data.msg);
            }

        });*/
    }
   
   $scope.$on("deleterequestfile", function(event, message){
      var filejson=[];
      var beforefiles=$.parseJSON($('#hiddenfiles').val());
      for (var i = 0; i < beforefiles.length; i++) {
          if (beforefiles[i] != message) {
              filejson.push(beforefiles[i]);
          }
      }
      if(filejson.length==0)
      {
        $('#hiddenfiles').val('1');
      }
      else
      {
        $('#hiddenfiles').val(JSON.stringify(filejson));
      }
   });
   
   $scope.switchToMentor= function(){
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               $http({
                 method: "POST",
                 url: $rootScope.serviceurl + "users/changerole",
                 data: $.param({'userid': $scope.loggedindetails.id,'role':'mentor'}),
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               }).success(function(datauser) {
               });
                if(data.msg_type==1 && data.is_profile_completed==1)
                {
                  data.userdetails.role=2;
                  $cookieStore.put('users', data.userdetails);
                  myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                  $scope.loggedindetails = myAuth.getUserNavlinks();
                  $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                  $location.path("/frontend/mentor-dashboard");
                }
                else if(data.msg_type==1 && data.is_profile_completed==0)
                {
                  $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/logoutuser",
                        data: $.param({'userid': $scope.loggedindetails.id}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function(logoutdata) {
                        $cookieStore.put('users', null);
                        $scope.loggedindetails = '';
                        $scope.loggedin = false;
                        $scope.onlinestat=false;
                        $scope.notloggedin = true;
                        $location.path("/frontend/mentor-signup-step1/"+data.encoded_id); 
                  });
                }
        });
    }
    
    /*$scope.dashboardload = function(){
        var isactivation = $cookieStore.get('isactivation');
        if (isactivation == 1) {
            $scope.getallcategoriesstep1();
            $scope.whytolike();
        }
       $('#dropdown-menu').hide();
       $cookieStore.put('isactivation', 0);
    }*/
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {
        var idx = $scope.selection.indexOf(cheboxkey);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {

        var idx = $scope.selection.indexOf(cheboxkey);

        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#categories_selected").val($scope.selection.join());
        $scope.categories_selected = $scope.selection.join();
    };


    
    
    
    

    
   // $scope.dashboardload();
});



