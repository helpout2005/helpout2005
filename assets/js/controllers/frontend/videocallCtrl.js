'use strict';
/** 
  * controllers used for the open request
*/
app.controller('videocallCtrl', function ($rootScope, $stateParams,$scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     $scope.opentok_id = $stateParams.id
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     if(!$scope.opentok_id)
     {
         $location.path("/frontend/home");
     }
    $scope.my_video_div = '<div class="img-responsive video_div_css" id="my_video_div"></div>';
    $scope.friends_video_div = '<div class="img-responsive video_div_css" id="friends_video_div"></div>';
   
    $scope.getVideoDetails = function(){
        console.log("Api Key ==== " ,myAuth.OpenTokApiKey);
         //$('.my_video_div_container').html($scope.my_video_div);
           // $('.friends_div_container').html($scope.friends_video_div);
            //$scope.$apply();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "opentoks/get_details",
            data: $.param({'opentok_id': $scope.opentok_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.status == 'danger')
            {
                $location.path("/frontend/home");
            }
            else
            {
                console.log("Api Key ==== " ,myAuth.OpenTokApiKey);
                $scope.OpenTokSession = OT.initSession(myAuth.OpenTokApiKey, data.details.sessionid);
                $scope.OpenTokSession.connect(data.details.tokenid, function (err) {            
                    if (!err) {
                        console.log('hi1');
                        $scope.OpenTokPublisher = OT.initPublisher( "my_video_div" );
                        $scope.OpenTokSession.publish( $scope.OpenTokPublisher, function(err1) {
                           console.log('hi2');
                           if (err1) return console.log('publishing error');
                           console.log('Publiched ============ ');
                         });
                         $scope.trackStream(); 
                    }
                    else {
                        console.log('hi error');
                        //enableButtons();
                    }
                });
                //$scope.trackStream(); 
            }
        })
    }
    $scope.getVideoDetails();
    
    $scope.trackStream = function(){
         
         $scope.OpenTokSession.on("streamCreated", function (event) {
                //$scope.OpenTokPublisher = OT.initPublisher( "my_video_div");
                $scope.OpenTokSession.subscribe( event.stream, 'friends_video_div',{height:'600px',width:'100%',marginbottom:'15px'} );
                console.log("New stream in the session: ", event.stream);
           });
    }
    
    $scope.disconnectVideoCall = function(data){
        if($scope.OpenTokPublisher)
        {
            //$scope.OpenTokSession.unpublish($scope.OpenTokPublisher);
            //$scope.OpenTokPublisher.destroy();
            //$scope.$apply();         
            //alert('11111111111');
            socket.emit("disconnect_video",data);
            $location.path("/frontend/home");
        }
    }
     socket.on('disconnect_all_call', function(data) {
        if($scope.OpenTokPublisher)
        {
            $scope.OpenTokPublisher.publishVideo(false);
            $scope.OpenTokPublisher.publishAudio(false);
            $scope.OpenTokSession.unpublish($scope.OpenTokPublisher);
            //socket.emit("disconnect_video",data);
            $scope.OpenTokPublisher.destroy();
            $scope.$apply(); 
            //alert('222222222222');
            $location.path("/frontend/home");
        }
     })
    
     
});


