'use strict';
/** 
  * controllers used for the user dashboard
*/
app.controller('userdashboardCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket) {
     var tagc=0;
     $scope.siteurl = myAuth.baseurl;
     $scope.youtubevideo='';
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
	 console.log($scope.isUserLoggedIn);
	 $scope.imgtype = $scope.loggedindetails.imgtype;
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
    $scope.getEscrowedAmount = function(){
		 $http({
            method: "POST",
            url: $rootScope.serviceurl + "transfers/get_my_escrowed_total/" + $scope.loggedindetails.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(amount){
			$scope.escrowed_amount = amount; 
        })
	}
	$scope.getEscrowedAmount();
	
    $scope.openGetHelp= function(){
      $scope.request_file_edit_div=false;
      $scope.allrequestfiles='';
      $( ".search_sec" ).slideDown(700);
    }
    
    $scope.$on('fileuploaddone', function(e, data){
       var beforefiles=$('#hiddenfiles').val();
       if(beforefiles==1){
           var filejson=[data.result.files[0].name];
       }
       else
       {
           var filejson;
           var filearray=$.parseJSON(beforefiles);
           var arr = $.map(filearray, function(el) { return el; });
           arr.push(data.result.files[0].name);
           filejson=arr;
      }
      $('#hiddenfiles').val(JSON.stringify(filejson));
   }); 
   
   $scope.$on("deleterequestfile", function(event, message){
      var filejson=[];
      var beforefiles=$.parseJSON($('#hiddenfiles').val());
      for (var i = 0; i < beforefiles.length; i++) {
          if (beforefiles[i] != message) {
              filejson.push(beforefiles[i]);
          }
      }
      if(filejson.length==0)
      {
        $('#hiddenfiles').val('1');
      }
      else
      {
        $('#hiddenfiles').val(JSON.stringify(filejson));
      }
   });
   
   $scope.switchToMentor= function(){
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               $http({
                 method: "POST",
                 url: $rootScope.serviceurl + "users/changerole",
                 data: $.param({'userid': $scope.loggedindetails.id,'role':'mentor'}),
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               }).success(function(datauser) {
               });
                if(data.msg_type==1 && data.is_profile_completed==1)
                {
                  data.userdetails.role=2;
                  $cookieStore.put('users', data.userdetails);
                  myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                  $scope.loggedindetails = myAuth.getUserNavlinks();
                  $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                  $location.path("/frontend/mentor-dashboard");
                }
                else if(data.msg_type==1 && data.is_profile_completed==0)
                {
                  $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/logoutuser",
                        data: $.param({'userid': $scope.loggedindetails.id}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function(logoutdata) {
                        $cookieStore.put('users', null);
                        $scope.loggedindetails = '';
                        $scope.loggedin = false;
                        $scope.onlinestat=false;
                        $scope.notloggedin = true;
                        $location.path("/frontend/mentor-signup-step1/"+data.encoded_id); 
                  });
                }
        });
    }
    
    $scope.dashboardload = function(){
        var isactivation = $cookieStore.get('isactivation');
        if (isactivation == 1) {
            $scope.getallcategoriesstep1();
            $scope.whytolike();
        }
       $('#dropdown-menu').hide();
       $cookieStore.put('isactivation', 0);
    }
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {
        var idx = $scope.selection.indexOf(cheboxkey);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };

    $scope.whytolike = function() {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/whytolikelist",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.whytolikelists = data.whytolike;
            });
    }

    $scope.getallcategoriesstep1 = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allcategories = data.allcategories;
            $('#account-activation-step-1').modal('show');
        });
    }
    
    $scope.getallsubcategories = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allsubcategories = data.allcategories;
        });
    }
    $scope.getallsubcategories();
    
    $scope.getallcategories = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allparentcategories = data.allcategories;
        });
    }
    $scope.getallcategories();

   $rootScope.$on("updatementorlist", function(event, message){
	console.log(event+'|'+message);
	$scope.getallmentors();
   });
    
    $scope.getallmentors = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getallmentors",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            //console.log('=================== All mentors =======',data);
            if(data.is_experts_available==0)
            {
              $scope.nomentoravailable=true;
            }
            else
            {
              $scope.nomentoravailable=false;
            }
            $scope.allrelatedmentors=data.experts;
        });
    }
    $scope.getallmentors();
    
    $scope.getallactivehelps = function() {
        $scope.loggedindetails = myAuth.getUserNavlinks();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getallactivehelps",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.is_active_request_exist==0)
            {
              $scope.noactiverequest=true;
            }
            else
            {
              $scope.noactiverequest=false;
            }
            //console.log(data.allrequests);
            $scope.allactiverequests=data.allrequests;
            $scope.noofactiverequests=data.allrequests.length;
        });
    }
    $scope.getallactivehelps();
    
    $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
    }
    
    $rootScope.$on("updaterequestlivefeedonpage", function(event, message){
     $scope.getallactivehelps();
    });
    
    $rootScope.$on("getupdateddashboard", function(event, message){
        console.log('Background call');
        $scope.getallactivehelps();
    });
    
    $scope.getnoofprevioushelps = function() {
        $scope.loggedindetails = myAuth.getUserNavlinks();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getnoofprevioushelps",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           $scope.noofpreviousrequests=data.allrequests.length;
        });
    }
    $scope.getnoofprevioushelps();
    
    $scope.getfinishedrequests = function() {
        $scope.loggedindetails = myAuth.getUserNavlinks();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getfinishedrequests",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.is_finished_request_exist==0)
            {
              $scope.nofinishedrequest=true;
            }
            else
            {
              $scope.nofinishedrequest=false;
              $scope.allfinishedrequests=data.allrequests;
            }
        });
    }
    $scope.getfinishedrequests();
    
    $scope.getcancelledrequests = function() {
        $scope.loggedindetails = myAuth.getUserNavlinks();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getcancelledrequests",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.is_cancel_request_exist==0)
            {
              $scope.nocancelrequest=true;
            }
            else
            {
              $scope.nocancelrequest=false;
              $scope.allcancelledrequests=data.allrequests;
            }
        });
    }
    $scope.getcancelledrequests();
    
    $scope.mark_as_solved=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/mark_as_solved",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
           if(data.type==0)
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('danger', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
           }
           else
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('success', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
             $scope.getallactivehelps();
             $scope.getnoofprevioushelps();
             $scope.getfinishedrequests();
             $scope.getcancelledrequests();
           }
       });
    }
    
    $scope.mark_as_cancel=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/mark_as_cancel",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
           if(data.type==0)
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('danger', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
           }
           else
           {
             $scope.changerequeststatusmessage=true;
             $scope.alert = myAuth.addAlert('success', data.msg);
             setTimeout(function()
             {
               $scope.changerequeststatusmessage=false;
             }, 1000);
             socket.emit("update_request_feed", requestid);
             $scope.getallactivehelps();
             $scope.getnoofprevioushelps();
             $scope.getfinishedrequests();
             $scope.getcancelledrequests();
           }
       });
    }
    
    $scope.make_active_menu=function(divid){
      $('.profile_left_menu').removeClass('act');
      $('#'+divid).addClass('act');
      if(divid=='activereq')
      {
        $('.activereq').show();
        $('.requesthistory').hide();
        $('.longtermmentorship').hide();
        $('.livehelpoutmonthly').hide();
      }
      else if(divid=='reqhistory')
      {
        $('.activereq').hide();
        $('.requesthistory').show();
        $('.longtermmentorship').hide();
        $('.livehelpoutmonthly').hide();
      }
      else if(divid=='longtermmentorshps')
      {
        $('.activereq').hide();
        $('.requesthistory').hide();
        $('.longtermmentorship').show();
        $('.livehelpoutmonthly').hide();
      }
      else if(divid=='livehelpuoumnthly')
      {
        $('.activereq').hide();
        $('.requesthistory').hide();
        $('.longtermmentorship').hide();
        $('.livehelpoutmonthly').show();
      }
    }
    
    $scope.openrequesthistorytab=function(divid){
      $('.requesthistoryli').removeClass('active');
      $('#li'+divid).addClass('active');
      $('.requesthistrydiv').removeClass('in');
      $('.requesthistrydiv').removeClass('active');
      $('#'+divid).addClass('in');
      $('#'+divid).addClass('active');
    }
    
    $scope.openlongtermmentorshiptab=function(divid){
      $('.longtermmentorshipli').removeClass('active');
      $('#li'+divid).addClass('active');
      $('.lngtrmmentrshpdiv').removeClass('in');
      $('.lngtrmmentrshpdiv').removeClass('active');
      $('#'+divid).addClass('in');
      $('#'+divid).addClass('active');
    }
    
    $scope.deleteFile = function(fileid,filename,type) {
      if(confirm('Are you sure you want to delete this file?'))
      {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/deleterequestFile",
            data: $.param({'fileid': fileid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.type=='success')
            {
              $http({
                    method: "POST",
                    url: $scope.siteurl + "php/deletefiles.php",
                    data: $.param({'filename': filename,'type':type}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              }).success(function(data) {
                $('#reqfilediv'+fileid).remove();
              });
            }
            else
            {
              
            }
        });
      }
    }
    
   $scope.gridOptions = {
     urlKey      :     "original_url",
     sortKey     :     "nth",
     onClicked   :     function(image) {
                        $rootScope.$emit('showbigimageslidemain', image);                        
                      },
     onBuilded   :     function() {                       
                        $scope.$apply()
                      },
     margin      :     2
   }
    
    $scope.edit_active_request=function(requestid){
       $http({
           method: "POST",
           url: $rootScope.serviceurl + "helps/getrequestdetails",
           data: $.param({'request_id': requestid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       }).success(function(data) {
          $scope.editrequestid=requestid;
          $scope.needhelptitle=data.allrequests.title;
          $scope.requestdetails=data.allrequests.details;
          $('#allselectedcats').val(data.allrequests.selectedcats);
          $('#allselectedcatsids').val(data.allrequests.selectedsubcatsids);
          $('#allselectedcatscats').val(data.allrequests.selectedmaincats);
          $('#allselectedcatscatsids').val(data.allrequests.selectedmaincatsids);
          $('#budget').val(data.allrequests.budget);
          $(".rotate_cat").removeClass("down"); 
          $(".rotate_cat").addClass("down"); 
          $("#catfild_on_off_cat").show("slow");
          $(".rotate").removeClass("down"); 
          $(".rotate").addClass("down")  ; 
          $("#catfild_on_off").show("slow");
          $('.catgory_holder_cat').html('');
          $('.catgory_holder').html('');
          $( "#gotonext" ).css("opacity","1");
          $('.price_div').removeClass("active"); 
          $('#price_div_'+data.allrequests.budget).addClass("active"); 
          $scope.request_file_edit_div=true;
          $scope.allrequestfiles=data.allrequests;
          $( ".search_sec" ).slideDown(700);
          $.each( data.allrequests.selectedmaincatsarr, function( key, value ) {
            if(value.id==0)
            {
              $('.catgory_holder_cat').prepend($compile('<div type="button" class="btn" id="btnn'+value.cat+'_'+tagc+'"><span>'+value.cat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCatcat(\''+0+'\',\''+value.cat+'_'+tagc+'\')"></i></div>')($scope));
              tagc++;
            }
            else
            {
               $('.catgory_holder_cat').prepend($compile('<div type="button" class="btn" id="btnn'+value.id+'"><span>'+value.cat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCatcat(\''+value.id+'\',\''+value.cat+'\')"></i></div>')($scope));
            }
        });
        $.each( data.allrequests.selectedcatsarr, function( key, value ) {
            if(value.id==0)
            {
              $('.catgory_holder').prepend($compile('<div type="button" class="btn" id="btnn'+value.cat+'_'+tagc+'"><span>'+value.cat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCat(\''+0+'\',\''+value.cat+'_'+tagc+'\')"></i></div>')($scope));
              tagc++;
            }
            else
            {
               $('.catgory_holder').prepend($compile('<div type="button" class="btn" id="btnn'+value.id+'"><span>'+value.cat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCat(\''+value.id+'\',\''+value.cat+'\')"></i></div>')($scope));
            }
       });
       var parentids=$("#allselectedcatscatsids").val();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
            data: $.param({'type': 'parent','parent_ids':parentids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allparentcategories = data.allcategories;
        });
        var childids=$("#allselectedcatsids").val();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
            data: $.param({'type': 'child','parent_ids':parentids,'childids':childids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allsubcategories = data.allcategories;
        });
     });
  }
  $scope.getSubCats = function(parentids){
      $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
            data: $.param({'type': 'parent','parent_ids':parentids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allparentcategories = data.allcategories;
        });
  }

  $scope.next = function(divID, type) {
        if (type == '1')
        {
            $scope.loading = true;
            if (($scope.selectedcats == '' || $scope.selectedcats === undefined))
            {
                $scope.loading = false;
            }
            else
            {
                $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/updateuserrequirementcats",
                    data: $.param({'userid': $scope.loggedindetails.id, 'selectedcats': $scope.selectedcats, 'chosencats': $scope.tags}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        setTimeout(function()
                        {
                            $('#' + divID).modal('show');
                        }, 500);
                    }
                });
            }
        }
        else if (type == '2')
        {
            if ($scope.expertlabel == '' || $scope.expertlabel === undefined)
            {
            }
            else
            {
                $scope.loading = true;
                $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/updateexpertlabel",
                    data: $.param({'userid': $scope.loggedindetails.id, 'lavel': $scope.expertlabel}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        setTimeout(function()
                        {
                            $('#' + divID).modal('show');
                        }, 500);
                    }
                });
            }
        }
        else if (type == '')
        {
            $('.modal').modal('hide');
            setTimeout(function()
            {
                $('#' + divID).modal('show');
            }, 500);
        }
    }

    $scope.done = function() {
        if ($scope.selectedwhy == '' || $scope.selectedwhy === undefined) {
        }else{
           $scope.loading = true;
           $scope.loggedindetails = myAuth.getUserNavlinks();
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/updatewhytolike",
                    data: $.param({'userid': $scope.loggedindetails.id, 'updatewhytolike': $scope.selectedwhy }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.loading = false;
                        $('.modal').modal('hide');
                        $('#youtubevideo').attr('src','https://www.youtube.com/embed/NLNuv7jAIhg?autoplay=1');
                        setTimeout(function()
                        {
                            $('#step4').modal('show');
                        }, 500);
                    }
                });
        }
    }

    $scope.skipstep1 = function(divID) {
        $('#youtubevideo').attr('src','');
        $('.modal').modal('hide');
    }

    $scope.active = function(aID, catName) {
        var array = [];
        var values = $("#categories").val();
        var splitted = values.split(',');
        if ($("#helpcat" + aID).hasClass('act')) {
            $("#helpcat" + aID).removeClass('act');
            for (var i = 0; i < splitted.length; i++) {
                if (splitted[i] != catName) {
                    array.push(splitted[i]);
                }
            }
        } else {
            $("#helpcat" + aID).addClass('act');
            splitted.push(catName);
            array = splitted;
        }
        $("#categories").val(array.join());
        $scope.selectedcats = array.join();
    }
    
    $scope.chooseCat = function(aID, catName) {
        $('.catgory_holder').prepend($compile('<div type="button" class="btn" id="btnn'+aID+'"><span>'+catName+'</span> <i class="fa fa-plus rote_close" ng-click="closeCat(\''+aID+'\',\''+catName+'\')"></i></div>')($scope));
        $('#'+aID).remove(); 
        var array = [];
        var values = $("#allselectedcats").val();
        var splitted = values.split(',');
        splitted.push(catName);
        array = splitted;
        $("#allselectedcats").val(array.join());
        $scope.allselectedcatsval = array.join();
        
        var arrayid = [];
        var valuesids = $("#allselectedcatsids").val();
        var splittedid = valuesids.split(',');
        splittedid.push(aID);
        arrayid = splittedid;
        $("#allselectedcatsids").val(arrayid.join());
        $scope.allselectedcatsidval = arrayid.join();
    }
    
    $scope.chooseCatcat = function(aID, catName) {
        $('.catgory_holder_cat').prepend($compile('<div type="button" class="btn" id="btnn'+aID+'"><span>'+catName+'</span> <i class="fa fa-plus rote_close" ng-click="closeCatcat(\''+aID+'\',\''+catName+'\')"></i></div>')($scope));
        $('#'+aID).remove(); 
        var array = [];
        var values = $("#allselectedcatscats").val();
        var splitted = values.split(',');
        splitted.push(catName);
        array = splitted;
        $("#allselectedcatscats").val(array.join());
        $scope.allselectedcatscatsval = array.join();
        
        var arrayid = [];
        var valuesids = $("#allselectedcatscatsids").val();
        var splittedid = valuesids.split(',');
        splittedid.push(aID);
        arrayid = splittedid;
        $("#allselectedcatscatsids").val(arrayid.join());
        $scope.allselectedcatscatsidval = arrayid.join();
    }
    
    $scope.closeCat = function(aID, catName) {
       var array = [];
       var values = $("#allselectedcats").val();
       var splitted = values.split(',');
       for (var i = 0; i < splitted.length; i++) {
            if (splitted[i] != catName) {
                array.push(splitted[i]);
            }
       }
       $("#allselectedcats").val(array.join());
       $scope.allselectedcatsval = array.join();
       
       var arrayid = [];
       var valuesids = $("#allselectedcatsids").val();
       var splittedid = valuesids.split(',');
       for (var i = 0; i < splittedid.length; i++) {
            if (splittedid[i] != aID) {
                arrayid.push(splittedid[i]);
            }
       }
       $("#allselectedcatsids").val(arrayid.join());
       $scope.allselectedcatsidval = arrayid.join();
       
       if(aID!='0')
       {
         if($scope.extraCat=='' || $scope.extraCat===undefined)
         {
          $('#allcategoryul').prepend($compile('<li id="'+aID+'" ng-click="chooseCat(\''+aID+'\',\''+catName+'\');" class="closecatli"><a href="javascript:void(0);">'+catName+'</a></li>')($scope));
         }
         else
         {
            var extracat=$scope.extraCat.toUpperCase();
            if(catName.toUpperCase().match("^"+extracat)) {
               $('#allcategoryul').prepend($compile('<li id="'+aID+'" ng-click="chooseCat(\''+aID+'\',\''+catName+'\');" class="closecatli"><a href="javascript:void(0);">'+catName+'</a></li>')($scope));
            }
         }
         $('#btnn'+aID).fadeOut();
       }
       else
       {
          $('#btnn'+catName).fadeOut();
       }
    }
    
    $scope.closeCatcat = function(aID, catName) {
       var array = [];
       var values = $("#allselectedcatscats").val();
       var splitted = values.split(',');
       for (var i = 0; i < splitted.length; i++) {
            if (splitted[i] != catName) {
                array.push(splitted[i]);
            }
       }
       $("#allselectedcatscats").val(array.join());
       $scope.allselectedcatscatsval = array.join();
       
       var arrayid = [];
       var valuesids = $("#allselectedcatscatsids").val();
       var splittedid = valuesids.split(',');
       for (var i = 0; i < splittedid.length; i++) {
            if (splittedid[i] != aID) {
                arrayid.push(splittedid[i]);
            }
       }
       $("#allselectedcatscatsids").val(arrayid.join());
       $scope.allselectedcatscatsidval = arrayid.join();
       
       $("#allselectedcats").val('');
       $scope.allselectedcatsval = '';
       $("#allselectedcatsids").val('');
       $scope.allselectedcatsidval = '';
       $scope.extraCat='';
       $('.catgory_holder').html('');
       
       if(aID!='0')
       {
         if($scope.extraCatcat=='' || $scope.extraCatcat===undefined)
         {
          $('#allcategoryulcat').prepend($compile('<li id="'+aID+'" ng-click="chooseCatcat(\''+aID+'\',\''+catName+'\');" class="closecatlicat"><a href="javascript:void(0);">'+catName+'</a></li>')($scope));
         }
         else
         {
            var extracat=$scope.extraCatcat.toUpperCase();
            if(catName.toUpperCase().match("^"+extracat)) {
               $('#allcategoryulcat').prepend($compile('<li id="'+aID+'" ng-click="chooseCatcat(\''+aID+'\',\''+catName+'\');" class="closecatlicat"><a href="javascript:void(0);">'+catName+'</a></li>')($scope));
            }
         }
         $('#btnn'+aID).fadeOut();
       }
       else
       {
          $('#btnn'+catName).fadeOut();
       }
    }
    
    $scope.writeCats = function(){
        var catsarray = [];
        var parentids=$("#allselectedcatscatsids").val();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getselectedsubcats",
            data: $.param({'type': 'child','keyword':$scope.extraCat,'parent_ids':parentids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           var values = $("#allselectedcats").val();
           var splitted = values.split(',');
           data.allcategories.forEach(function(categorydata,index){ 
              if($.inArray(categorydata.Category.name, splitted) == -1)
              {
                catsarray.push(categorydata);
              }
           });
            $('.closecatli').remove();
            $scope.allsubcategories = catsarray;
        });
    }
    
    $scope.writeCatscats = function(){
        var catsarray = [];
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getselectedsubcats",
            data: $.param({'type': 'parent','keyword':$scope.extraCatcat}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           var values = $("#allselectedcatscats").val();
           var splitted = values.split(',');
           data.allcategories.forEach(function(categorydata,index){ 
              if($.inArray(categorydata.Category.name, splitted) == -1)
              {
                catsarray.push(categorydata);
              }
           });
            $('.closecatlicat').remove();
            $scope.allparentcategories = catsarray;
        });
    }
    
    $scope.submitTag = function(keyEvent) {
       if (keyEvent.which === 13)
       {
         if($scope.extraCat=='' || $scope.extraCat===undefined)
         {
         }
         else
         {
            $('.catgory_holder').prepend($compile('<div type="button" class="btn" id="btnn'+$scope.extraCat+'_'+tagc+'"><span>'+$scope.extraCat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCat(\''+0+'\',\''+$scope.extraCat+'_'+tagc+'\')"></i></div>')($scope));
            var parentids=$("#allselectedcatscatsids").val();
            var childids=$("#allselectedcatsids").val();
            $http({
               method: "POST",
               url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
               data: $.param({'type': 'child','parent_ids':parentids,'childids':childids}),
               headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
               $scope.allsubcategories = data.allcategories;
            });
            tagc++;
            
            var array = [];
            var values = $("#allselectedcats").val();
            var splitted = values.split(',');
            splitted.push($scope.extraCat);
            array = splitted;
            $("#allselectedcats").val(array.join());
            $scope.allselectedcatsval = array.join();
            $scope.extraCat='';
         } 
       }
    }
    
     $scope.submitTagcat = function(keyEvent) {
       if (keyEvent.which === 13)
       {
         if($scope.extraCatcat=='' || $scope.extraCatcat===undefined)
         {
         }
         else
         {
            $('.catgory_holder_cat').prepend($compile('<div type="button" class="btn" id="btnn'+$scope.extraCatcat+'_'+tagc+'"><span>'+$scope.extraCatcat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCatcat(\''+0+'\',\''+$scope.extraCatcat+'_'+tagc+'\')"></i></div>')($scope));
            var parentids=$("#allselectedcatscatsids").val();
            $http({
               method: "POST",
               url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
               data: $.param({'type': 'parent','parent_ids':parentids}),
               headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
               $scope.allparentcategories = data.allcategories;
            });
            tagc++;
            
            var array = [];
            var values = $("#allselectedcatscats").val();
            var splitted = values.split(',');
            splitted.push($scope.extraCatcat);
            array = splitted;
            $("#allselectedcatscats").val(array.join());
            $scope.allselectedcatscatsval = array.join();
            $scope.extraCatcat='';
         } 
       }
    }
    
    $scope.gotonextstep=function(keyEvent)
    {
      if (keyEvent.which === 13)
      {
        var childids=$("#allselectedcatsids").val();
        var parentids=$("#allselectedcatscatsids").val();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
            data: $.param({'type': 'child','parent_ids':parentids,'childids':childids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allsubcategories = data.allcategories;
        });
      }
    }
    
    $scope.gotonextstepclick=function()
    {
       var childids=$("#allselectedcatsids").val();
       var parentids=$("#allselectedcatscatsids").val();
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getfilteredsubcategories",
            data: $.param({'type': 'child','parent_ids':parentids,'childids':childids}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allsubcategories = data.allcategories;
        });
    }
    
    $scope.addextraCats = function(keyEvent) 
    {
         if($scope.extraCat=='' || $scope.extraCat===undefined)
         {
         }
         else
         {
            $('.catgory_holder').prepend($compile('<div type="button" class="btn" id="btnn'+$scope.extraCat+'_'+tagc+'"><span>'+$scope.extraCat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCat(\''+0+'\',\''+$scope.extraCat+'_'+tagc+'\')"></i></div>')($scope));
            $scope.getallsubcategories();
            tagc++;
            var array = [];
            var values = $("#allselectedcats").val();
            var splitted = values.split(',');
            splitted.push($scope.extraCat);
            array = splitted;
            $("#allselectedcats").val(array.join());
            $scope.allselectedcatsval = array.join();
            $scope.extraCat='';
         } 
    }
    
    $scope.addextraCatscat = function(keyEvent) 
    {
         if($scope.extraCatcat=='' || $scope.extraCatcat===undefined)
         {
         }
         else
         {
            $('.catgory_holder_cat').prepend($compile('<div type="button" class="btn" id="btnn'+$scope.extraCatcat+'_'+tagc+'"><span>'+$scope.extraCatcat+'</span> <i class="fa fa-plus rote_close" ng-click="closeCatcat(\''+0+'\',\''+$scope.extraCatcat+'_'+tagc+'\')"></i></div>')($scope));
            $scope.getallcategories();
            tagc++;
            var array = [];
            var values = $("#allselectedcatscats").val();
            var splitted = values.split(',');
            splitted.push($scope.extraCatcat);
            array = splitted;
            $("#allselectedcatscats").val(array.join());
            $scope.allselectedcatscatsval = array.join();
            $scope.extraCatcat='';
         } 
    }

    $scope.selectCategory = function(value) {

    }
    
    $scope.openwatchvideomodal= function(value) {
       $('#watchyoutubevideo').attr('src','https://www.youtube.com/embed/NLNuv7jAIhg?autoplay=1');
       setTimeout(function()
       {
            $('#watchvideomodal').modal('show');
       }, 500);
    }
    
    $scope.closewatchvideomodal= function(value) {
       $('#watchyoutubevideo').attr('src','');
       $('#watchvideomodal').modal('hide');
    }
    
    $scope.submithelp = function() {
       $scope.loggedindetails = myAuth.getUserNavlinks();
       var editrequestid=$scope.editrequestid;
       var selectedcats=$('#allselectedcats').val();
       var allselectedcatsids=$('#allselectedcatsids').val();
       var allselectedcatscats=$('#allselectedcatscats').val();
       var allselectedcatscatsids=$('#allselectedcatscatsids').val();
       var budget=$('#budget').val();
       var uploadfiles=$('#hiddenfiles').val();
       if(uploadfiles==1)
       {
       }
       else
       {
         uploadfiles=$.parseJSON(uploadfiles);
       }
       var session_type='schedule';
       var is_private=0;
       if(editrequestid=='' || editrequestid==undefined)
       {
         editrequestid=0;
       }
       if($("#c1").prop('checked') == true){
          is_private=1;
       }
       if($scope.needhelptitle=='' || $scope.needhelptitle===undefined || $scope.requestdetails=='' || $scope.requestdetails===undefined || selectedcats=='' || selectedcats===undefined || budget=='' || budget===undefined || allselectedcatscats=='' || allselectedcatscats===undefined)
       {
           if(selectedcats=='' || selectedcats===undefined)
           {
             $('.buttontext').css('color','#E90B0B');
           }
           else
           {
             $('.buttontext').css('color','#ccc');
           }
           
           if($scope.requestdetails=='' || $scope.requestdetails===undefined)
           {
             $('#requestdetails').css('border','1px solid #E90B0B');
           }
           else
           {
             $('#requestdetails').css('border','1px solid #ccc');
           }
       }
       else
       {
          $('.buttontext').css('color','#ccc');
          $('#requestdetails').css('border','border: 1px solid #ccc');
          $scope.loading = true;
          $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/posthelp",
            data: $.param({'userid': $scope.loggedindetails.id,'selectedcats':selectedcats,'budget':budget,'title':$scope.needhelptitle,'details':$scope.requestdetails,'is_private':is_private,'session_type':session_type,'allselectedcatscats':allselectedcatscats,'allselectedcatscatsids':allselectedcatscatsids,'editrequestid':editrequestid,'allselectedcatsids':allselectedcatsids,'uploadfiles':uploadfiles}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
           var helpstatus={'status':'new','userid':$scope.loggedindetails.id};
           $rootScope.$broadcast('updatelivefeed',helpstatus);
           $scope.loading = false;
               if(data.type==1)
               {
                 location.reload();
               }
          });
       }
    }
    
    

    $scope.loadTags = function(query) {
        return $http.get($rootScope.serviceurl + "categories/getsubcategories/" + query);
    }

    $scope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
    };
    
    $scope.open_active_request = function(id) {
        $('#activerequestdetails'+id).slideToggle(500);
    };

    $scope.interested_mentors = {};
    $scope.show_mentors = function(mentors,ind)
    {
        $scope.interested_mentors = mentors;
        mentors.active = !mentors.active;
        //$scope.mlist+ind = true;
        //$(btn).closest('li').find('.mentor_table').toggle();
        //$('#intereseted_mentors_modal').modal('show');       
    }
    
    $scope.accept_mentor = function(obj,pay_details){
		$scope.modal_escrow_btn = true;
		$('#close_escrow_modal').prop('disabled', true);
        $scope.acceptMentor = {}
        $scope.acceptMentor.helperid = obj.userid;
        $scope.acceptMentor.helpid = obj.help_id;
        $scope.acceptMentor.helpeeid = $scope.loggedindetails.id;
        $scope.acceptMentor.interest_id = obj.interest_id;
        $scope.acceptMentor.amount = pay_details.budget;
        $scope.loading = true;
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "assigned_helps/assign",
            data: $.param($scope.acceptMentor),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
              //$location.path('/frontend/helpees_assigns');
                $http({
				  method: "POST",
				  url: $rootScope.serviceurl + "transfers/escrow_credit",
				  data: $.param({payer_id : $scope.loggedindetails.id, receiver_id : obj.userid, amount:pay_details.budget,help_id:pay_details.id}),
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function(data) {             
				      $scope.changerequeststatusmessage = true;
				      $scope.alert = myAuth.addAlert(data.status, data.message);
				      if(data.status == 'success')
				      {
				          $http({
				                  method: "POST",
				                  url: $rootScope.serviceurl + "users/getuserdetails",
				                  data: 'userid=' + $scope.loggedindetails.id ,
				                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				              }).success(function(userdata){
								  $scope.modal_escrow_btn = false;
									$('#close_escrow_modal').prop('disabled', false);
				                  if(userdata && userdata.msg_type)
				                  {
				                      myAuth.updateUserinfo(userdata.userdetails);
				                      $cookieStore.put('users',userdata.userdetails);
				                      $scope.listView = !$scope.listView;
				                      //$scope.getAssignedHelps();
				                      $scope.loggedindetails = myAuth.getUserNavlinks();
				                      $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                                                      $rootScope.$broadcast('updatelivefeed',{'status':'new','userid':$scope.loggedindetails.id});
				                      $('.modal').modal('hide');
				                      $scope.changerequeststatusmessage=true;
								  $scope.alert = myAuth.addAlert('success', 'Mentor accepted successfully.');
								  $scope.getEscrowedAmount();
								  setTimeout(function()
								  {
									  
								    $scope.changerequeststatusmessage=false;
								  }, 1000);
								  $scope.getallactivehelps();
				                      //$location.path('/frontend/payment_success');
				                  }
				              })
				      }else if(data.redirect){
				      	$location.path('/frontend/'+data.redirect);
				      }
				})
                
                
          })
    }
    
    
    
    $scope.dashboardload();
    
    $scope.no_escrow = function(){
    		$('.modal').modal('hide');
          $('#noEscrow').modal('show');
          setTimeout(function()
                {
                  console.log('redirect');
                  $location.path('/frontend/buy_credit');
                }, 300);
    }
    
    $scope.escrow_modal = function(interested_mentor,allactiverequest){
			
    	     $scope.interested_mentor = interested_mentor;
    	     $scope.allactiverequest = allactiverequest;
    	     console.log($scope.interested_mentor);
    	     console.log($scope.allactiverequest);
    	     $('.modal').modal('hide');
			$('#escrowModal').modal('show');
			
          
          
    }
    
    $scope.check_escrow = function(interested_mentor,allactiverequest){
    		$scope.interested_mentor = interested_mentor;
    	     $scope.allactiverequest = allactiverequest;
    	     if( parseInt(allactiverequest.budget)>parseInt($scope.loggedindetails.credit_tokens) )
    	     {
    	     	$scope.no_escrow();
    	     }
    	     else{
    	     	$scope.escrow_modal(interested_mentor,allactiverequest);
    	     }
    }
    
    
    
    //--------------------------------- Chat Start ------------------------
    
    
    $scope.openChat = function(mentor){
       /* var sessionId = '2_MX40NTQ3MTk1Mn5-MTQ1Mzk2MjUxNzg2M34xdVcxWmpoYWNlN2xtMm85OGFBdGd1UHN-UH4';    
        var apiKey = '45471952';
        var session = OT.initSession(apiKey, sessionId);
		 var token = 'T1==cGFydG5lcl9pZD00NTQ3MTk1MiZzaWc9NGQ3ZTg2NjNhMjQzYzYyOTYxMjcwMzBlOTBhZWU3M2IzYWQwMDNlNDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTJfTVg0ME5UUTNNVGsxTW41LU1UUTFNemsyTWpVeE56ZzJNMzR4ZFZjeFdtcG9ZV05sTjJ4dE1tODVPR0ZCZEdkMVVITi1VSDQmY3JlYXRlX3RpbWU9MTQ1NDEzODI2NiZub25jZT0wLjcxOTc4NDkxMjkwNzYyNTQmZXhwaXJlX3RpbWU9MTQ1NjczMDIyNSZjb25uZWN0aW9uX2RhdGE9';
        session.connect(token, function (err) {           
          if (!err) {
              console.log('hiiiiiiiiii');
            //showConnection();
          }
          else {
            console.error(err);
            //enableButtons();
          }
        })*/
		$rootScope.$broadcast('open_chat',mentor);
		
		/*session.signal({
        type: mentor.username,
        data: 'helloooooooo'
      }, retchat);
	  
	  
	  function retchat(temp)
	  {
		  
		  console.log("chat sent --------",temp);
	  }*/
        /*var chatWidget = new OTSolution.TextChat.ChatWidget({
            session: session,
            container: '#chat',
            signalName: mentor.username
          });
        var token = $scope.gen_token($scope.loggedindetails.username);
        session.connect(token, function (err) {           
          if (!err) {
              console.log('hiiiiiiiiii');
            //showConnection();
          }
          else {
            console.error(err);
            //enableButtons();
          }
        });*/
      
    }
    
    /*$scope.gen_token = function(data)
     {
          var secondsInDay = 86400;
  // Credentials
        var sessionId = '2_MX40NTQ3MTk1Mn5-MTQ1Mzk2MjUxNzg2M34xdVcxWmpoYWNlN2xtMm85OGFBdGd1UHN-UH4';    
        var apiKey = '45471952';
        
        var secret = '37bc19b808ad4f67300b1fa29fb17b180587982a';
        // Token Params
        var timeNow = Math.floor(Date.now()/1000);
        var expire = timeNow+secondsInDay;
        var role = "publisher";
        //var data = "bob";
        //TB.setLogLevel(TB.DEBUG);
        // Calculation
        data = escape(data);
        var rand = Math.floor(Math.random()*999999);
        var dataString =  "session_id="+sessionId+"&create_time="+timeNow+"&expire_time="+expire+"&role="+role+"&connection_data="+data+"&nonce="+rand;
        // Encryption
        var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, secret);
        hmac.update( dataString );
        var hash = hmac.finalize();
        var preCoded = "partner_id="+apiKey+"&sig="+hash+":"+dataString;
        var token = "T1=="+$.base64.encode( preCoded );
        return token;
     }*/
    //--------------------------------- Chat End ------------------------
    
    $scope.addattchment = function(files,activereq){
        $scope.loading1 = true;
        var fd = new FormData();
        fd.append("file", files[0]);
        console.log(fd);
        $http.post("php/upload_attachments.php", fd, {
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function(temp){
            $scope.loading1 
            $('#hidden_attach_' + activereq).val(files[0]['name']);
        } );
    }
    
    $scope.addcomment=function(divid ,type, commentform){
        $scope.loading1=true;
        var temp_blocker = false;
        var comment=$("#textarea_"+divid).val();
        console.log(commentform);
        if(comment!=''){
           /*if(activereq.new_attachment)
           {
                
           }
           else
           {
               temp_blocker = true;
           }
           while(!temp_blocker)
           {
               console.log('hiiiiiii');
           }
           console.log('======================');*/
           $http({
                  method: "POST",
                  url: $rootScope.serviceurl + "comments/addComment",
                  data: $.param({'userid': $scope.loggedindetails.id,'help_id':divid,'comment':comment}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                   $scope.loading1=false;
                   var html='<div id="comment_'+data.lastid+'">';
                     html+='<div class="col-md-1 col-sm-1 col-xs-3" style="padding-left:15px;">';
                     html+='<div class="profile_img-new">';
                     html+='<img src="'+$scope.siteurl+'assets/upload/user_images/'+data.image+'">';
                     html+='</div> ';
                     html+='</div>';
                     html+='<div  class="col-md-11 col-sm-11 col-xs-9">';
                     html+=' <h2><a class="ng-binding">'+data.name+'</a>';
                     html+='<span style="margin-left:10px; font-size: 12px;">'+data.time+'</span>';
                     html+='<span style="margin-left:10px; font-size: 14px; cursor: pointer;"  ng-click="deletecomment('+data.lastid+');">X</span>';
                     html+='</h2> ';
                     html+='<p>'+comment+'</p>';
                     html+='</div>';
                     html+='</div><hr  style="clear: both;">';
                 $('.commentarea').append($compile(html)($scope));
                 $("#textarea_"+divid).val('')
                 //socket.emit("update_request_feed", data.lastid);
                 var helpstatus={'status':'new','userid':$scope.loggedindetails.id};
                 $rootScope.$broadcast('updatelivefeed',helpstatus);
              });
          }else{
              $scope.loading1=false;
              $("#textarea_"+divid).focus()
          }
    }
    
});


