'use strict'; 
/** 
  * controllers used for the open request
*/
app.controller('buycreditCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket,$window) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     
     $scope.regalertmessage = false;
     
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
    
    /*$scope.number = '4242-4242-4242-4242';
    $scope.expiry = '12/16';
    $scope.cvc = '999';*/
    $scope.number = '';
    $scope.expiry = '';
    $scope.cvc = '';
    
    $scope.packages = {};
    $scope.getCreditPackages = function(){
        $http({
                url: $rootScope.serviceurl + "packages/getAll",                
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data)
            {
               $scope.packages = data;
            });
    }
    $scope.getCreditPackages();
    
    $scope.selected = -1;
    $scope.payment_type = 'stripe';
    $scope.selectedPackage = false;
    $scope.select = function(obj,ind){
        if($scope.selected == ind)
        {
            $scope.selected = -1;
            $scope.selectedPackage = false;
        }
        else
        {
            if(ind == 0)
            {
                $scope.payment_type = 'stripe';
            }
            $scope.selected = ind;
            $scope.selectedPackage = obj.Package;
        }
    }
    
    
    $scope.stripeCallback = function (code, result) {
        $scope.amount = 50;
        if (result.error) {
            $scope.regalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', "Invalid card details.");
        }
        else
        {
            if(!$scope.selectedPackage)
            {
                $scope.regalertmessage = true;
                $scope.alert = myAuth.addAlert('danger', "Please select your package.");
            }
            else
            {
                $scope.regalertmessage = false;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "stripes/buy_credit",
                    data: 'user_id=' + $scope.loggedindetails.id + '&stripeToken=' + result.id + '&package_id=' + $scope.selectedPackage.id,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    if(data.status=='success')
                    {
                         $http({
                            method: "POST",
                            url: $rootScope.serviceurl + "users/getuserdetails",
                            data: 'userid=' + $scope.loggedindetails.id ,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function(userdata){
                            if(userdata && userdata.msg_type)
                            {
                                myAuth.updateUserinfo(userdata.userdetails);
                                $cookieStore.put('users',userdata.userdetails);
                                $location.path('/frontend/payment_success');
                            }
                        })
                        
                        /*************** Reset Form **********************/
                        $scope.number = '';
                        $scope.expiry = '';
                        $scope.cvc = '';
                        $scope.expiry_month ='';
                        $scope.expiry_year ='';
                        $scope.selected = -1;
                        $scope.selectedPackage = false;
                    }
                    $scope.regalertmessage = true;
                    $scope.alert = myAuth.addAlert(data.status, data.message);
                });
            }
        }
    }
    
    $scope.paypal_payment = function()
    {
       $http({
                method: "POST",
                url: $rootScope.serviceurl + "paypals/buy_credit",
                data: 'user_id=' + $scope.loggedindetails.id + '&package_id=' + $scope.selectedPackage.id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){	
                $scope.regalertmessage = true;
                $scope.alert = myAuth.addAlert(data.status, data.message);
                console.log(data);
                if(data.status=='success')
                {
                    $window.location.href = data.link;
                }
            });
    }
    
});


