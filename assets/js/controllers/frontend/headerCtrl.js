'use strict';
/** 
 * controllers used for the frontend header
 */
app.controller('headerCtrl', function($rootScope, $scope, $window, $http, $location, $sce, $cookies, $cookieStore, myAuth, socket, $compile, $facebook, GooglePlus, $linkedIn,$anchorScroll) {
	
	
    $scope.siteurl = myAuth.baseurl;
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
    //console.log($scope.loggedindetails);
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    
     if($scope.isUserLoggedIn){
     	socket.emit('joinuser', $scope.loggedindetails.id);
     	$scope.imgtype = $scope.loggedindetails.imgtype;
     	if($scope.loggedindetails.fbid!="")
            {
              $scope.headersiteimage = false;
            }else{
            	$scope.headersiteimage = true;
            }
     }
	
        
	
	
    $rootScope.$on("update_parent_controller", function(event, message)
    {
        $scope.loggedindetails = message;
        if ($scope.loggedindetails)
        {
            $scope.loggedin = true;
            $scope.notloggedin = false;
            $scope.onlinestatnoti=true;
            if ($scope.loggedindetails.role == 1) {
                $scope.onlinestat = false;
            } else {
                $scope.onlinestat = true;
            }
        }
        else
        {
            $scope.loggedin = false;
            $scope.notloggedin = true;
            $scope.onlinestat = false;
            $scope.onlinestatnoti=false;
        }
    });
    
    $scope.getnotifications = function() {
       
       $scope.loggedindetails = myAuth.getUserNavlinks();
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getnotifications",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.notification_exist==1)
            {
              $scope.notificationlist=true;
              $scope.notificationcount=data.allnotifications.length;
              $scope.allnotifications=data.allnotifications;
            }
            else
            {
              $scope.notificationlist=false;
              $scope.notificationcount=0;
            }
           
        });
    }
    $scope.getnotifications();

    $rootScope.$on("showbigimageslidemain", function(event, helpid) {
        $('.modal').modal('hide');
        $('#myModal-forimage').modal('show');
        var loaderhtml = '<div id="loaderimages" style="padding-top: 25px; ">';
        loaderhtml += '<div class="circle"></div>';
        loaderhtml += '<div class="circle1"></div>';
        loaderhtml += '</div>';
        $('#modal_image_body').html(loaderhtml);
        $('#loaderimages').show();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getrequestimages",
            data: $.param({'helpid': helpid.help_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $('#allimages').val(JSON.stringify(data.allfiles));
            $('#countallimages').val(data.file_count);
            $scope.startslide('1',helpid.name);
        });
    });
    
    
    $rootScope.$on("likerequestemit", function(event, requestid) {
       $scope.loggedindetails = myAuth.getUserNavlinks();
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/likerequest",
            data: $.param({'userid': $scope.loggedindetails.id,'requestid':requestid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.type==1)
            {
              socket.emit("update_request_feed", requestid);
              socket.emit("live_notification", requestid);
              $('#likecount'+requestid).html(data.likecount);
              if(data.is_like==0)
              {
                $('#likeheart'+requestid).removeClass('fa-heart');
                $('#likeheart'+requestid).addClass('fa-heart-o');
              }
              else if(data.is_like==1)
              {
                $('#likeheart'+requestid).removeClass('fa-heart-o');
                $('#likeheart'+requestid).addClass('fa-heart');
              }
            }
        });
    });
    
    socket.on('leave', function(data) {
      alert(data +" is leaving");
    });
    
    socket.on('getupdaterequestfeed', function(data) {
        $rootScope.$broadcast('updaterequestlivefeedonpage');
        $scope.$digest();
    });
    $rootScope.$on("update_live_notifications",function(){
        socket.emit("update_live_notifications");
    })
    
    socket.on('getlivenotification', function(data) {
        $scope.getnotifications();
        $scope.$digest();
    });
    
    $rootScope.$on("updateliveinterest", function(event, message) {
        socket.emit("live_interest", message);
    });
    
    socket.on('getliveinterest', function(data) {
        $scope.getnotifications();
        $scope.$digest();
    });

    $scope.startslide = function(type,clickedimage) {
        var filesimages = $('#allimages').val();
        var fileshow;
        var filearray = $.parseJSON(filesimages);
        var arr = $.map(filearray, function(el) {
            return el;
        });
        if (type == 1) {
            fileshow = arr[0];
        }
        var imagehtml = '<div id="bigparentdiv" style="max-height:500px;"><img id="bigimage" style="max-height:500px; max-width:100%;" src="' + $scope.siteurl + 'assets/frontend/uploads/requestimages/' + clickedimage + '"></div>';
        imagehtml += '<ul style="padding-left:0; margin-top:10px;" >';
        for (var i = 0; i < arr.length; i++) {
            var imgid=arr[i].replace(/\./g, "");    
            if (i == 0) {
                imagehtml += '<li class="smallimageonslider " id="'+imgid+'" ng-click="setbigimage(&quot;' + arr[i] + '&quot;);"><img src="' + $scope.siteurl + 'assets/frontend/uploads/requestimages/thumbnail/' + arr[i] + '"></li>';
            } else {
                imagehtml += '<li class="smallimageonslider" id="'+imgid+'"  ng-click="setbigimage(&quot;' + arr[i] + '&quot;);"><img src="' + $scope.siteurl + 'assets/frontend/uploads/requestimages/thumbnail/' + arr[i] + '"></li>';

            }
        }
        var imgidnew=clickedimage.replace(/\./g, ""); 
        imagehtml += '<div class="clearfix"></div></ul>';
        $('#modal_image_body').html($compile(imagehtml)($scope));
        $("#"+imgidnew).addClass('imgactive');
    }

    $scope.setbigimage = function(image) {
        var imgid=image.replace(/\./g, "");    
        $('.smallimageonslider').removeClass('imgactive');
        $('#bigparentdiv img').hide();
        var loaderhtml = '<div id="loaderimages" style="padding-top: 25px; ">';
        loaderhtml += '<div class="circle"></div>';
        loaderhtml += '<div class="circle1"></div>';
        loaderhtml += '</div>';
        $('#bigparentdiv').html(loaderhtml);
        setTimeout(function()
        {
            $('#'+imgid).addClass('imgactive');
            $("#bigparentdiv").html('<img id="bigimage" style="max-height:500px; display:none; max-width:100%;" src="' + $scope.siteurl + 'assets/frontend/uploads/requestimages/' + image + '">');
            $('#bigparentdiv img').fadeIn('slow');
        }, 1000);

    }
    
    $scope.open_notification_box = function() {
       $('#notificationbox').toggle();
    }

    $rootScope.$on("updatelivefeed", function(event, message) {
        socket.emit("live_feed", message);
    });
    

    socket.on('getlivefeed', function(data) {
        $rootScope.$broadcast('updatelivefeedonpage');
        $scope.getnotifications();
        $scope.$digest();
    });
    
    $rootScope.$on("updatehelpeedashboard", function(event) {
        socket.emit("update_helpee_dashboard");
    });
    socket.on('getupdateddashboard', function() {
        $rootScope.$broadcast('getupdateddashboard');
    });

 $rootScope.$on("mentorFblogin", function(event, message) {
		console.log('updatementorlist');
		$scope.loggedin = true;
		$scope.notloggedin = false;
		$scope.onlinestatnoti=true;
		myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               console.log($scope.loggedindetails);
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if ($scope.loggedindetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   
		                   
		                   $scope.onlinestat = true;
		                   if ($scope.loggedindetails.current_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if ($scope.loggedindetails.current_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if ($scope.loggedindetails.current_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if ($scope.loggedindetails.current_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
    });
    $rootScope.$on("mentoractivation", function(event, message) {
        socket.emit("mentor_activation", message);
    });

    socket.on('getmentoractivation', function(data) {
        $rootScope.$broadcast('updatementorlist');
        $scope.$digest();
    });


    $scope.sendtosearch = function() {
        var newval = $scope.search_keywords;
        if ($scope.search_keywords === undefined) {
            newval = '';
        } else {

            if (newval.indexOf(" ") > -1) {
                newval = newval.replace(/\ /g, "+");
            }
            if (newval.indexOf("/") > -1) {
                newval = newval.replace(/\//g, "+");
            }
        }
        $location.path("/frontend/search-result/" + newval);
    }


    $scope.loadpages = function() {
        if ($scope.loggedindetails)
        {
            $scope.loggedin = true;
            $scope.notloggedin = false;
            $scope.onlinestatnoti=true;
            if ($scope.loggedindetails.role == 1) {
                $scope.onlinestat = false;
            } else {
                $scope.onlinestat = true;
                if ($scope.loggedindetails.current_status == '1') {
                    var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
                } else if ($scope.loggedindetails.current_status == '2') {
                    var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
                }
                else if ($scope.loggedindetails.current_status == '3') {
                    var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
                }
                else if ($scope.loggedindetails.current_status == '4') {

                    var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
                }

                $("#headstatus").html(htmls + '<span class="caret"></span>');
            }
        }
        else
        {
            $scope.loggedin = false;
            $scope.onlinestat = false;
            $scope.notloggedin = true;
            $scope.onlinestatnoti=false;
        }
    }
    $scope.loadpages();

    socket.on('getlivestatus', function(data) {
        if (data.userid != $scope.loggedindetails.id) {
            $rootScope.$broadcast('updatementorlist');
            $scope.$digest();
        }
        $rootScope.$broadcast('updatesearchmentorlist');
    });

    $scope.changestatus = function(status, statusid) {
        $('.dropdown-menu2').hide();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/updatementorloginstatus",
            data: $.param({
                'userid': $scope.loggedindetails.id,
                'status': statusid
            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            var jsonstatus = {'status': statusid, 'userid': $scope.loggedindetails.id};
            socket.emit("change_status", jsonstatus);
            var html = $("." + status).html();
            $("#headstatus").html(html + '<span class="caret"></span>');
            $cookieStore.put('users', data.userdetails);
        });
    }

    $scope.showdropdownmenu = function() {
        $('.dropdown-menu1').toggle();
    };

    $scope.showdropdownmenu1 = function() {
        $('.dropdown-menu2').toggle();
    };

    $scope.gotomyprofile = function() {
        $('.dropdown-menu').hide();
        myAuth.updateUserinfo(myAuth.getUserAuthorisation());
        $scope.loggedindetails = myAuth.getUserNavlinks();
        if ($scope.loggedindetails.role == 1)
        {
            $location.path("/frontend/profile/" + $scope.loggedindetails.username);
        }
        else
        {
            $location.path("/frontend/mentor/" + $scope.loggedindetails.username);
            //$location.path("/frontend/mentor-dashboard");
        }
    };
    
    $scope.gotomysettings = function() {
        $('.dropdown-menu').hide();
        myAuth.updateUserinfo(myAuth.getUserAuthorisation());
        $scope.loggedindetails = myAuth.getUserNavlinks();
        if ($scope.loggedindetails.role == 1)
        {
            $location.path("/frontend/my-setting");
        }
        else
        {
            $location.path("/frontend/my-setting");
        }
    };

    $scope.gotomydashboard = function() {
        $('.dropdown-menu').hide();
        myAuth.updateUserinfo(myAuth.getUserAuthorisation());
        $scope.loggedindetails = myAuth.getUserNavlinks();
        if ($scope.loggedindetails.role == 1)
        {
            $location.path("/frontend/dashboard");
        }
        else
        {
            $location.path("/frontend/mentor-dashboard");
        }
    };

    $scope.gotolivefeed = function() {
        $('.dropdown-menu1').hide();
        $location.path("/frontend/live-feed");
    };
    
    $scope.gotoavailability = function() {
        $('.dropdown-menu1').hide();
        console.log('Availability Step1');
        $location.path("/frontend/availability");
    };

    $scope.gotomentor_signup = function() {
        $('.modal').modal('hide');
        $location.path("/frontend/mentor-signup");
    }

    $scope.gotohome = function() {
        $('.dropdown-menu').hide();
        $location.path("/frontend/home");
    };

    $scope.openModal = function(divId) {
        $('.modal').modal('hide');
        setTimeout(function()
        {
            $('#' + divId).modal('show');
        }, 500);
    }

    socket.on('getmentorloginstatus', function(data) {
        $rootScope.$broadcast('updatementorlist');
        $scope.$digest();
    });

    $scope.login = function() {
        $scope.loginalertmessage = false;
		if ($scope.user_username == '' || $scope.user_username === undefined || $scope.user_password == '' || $scope.user_password === undefined)
        {
            $scope.loginalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your login details.');
        }
        else
        {
            $scope.loggedindetails = '';
            $scope.loading = true;
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/loginuser",
                data: $.param({
                    'username': $scope.user_username,
                    'password': $scope.user_password
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                if (data.msg_type == '0')
                {
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                    $scope.user_password = '';
                } else {
                    $cookieStore.put('users', data.userdetails);
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.user_username = '';
                    $scope.user_password = '';
                    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                    $scope.loggedindetails = myAuth.getUserNavlinks();
					$scope.startSignal();
					
                    $scope.imgtype = $scope.loggedindetails.imgtype;
                    if($scope.loggedindetails.fbid!="")
				  {
				    $scope.headersiteimage = false;
				  }else{
				  	$scope.headersiteimage = true;
				  }
                    $scope.loggedin = true;
                    $scope.notloggedin = false;
                    $scope.onlinestatnoti=true;
                    if (data.userdetails.role == 1) {
                        $scope.onlinestat = false;
                    } else {
                        var jsonstatus = {'userid': $scope.loggedindetails.id};
                        console.log($scope.loggedindetails.fbid);
                        socket.emit('joinuser', $scope.loggedindetails.id);
                        socket.emit("mentor_login", jsonstatus);
                        $scope.onlinestat = true;
                        if (data.userdetails.login_status == 1) {
                            var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
                        } else if (data.userdetails.login_status == 2) {
                            var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
                        }
                        else if (data.userdetails.login_status == 3) {
                            var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
                        }
                        else if (data.userdetails.login_status == 4) {
                            var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
                        }
                        $("#headstatus").html(htmls + '<span class="caret"></span>');
                    }
                    $scope.getnotifications();
                    $('#myModal').modal('hide');
                    if (data.userdetails.role == 1)
                    {
                        $location.path("/frontend/dashboard");
                    }
                    else
                    {
                        $location.path("/frontend/mentor-dashboard");
                    }
                }
            });
        }
    }
    
    
    $scope.forgotpassword = function() {
        if ($scope.forgotpasswordForm.$valid) {
            if ($scope.forgotemail == '' || $scope.forgotemail === undefined)
            {
                $scope.forgetpasswordalertmessage = true;
                $scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
            }
            else
            {
                $scope.loggedindetails = '';
                $scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/forgotpassword",
                    data: $.param({
                        'forgotemail': $scope.forgotemail

                    }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.forgetpasswordalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    } else {
                        $scope.forgetpasswordalertmessage = true;
                        $scope.alert = myAuth.addAlert('success', data.msg);
                        $scope.forgotemail = '';
                    }

                });
            }
        }
    }


    $scope.signup = function() {
         $scope.signupalertmessage = false;
		if ($scope.registrationForm.$valid) {
            if ($scope.email == '' || $scope.email === undefined)
            {
                $scope.signupalertmessage = true;
                $scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
            }
            else
            {
                var emailfilter = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
                if (!(emailfilter.test($scope.email)))
                {
                    $scope.signupalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', 'Please provide valid email address.');
                }
                else
                {
                    $scope.loading = true;
                    $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/signupuser",
                        data: $.param({'email': $scope.email}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).success(function(data) {
                        $scope.loading = false;
                        if (data.msg_type == '0')
                        {
                            $scope.signupalertmessage = true;
                            $scope.alert = myAuth.addAlert('danger', data.msg);
                        } else {
                            $scope.signupalertmessage = true;
                            $scope.alert = myAuth.addAlert('success', data.msg);
                            $scope.email = '';
                        }

                    });
                }
            }
        }
    }

    socket.on('getuserlogoutstatus', function(data) {
        $rootScope.$broadcast('updatementorlist');
        $scope.$digest();
    });

    $scope.logout = function() {
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/logoutuser",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            var jsonstatus = {'userid': $scope.loggedindetails.id};
            socket.emit("user_logout", jsonstatus);
			myAuth.resetUserinfo();
            $cookieStore.put('users', null);
            myAuth.updateUserinfo(myAuth.getUserAuthorisation());
            $scope.loggedindetails = myAuth.getUserNavlinks();
            $scope.loggedindetails = '';
            $scope.loggedin = false;
            $scope.onlinestat = false;
            $scope.onlinestatnoti=false;
            $scope.notloggedin = true;
            $scope.signalStarted = false;
			$scope.chat_boxes = []; 
            $location.path("/frontend/home");
        });
    }

    $scope.redirecttopage = function(redirectto) {
        $location.path("/frontend/" + redirectto);
    }
/*************LinkedIn Login**********************/    
     $scope.linkedinlogin = function(idmsg) {
	 	//IN.Event.on(IN, "auth", onLinkedInAuth);
		$scope.idmsg = idmsg;
		if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = false;
						}else{
							$scope.signupalertmessage = false;
						}
	 	$linkedIn.authorize();
	 	IN.Event.on(IN, "auth", onLinkedInAuth);
	}
	
	function onLinkedInAuth(){
		IN.API.Profile("me")
          .fields('id','first-name','last-name','location','industry','headline','picture-urls::(original)','email-address')
          .result(linkrefresh)
          .error(function(err){
              console.log(err);
          });
          IN.User.logout();
     } 
	
	function linkrefresh(profile){
	
		console.log(profile);
		var member = profile.values[0];
		console.log(member.pictureUrls.length);
		if(member.pictureUrls.values.length>0)
		{
			console.log(member.pictureUrls.values[0]+' | lllllllllll');
			var image = member.pictureUrls.values[0];
		}else{
			var image = '';
		}
		
		console.log('email'+ member.emailAddress +' name'+member.firstName+' '+member.lastName +' lnid'+member.id + ' image'+image+' location'+member.location.name);
		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/lnsignupuser",
                    data: $.param({'email': member.emailAddress,'name':member.firstName+' '+member.lastName,'lnid':member.id,'image':image,'location':member.location.name}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.signupalertmessage = true;
						}
						//$scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.signupalertmessage = true;
						}
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#lnmodal').modal('show');
                    	$scope.user_name = member.firstName+' '+member.lastName;
                    	$scope.user_email = member.emailAddress;
                    	$scope.user_picture = image;
                    	$scope.user_location = member.location.name
                    	$scope.user_lnid = member.id
                    	
                    }
                });
	}
	
	$scope.lnusersignup = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' lnid'+$scope.user_lnid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/lnsignupusername",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'lnid':$scope.user_lnid,'username':$scope.user_username,'image':$scope.user_picture,'location':$scope.user_location}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.lnsignupMessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.lnsignupMessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.lnid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
    }
    
    $scope.lnsignuser = function() {
	 	$linkedIn.authorize();
	 	IN.Event.on(IN, "auth", onLinkedInLog);
	 	/*$linkedIn.authorize().then($linkedIn.isAuthorized().then(function(){
	 		IN.API.Profile("me")
                                        .fields('id','first-name','last-name','location','industry','headline','picture-urls::(original)','email-address')
                                        .result(lnreset)
                                        .error(function(err){
                                            console.log(err);
                                            //scope.linkedinMsg.loading = false
                                            //scope.linkedinMsg.errorMsg = 'Unable to get LinkedIn profile information.  Please re-authorize.';
                                        });
	 	})
	 	);*/
	 	/*console.log(loginDetails);*/
	 	/*var details=$linkedIn.isAuthorized();
	 	console.log(details);*/
	 	
	} 
	function onLinkedInLog(){
		IN.API.Profile("me")
          .fields('id','first-name','last-name','location','industry','headline','picture-urls::(original)','email-address')
          .result(lnreset)
          .error(function(err){
              console.log(err);
              //scope.linkedinMsg.loading = false
              //scope.linkedinMsg.errorMsg = 'Unable to get LinkedIn profile information.  Please re-authorize.';
          });
          IN.User.logout();
     } 
    
    function lnreset(profile) {
     	console.log(profile);
		var member = profile.values[0];
		console.log(member.pictureUrls.values.length);
		if(member.pictureUrls.values.length>0)
		{
			console.log(member.pictureUrls.values[0]+' | lllllllllll');
			var image = member.pictureUrls.values[0];
		}else{
			var image = '';
		}
		
		console.log('email'+ member.emailAddress +' name'+member.firstName+' '+member.lastName +' lnid'+member.id + ' image'+image+' location'+member.location.name);
		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/lnloginuser",
                    data: $.param({'email': member.emailAddress,'name':member.firstName+' '+member.lastName,'lnid':member.id,'image':image}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.loginalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '1')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               $scope.loginalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;

		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
     
    }
/*************************FaceBook Login*************************************/    
    $scope.fbsignuser = function () {
     $facebook.login().then(function() {
      reset();
     });
    }
    
    function reset() {
     $facebook.api("/me").then( 
      function(response) {
        console.log(response);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbloginuser",
                    data: $.param({'email': response.email,'name':response.name,'fbid':response.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.loginalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '1')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               $scope.loginalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
      },
      function(err) {
            console.log(err);
            //$scope.alertmessage = true;
            //$scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
      });
    }
    
 /*************************FaceBook Sign Up*************************************/   
    $scope.fbloginuser = function (idmsg) {
     $scope.idmsg = idmsg;
	 if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = false;
						}else{
							$scope.signupalertmessage = false;
						}
	 $facebook.login().then(function() {
      refresh();
     });
    }
    
    function refresh() {
     $facebook.api("/me").then( 
      function(response) {
        console.log(response);
		console.log($scope.idmsg);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbsignupuser",
                    data: $.param({'email': response.email,'name':response.name,'fbid':response.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.signupalertmessage = true;
						}
						//$scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               //$scope.loginalertmessage = true;
					   if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.alertmessage = true;
						}
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#fbmodal').modal('show');
                    	$scope.user_name = response.name;
                    	$scope.user_email = response.email;
                    	$scope.user_fbid = response.id
                    	/*$cookieStore.put('users', data.userdetails);
		               $scope.loginalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);

		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.loggedin = true;
		               $scope.notloggedin = false;

		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};

		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';

		                   }
		                   else if (data.userdetails.login_status == 3) {

		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }*/
                    }
                });
      },
      function(err) {
            console.log(err);
            //$scope.alertmessage = true;
            //$scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
      });
    }
    
    $scope.fbusersignup = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' fbid'+$scope.user_fbid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbsignupusername",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'fbid':$scope.user_fbid,'username':$scope.user_username}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.fbsignupMessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.fbsignupMessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
    }
/****************************Gplus Login*********************************/    
    $scope.gpluslogin = function (idmsg) {
	    $scope.idmsg = idmsg;
		if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = false;
						}else{
							$scope.signupalertmessage = false;
						}
        GooglePlus.login().then(function (authResult) {
            console.log(authResult);

            GooglePlus.getUser().then(gfresh);
            /*GooglePlus.getUser().then(function (user) {
                gfresh();
            });*/
        }, function (err) {
            console.log(err);
        });
    };
    
    function gfresh(user) {
        console.log(user);
        console.log('GooGle SignUp:----'+user.name+'|'+user.id+'|'+user.picture+'|'+user.email);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/gpsignupuser",
                    data: $.param({'email': user.email,'name':user.name,'gpid':user.id,'image':user.picture}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    console.log(data);
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.signupalertmessage = true;
						}
						//$scope.signupalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               if($scope.idmsg == 'login')
						{
							$scope.loginalertmessage = true;
						}else{
							$scope.signupalertmessage = true;
						}
					   //$scope.signupalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#gpmodal').modal('show');
                    	$scope.user_name = user.name;
                    	$scope.user_email = user.email;
                    	$scope.user_gpid = user.id;
                    	$scope.user_picture = user.picture;
                    }
                });
      
      
    }
    
    $scope.gpusersignup = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' gpid'+$scope.user_gpid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/gpsignupusername",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'gpid':$scope.user_gpid,'username':$scope.user_username,'image':$scope.user_picture}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.gpsignupMessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.gpsignupMessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.gpid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
    }
    
    $scope.gpsignuser = function () {
        GooglePlus.login().then(function (authResult) {
            console.log(authResult);

            GooglePlus.getUser().then(gpreset);
            /*GooglePlus.getUser().then(function (user) {
                gfresh();
            });*/
        }, function (err) {
            console.log(err);
        });
    };
    
    function gpreset(profile) {
     	console.log(profile);
		var member = profile;
		var image = member.picture;
		
		
		console.log('email'+ member.email +' name'+member.name +' gpid'+member.id + ' image'+image);
		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/gploginuser",
                    data: $.param({'email': member.email,'name':member.name,'gpid':member.id,'image':image}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.loginalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '1')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               $scope.loginalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;

		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
     
    }
    
    $scope.signalStarted = false;
    $scope.startSignal = function(){
        if(($scope.loggedindetails.username && $scope.loggedindetails.username!='') && (!$scope.signalStarted) && !myAuth.SafariBrowser) 
        {
            $scope.OpenTokSession.on('signal:' + $scope.loggedindetails.username, function(t){
                if(t)
                {
                    $scope.signalStarted = true;
                        var found = false;
                        if($scope.chat_boxes.length>0)
                        {
                                angular.forEach($scope.chat_boxes,function(temp){
                                        if(temp.username == t.data.user.username)
                                        {
                                                temp.messages.push({message:t.data.message,my:false});
                                                found = true;
                                                temp.hide = false;
                                        }
                                })
                        }
                        if(!found)
                        {
                                var temp;
                                temp = t.data.user;
                                temp.hide = false;
                                temp.messages = [];
								$scope.get_old_messages(temp);
                                //temp.messages.push({message:t.data.message,my:false});
                                temp.boxvalue = '';
                                $scope.chat_boxes.push(temp);
                        }
                        $scope.$apply();

                        $(".chat-area").animate({ scrollTop: $('.chat-area').prop("scrollHeight")}, 1000);
                }
            });
        }
    }
          
	$scope.chat_boxes = [];
	
	if(!myAuth.SafariBrowser)
        {
           
            $scope.OpenTokSession = OT.initSession(myAuth.OpenTokApiKey, myAuth.OpenTokSessionId);
            $scope.OpenTokSession.connect(myAuth.OpenTokToken, function (err) {            
                if (!err) {
                    console.log('hiiiiiiiiii');
                    //showConnection();
                }
                else {
                    console.error(err);
                    //enableButtons();
                }
            });
           
           /*
            $scope.OpenTokSession.on("streamCreated", function (event) {
                //$scope.OpenTokPublisher = OT.initPublisher( "my_video_div");

                $scope.OpenTokSession.subscribe( event.stream, 'friends_video_div' );

              console.log("New stream in the session: ", event.stream);
           });*/
       }
        $scope.startSignal();
	
    //$scope.my_video_div = '<div id="my_video_div" class="img-responsive video_div_css"></div>';
    //$scope.friends_video_div = '<div id="friends_video_div" class="img-responsive video_div_css"></div>';
    
    $scope.openVideo = function(mentor){
        console.log('Video chat');
        $rootScope.$broadcast('video_chat',mentor);
    }
    $rootScope.$on("video_chat", function(event,mentor)
    {
        if ($scope.OpenTokSession.capabilities.publish == 1) {  
            
            $scope.outringing = new Audio('http://soundbible.com/grab.php?id=1370&type=mp3');
            $scope.outringing.loop = true;
            $scope.outringing.play();
            var calldetails = {fromid : $scope.loggedindetails.id,
                toid : mentor.userid,
                fromname:$scope.loggedindetails.username,
                toname : mentor.username,
                callstarted:$scope.loggedindetails.id
            };
            console.log('Started Call',calldetails);
             $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "opentoks/request_video",
                    data: $.param({fromid : $scope.loggedindetails.id,
                        fromname:$scope.loggedindetails.username, 
                        toid : mentor.userid, 
                        toname : mentor.username,
                        callstarted:$scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function(data) { 
                      if(data.status ='success')
                      {
                        calldetails.opentok_id = data.data.opentok_id;
                        socket.emit("request_video", calldetails);                    
                        $scope.outgoingCallDiv = true;
                        $scope.outgoingCallTo = mentor.username;
                        $scope.outgoingUser = calldetails;
                        
                        /*$('#video_modal').modal('show');
                        $scope.OpenTokPublisher = OT.initPublisher( "my_video_div" );
                        $scope.OpenTokSession.publish( $scope.OpenTokPublisher, function(err) {
                           if (err) return console.log('publishing error');
                           console.log('Publiched ============ ');
                         });*/
                      }
                  })
          }
          else
          {
              alert("Sorry you don't have any Mic and Camera device to call");
          }
        /*
        console.log('header controlle');
         $scope.OpenTokPublisher = OT.initPublisher( "my_video_div",{publisher:$scope.loggedindetails.username,name:mentor.username} );
         $scope.OpenTokSession.publish( $scope.OpenTokPublisher, function(err) {
            if (err) return console.log('publishing error');
            console.log('Publiched ============ ');
          });*/
    })
    
    $scope.cancelOutgoingCall = function(user){
        $scope.outringing.pause();
        $scope.outgoingCallDiv = false;
        socket.emit("outgoung_call_canceled",user); 
    }
    
    socket.on('cancel_incoming_call', function(user) {
        if(user.toid == $scope.loggedindetails.id)
        {
            $scope.incomingCallDiv = false;
            $scope.$apply();
            $scope.ringing.pause();
        }
    })
    
    socket.on('newvideorequest', function(data) {
        //alert('hiii1');
        if((data.toid == $scope.loggedindetails.id || data.fromid == $scope.loggedindetails.id) && (data.callstarted != $scope.loggedindetails.id))
        {
            //alert('hi2');
            console.log('New video call');
            $scope.ringing = new Audio('http://soundbible.com/grab.php?id=1868&type=mp3');
            $scope.ringing.loop = true;
            $scope.ringing.play();
            $scope.incomingCallDiv = true;
            $scope.incomingCallFrom = data.fromname;
            $scope.incomingUser = data;
            $scope.$apply();
            /**/
        }
    });
    
    socket.on('reject_outgoing_call', function(data) {
        if(data.callstarted==$scope.loggedindetails.id)
        {
            console.log('Outgoinbg call rejected');
            $scope.outgoingCallDiv = false;            
            //$('#video_modal').modal('hide');
            $scope.$apply();
            $scope.outringing.pause();
        }
    })
    
    socket.on('outgoing_call_accepted', function(data) {
        if(data.callstarted == $scope.loggedindetails.id)
        {
            console.log("Outgoing call accepted == ",data);
            $('.my_video_div_container').html($scope.my_video_div);
            $('.friends_div_container').html($scope.friends_video_div);
            $scope.outringing.pause();
            $scope.outgoingCallDiv = false;
            console.log('Accepted other end');
            $location.path("/frontend/video_call/" + data.opentok_id);
            $scope.$apply();
            /******** Opening in new tab For Video **************/
            /*$('#video_modal').modal('show');
            $scope.callData = data;
            
            $scope.OpenTokPublisher = OT.initPublisher( "my_video_div" );
            $scope.OpenTokSession.publish( $scope.OpenTokPublisher, function(err) {
               if (err) return console.log('publishing error');
               console.log('Publiched ============ ');
             });
             $scope.$apply();*/
        }
        
    })
    
    $scope.acceptVideoCall = function(data){
        $scope.ringing.pause();
        $('.my_video_div_container').html($scope.my_video_div);
        $('.friends_div_container').html($scope.friends_video_div);
        
        socket.emit("accept_incoming_call",data);
        $scope.incomingCallDiv = false;
        $scope.callData = data;
        $scope.$apply();
        console.log('Accepted my end');
        $location.path("/frontend/video_call/" + data.opentok_id);
        /*$('#video_modal').modal('show');
            $scope.OpenTokPublisher = OT.initPublisher( "my_video_div" );
            $scope.OpenTokSession.publish( $scope.OpenTokPublisher, function(err) {
               if (err) return console.log('publishing error');
               console.log('Publiched ============ ');
             });*/
    }
    
    
    $scope.rejectVideoCall = function(data){
        $scope.ringing.pause();
        socket.emit("reject_incoming_call",data);
        $('#video_modal').modal('hide');
        $scope.incomingCallDiv = false;
    }
	
    $rootScope.$on("open_chat", function(event,mentor)
    {	
        if(!myAuth.SafariBrowser)
        {
            //$('#chat').load(chat_template);
            $scope.push_chat_box(mentor);
            $(".chat-area").animate({ scrollTop: $('.chat-area').prop("scrollHeight")}, 1000);
        }
        else
        {
            alert('Sorry! Chat functionality is not available for Safari browser.');
        }
    });
	
	$scope.hide_chat_box = function(mentor){
		mentor.hide = true;
		$scope.$apply();
	}
	
	$scope.push_chat_box = function(data)
	{
		//console.log(data,"old =======",$scope.chat_boxes);
		var found = false;
		if($scope.chat_boxes.length>0)
		{
			angular.forEach($scope.chat_boxes,function(temp){
				if(temp.username == data.username)
				{
					//temp.messages.push({message:t.data.message,my:false});
					data.hide = false;
					found = true;
				}
			})
		}
		if(!found)
		{
			$scope.get_old_messages(data);
			data.messages = [];
			data.boxvalue = '';
			data.hide = false;
			$scope.chat_boxes.push(data);
		}
		
		$scope.$apply();
		
		//console.log("New =======",$scope.chat_boxes);
	}
	
	$scope.send_chat = function(key,mentor){
		if(key.which==13 && mentor.boxvalue)
		{	
			var value = mentor.boxvalue;
			mentor.boxvalue='';
			mentor.messages.push({message:value,my:true});
			
			$scope.OpenTokSession.signal({
                                type: mentor.username,
                                data: {user:{username:$scope.loggedindetails.username,name:$scope.loggedindetails.name,userid:$scope.loggedindetails.id},message:value}
                          }, $scope.retchat );
                          
                        $http({
                            method: "POST",
                            url: $rootScope.serviceurl + "chats/save",
                            data: $.param({from_user : $scope.loggedindetails.id,to_user:mentor.userid, message : value}),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                          });
                          
                          $(".chat-area").animate({ scrollTop: $('.chat-area').prop("scrollHeight")}, 1000);
		}
	}
	
	 $scope.retchat = function(temp)
			  {
				  
				  console.log("chat sent --------",temp);
			  }
	
    socket.on('disconectuser', function(data) {
    //alert(data +" is loged in");
      console.log("New Loged user"+data);
      	/*$http({
			  method: "POST",
			  url: $rootScope.serviceurl + "users/logoutuser",
			  data: $.param({'userid': $scope.loggedindetails.id}),
			  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data) {
			console.log($scope.loggedindetails);
			  var jsonstatus = {'userid': $scope.loggedindetails.id};
			  socket.emit("user_logout", jsonstatus);
				myAuth.resetUserinfo();
			  $cookieStore.put('users', null);
			  myAuth.updateUserinfo(myAuth.getUserAuthorisation());
			  $scope.loggedindetails = myAuth.getUserNavlinks();
			  $scope.loggedindetails = '';
			  $scope.loggedin = false;
			  $scope.onlinestat = false;
			  $scope.onlinestatnoti=false;
			  $scope.notloggedin = true;
			}); */
    });
    
    $scope.get_old_messages = function(data)
    {
        console.log('Old  Messages --- ',data);
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "chats/get_history",
            data: $.param({'from_user': $scope.loggedindetails.id,'to_user':data.userid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         }).success(function(res) {
             angular.forEach(res.messages,function(message){
                 data.messages.push(message);
             });
             $scope.$apply();
         });
    }
    
    /**************** Checking browser close *************/
    /*$window.onbeforeunload = function(event){
          var answer = confirm("Are you sure you want to leave this page?");
        if (!answer) {
            event.preventDefault();
        }
    }
    /*$scope.$on('$locationChangeStart', function( event ) {
        var answer = confirm("Are you sure you want to leave this page?")
        if (!answer) {
            event.preventDefault();
        }
    });*/

});

