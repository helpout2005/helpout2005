'use strict';
/** 
  * controllers used for the open request
*/
app.controller('withdrawCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.listView=false;
     $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
     }
     
    $scope.stripeCallback = function (code, result) {
        $scope.error = false;
        $scope.success = false;
        $scope.ref = false;
        
        if (result.error) {
                $scope.changerequeststatusmessage=true;
                $scope.alert = myAuth.addAlert('danger','Invalid account details');
        } else {
            $scope.userdetails = {};
            $scope.userdetails.stripeToken = result.id;
            
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "paypals/withdraw",
                data:$.param({token : $scope.userdetails.stripeToken,user_id : $scope.loggedindetails.id,amount:$scope.amount,bank_id:result.bank_account.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(trans){
                $scope.changerequeststatusmessage=true;
                $scope.alert = myAuth.addAlert(trans.status,trans.message);
                if(trans.status == 'success')
                {
                    $scope.routing_number = '';
                    $scope.account_number = '';
                    $scope.country = '';
                    $scope.currency = '';
                    $scope.amount = '';
                    $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/getuserdetails",
                        data: 'userid=' + $scope.loggedindetails.id ,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(userdata){
                        if(userdata && userdata.msg_type)
                        {
                            myAuth.updateUserinfo(userdata.userdetails);
                            $cookieStore.put('users',userdata.userdetails);
                            $scope.likerequest();
                            //$location.path('/frontend/payment_success');
                        }
                    })
                    //$scope.assignedhelps = trans.data;
                }
            })
            /*
           $http({
                    method: "POST",
                    url:"./api/pay.php",
                    data: 'stripeToken=' + result.id + '&amount=' + $scope.amount,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){				
                  if(data=='error')
                  {
                        $scope.changerequeststatusmessage=true;
                        $scope.alert = myAuth.addAlert('danger','Invalid card details');
                  }
                  else
                  {
                      //console.log(data);
                      $scope.success=true;
                      $scope.error = false;
                      $scope.success_msg = 'Payment successfull';
                      if(data.id)
                      {
                          $scope.refund_id = data.id;
                          $scope.ref = true;
                      }
                  }

            });*/
            //window.alert('success! token: ' + result.id);
        }
    };
});


