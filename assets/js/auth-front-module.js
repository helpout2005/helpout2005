angular
.module("authFront", ["ngCookies"])
.factory("myAuth", function ($http, $cookies, $cookieStore) {
  var factobj = {};
  factobj.alerts = [
    { type: 'danger', msg: 'Invalid Email/Password!', altclass: 'text-center alert alert-danger' },
    { type: 'success', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-success' },
    { type: 'warning', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-warning' }
  ];

  factobj.addAlert = function(type,message) {
   var msg={};
    angular.forEach(factobj.alerts,function(alert,key){
       if(alert.type==type)
       {
         alert.msg=message;
         msg=alert;
       }
    });
    return msg;
  };
  
    //factobj.baseurl = "http://localhost/livehelpout/";
    //factobj.baseurl = "http://45.79.160.248/";
     factobj.baseurl = "http://beta.livehelpout.com/";
    
    var years = [];
    for (var i = 2014; i <= 2050; i++) {
        years.push(i);
    }
    factobj.years = years;
    
    var days = [];
    for (var i = 1; i <= 31; i++) {
        days.push(i);
    }
    factobj.days = days;
    
    /*********************************Admin Authorisation ***********************************/
    factobj.adminuserinfo = { loginstatus: false, id: "",username:"" };
    factobj.updateAdminUserinfo = function (obj) {
    if(obj)
	{
           factobj.adminuserinfo = { loginstatus: obj.isloggedin, id: obj.id,username:obj.username };
           return true;
	}
    };
    factobj.resetAdminUserinfo = function () {
        factobj.adminuserinfo = { loginstatus: false, id: "", username:""};
    };
    
    factobj.getAdminAuthorisation = function () {      
       var obj=$cookieStore.get('admins');
       if(obj)
       return obj;
       else
       return null;
    };
    
    factobj.getAdminNavlinks = function () {
        var adminlogin = factobj.adminuserinfo.loginstatus,
            adminemail = (typeof factobj.adminuserinfo.id == "undefined" || factobj.adminuserinfo.id == "") ? "Unknown" : factobj.adminuserinfo.id;
        if (!adminlogin) {
           return false;
        } else {
	    return factobj.adminuserinfo;
        }
    };
    
    factobj.isAdminLoggedIn = function () {
        var adminlogin = factobj.adminuserinfo.loginstatus;
        if (!adminlogin) {
            return false;
        } else {
	    return true;
        }
    };
    
    /*********************************Admin Authorisation End***********************************/
    
    /*********************************User Authorisation ***********************************/
    factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"",role:"",current_status:"",price:"",fbid:"",lnid:"",gid:"",imgtype:"",credit_tokens:0,stripe_id:"" };
    factobj.updateUserinfo = function (obj) {
    if(obj)
	{
           if(obj.imgtype==1){
           	if(obj.image=='')
           	{
           		obj.image = factobj.baseurl+'assets/upload/user_images/noimage.png';
           	}else{
                        var pat = /^((http|https|ftp):\/\/)/;
                        if(pat.test(obj.image))
                        {
                            obj.image = obj.image;
                        }
                        else
                        {
                            obj.image = factobj.baseurl+'assets/upload/user_images'+obj.image;
                        }
           	}
           }
           factobj.userinfo = { loginstatus: obj.isloggedin, id: obj.id, email: obj.email,name:obj.name,username:obj.username,image:obj.image,role:obj.role,current_status:obj.login_status,price:obj.price,fbid:obj.fbid,lnid:obj.lnid,gid:obj.gid,imgtype:obj.imgtype,credit_tokens:obj.credit_tokens,stripe_id:obj.stripe_id};
           return true;
	}
    };
    factobj.resetUserinfo = function () {
        factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"",role:"",current_status:"",price:"",fbid:"",lnid:"",gid:"",imgtype:"",credit_tokens:0,stripe_id:"" };
    };
    
    factobj.getUserAuthorisation = function () {      
       var obj=$cookieStore.get('users');
       if(obj)
       return obj;
       else
       return null;
    };
    
    factobj.getUserNavlinks = function () {
        var userlogin = factobj.userinfo.loginstatus,
            useremail = (typeof factobj.userinfo.id == "undefined" || factobj.userinfo.id == "") ? "Unknown" : factobj.userinfo.id;
        if (!userlogin) {
           return false;
        } else {
	    return factobj.userinfo;
        }
    };
    
    factobj.isUserLoggedIn = function () {
        var userlogin = factobj.userinfo.loginstatus;
        if (!userlogin) {
            return false;
        } else {
	    return true;
        }
    };
    
    /*********************************User Authorisation End***********************************/
    
    /******************************** Opentok Start**************************************/
    factobj.OpenTokSessionId = '2_MX40NTQ3MTk1Mn5-MTQ1Mzk2MjUxNzg2M34xdVcxWmpoYWNlN2xtMm85OGFBdGd1UHN-UH4';
    factobj.OpenTokApiKey = '45471952';
    factobj.OpenTokSecret = '37bc19b808ad4f67300b1fa29fb17b180587982a';
	factobj.OpenTokToken = 'T1==cGFydG5lcl9pZD00NTQ3MTk1MiZzaWc9NGQ3ZTg2NjNhMjQzYzYyOTYxMjcwMzBlOTBhZWU3M2IzYWQwMDNlNDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTJfTVg0ME5UUTNNVGsxTW41LU1UUTFNemsyTWpVeE56ZzJNMzR4ZFZjeFdtcG9ZV05sTjJ4dE1tODVPR0ZCZEdkMVVITi1VSDQmY3JlYXRlX3RpbWU9MTQ1NDEzODI2NiZub25jZT0wLjcxOTc4NDkxMjkwNzYyNTQmZXhwaXJlX3RpbWU9MTQ1NjczMDIyNSZjb25uZWN0aW9uX2RhdGE9';
	
    /*factobj.genOpenTokToken = function(data)
     {
          var secondsInDay = 86400;
  // Credentials
        var sessionId = factobj.OpenTokSessionId;    
        var apiKey = factobj.OpenTokApiKey;
        
        var secret = factobj.OpenTokSecret;
        // Token Params
        var timeNow = Math.floor(Date.now()/1000);
        var expire = timeNow+secondsInDay;
        var role = "publisher";
        //var data = "bob";
        //TB.setLogLevel(TB.DEBUG);
        // Calculation
        data = escape(data);
        var rand = Math.floor(Math.random()*999999);
        var dataString =  "session_id="+sessionId+"&create_time="+timeNow+"&expire_time="+expire+"&role="+role+"&connection_data="+data+"&nonce="+rand;
        // Encryption
        var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, secret);
        hmac.update( dataString );
        var hash = hmac.finalize();
        var preCoded = "partner_id="+apiKey+"&sig="+hash+":"+dataString;
        var token = "T1=="+$.base64.encode( preCoded );
        return token;
     }*/
    /******************************** Opentok End**************************************/
    
    
     /********** Browser Detecting Start **************/
   BrowserDetect={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser",this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version",this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(i){for(var n=0;n<i.length;n++){var r=i[n].string,t=i[n].prop;if(this.versionSearchString=i[n].versionSearch||i[n].identity,r){if(-1!=r.indexOf(i[n].subString))return i[n].identity}else if(t)return i[n].identity}},searchVersion:function(i){var n=i.indexOf(this.versionSearchString);if(-1!=n)return parseFloat(i.substring(n+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera",versionSearch:"Version"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};
    /********** Browser Detecting Start **************/
	 /****************** Check Safari ***********/
    BrowserDetect.init();
    console.log('User Agent ========= ',BrowserDetect.browser);
    if(BrowserDetect.browser == 'Safari')
    {
        
        factobj.SafariBrowser = true;
    }
    else
    {
        factobj.SafariBrowser = false;
         var js = document.createElement("script");
        js.type = "text/javascript";
        js.src = 'http://static.opentok.com/v2/js/opentok.min.js';
        //document.head.appendChild(js);
    }
    
    return factobj;
	
})
