'use strict';
/** 
  * Returns the id of the selected e-mail. 
*/
app.directive('profileImage',  function () {
    return {
    link: function(scope, element, attrs) {
    	if(attrs.image)
    	{
    		scope.imgsrc = attrs.image;
			 /* if(attrs.fbid || attrs.gpid || attrs.lnid)
		      {
			  	scope.imgsrc = attrs.image;
			  }			 
			  else
			  {
			  	scope.imgsrc = 'assets/upload/user_images/noimage.png';
			  }*/
		}
		else if(attrs.imageUrl)
		{
			scope.imgsrc = attrs.imageUrl;
		}
		else
		{
			scope.imgsrc = 'assets/upload/user_images/noimage.png';
		}
      
        
      if(attrs.username)
      {
	  	scope.alt = attrs.username;
	  }
	  else
	  {
	  	scope.alt = 'Profile Pic';
	  }
	  
	  if(attrs.imgclass)
	  {
	  	scope.imgclass = attrs.imgclass;
	  }
	  else
	  {
	  	scope.imgclass = "";
	  }
    },
    template:'<img src="{{imgsrc}}" alt="{{alt}}" class="{{imgclass}}">'
  };
});
