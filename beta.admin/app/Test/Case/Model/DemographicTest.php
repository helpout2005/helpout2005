<?php
App::uses('Demographic', 'Model');

/**
 * Demographic Test Case
 *
 */
class DemographicTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.demographic'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Demographic = ClassRegistry::init('Demographic');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Demographic);

		parent::tearDown();
	}

}
