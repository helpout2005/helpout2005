<!-- BEGIN CONTAINER -->
<?php 
$total = $count1;
$logUser = $log_user;
$logoutUser = $notlog_user;
$percntLogUser = round(($logUser/$total)*100);
$percntLogoutUser = round(($logoutUser/$total)*100);
?>
<script>
function user_extra()
{
	$.ajax({
            method:'post',
            url:'<?php echo $this->webroot;?>admin/users/extra',
            success:function(data){
              $("#extra").html(data);
              setTimeout("user_extra()",10000);
            }
          });
	/*$.post("<?php echo $this->webroot;?>admin/users/extra",$(this).serialize(),function(data){
		$('#extra').html(data);
	});*/
	
}
function user_count()
{
	$.ajax({
            method:'post',
            url:'<?php echo $this->webroot;?>admin/users/count',
            success:function(data){
              $("#count").html(data);
              setTimeout("user_count()",10000);
            }
          });
	/*$.post("<?php echo $this->webroot;?>admin/users/extra",$(this).serialize(),function(data){
		$('#extra').html(data);
	});*/
	
}
function user_chart()
{
	$.ajax({
            method:'post',
            url:'<?php echo $this->webroot;?>admin/users/chart_online',
            success:function(online){
		        $.ajax({
				  method:'post',
				  url:'<?php echo $this->webroot;?>admin/users/chart_ofline',
				  success:function(ofline){
					   $('.easy-pie-chart .percentage').data('easyPieChart').update(online);
		        		   $('.easy-pie-chart .percentage span').text(online);
					   $('.easy-pie-chart .percentage2').data('easyPieChart').update(ofline);
					   $('.easy-pie-chart .percentage2 span').text(ofline);
					    setTimeout("user_chart()",10000);
				  }
				}); 
            }
          });
	
}

$(document).ready(function(e) {
	setTimeout("user_extra()",10000);
	setTimeout("user_chart()",10000);
	/*$('.update-easy-pie-chart').click(function(){
        $.ajax({
            method:'post',
            url:'<?php echo $this->webroot;?>admin/users/chart_online',
            success:function(online){
		        $.ajax({
				  method:'post',
				  url:'<?php echo $this->webroot;?>admin/users/chart_ofline',
				  success:function(ofline){
					   $('.easy-pie-chart .percentage').data('easyPieChart').update(online);
		        		   $('.easy-pie-chart .percentage span').text(online);
					   $('.easy-pie-chart .percentage2').data('easyPieChart').update(ofline);
					   $('.easy-pie-chart .percentage2 span').text(ofline);
				  }
				}); 
            }
          });
         
    });*/
});


/*$(function() {
    //create instance
    $('.easy-pie-chart1').easyPieChart({
        animate: 2000,
        size: 220,
        lineWidth: 5,
        barColor: #5EB95E
    });
    $('.easy-pie-chart2').easyPieChart({
        animate: 2000,
        size: 220,
        lineWidth: 5,
        barColor: #DD514C
    });
    //update instance after 5 sec
    setTimeout(function() {
        $('.chart').data('easyPieChart').update(40);
    }, 5000);
});*/
</script>
		
		<!-- BEGIN PAGE -->  
      <div id="">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN THEME CUSTOMIZER-->
                   <div id="theme-change" class="hidden-phone">
                       <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                   </div>
                   <!-- END THEME CUSTOMIZER-->
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                     Dashboard
                   </h3>
                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Home</a>
                           <span class="divider">/</span>
                       </li>
                       <!--<li>
                           <a href="#">Metro Lab</a>
                           <span class="divider">/</span>
                       </li>-->
                       <li class="active">
                           Dashboard
                       </li>
                      <!-- <li class="pull-right search-wrap">
                           <form action="http://thevectorlab.net/metrolab/search_result.html" class="hidden-phone">
                               <div class="input-append search-input-area">
                                   <input class="" id="appendedInputButton" type="text">
                                   <button class="btn" type="button"><i class="icon-search"></i> </button>
                               </div>
                           </form>
                       </li>-->
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <?php
            //print_r($all_user);
           
            ?>
            <div class="row-fluid">
                <!--BEGIN METRO STATES-->
                <div class="metro-nav">
                    <div class="metro-nav-block nav-block-orange">
                        <a data-original-title="" href="#">
                            <i class="icon-user"></i>
                            <div class="info" id="count"><?php echo $count1;?></div>
                            <div class="status">Totla User</div>
                        </a>
                    </div>
                    <!--<div class="metro-nav-block nav-block-yellow">
                        <a data-original-title="" href="#">
                            <i class="icon-tags"></i>
                            <div class="info">+970</div>
                            <div class="status">Sales</div>
                        </a>
                    </div>-->
                    <!--<div class="metro-nav-block nav-block-grey">
                        <a data-original-title="" href="#">
                            <i class="icon-comments-alt"></i>
                            <div class="info">49</div>
                            <div class="status">Comments</div>
                        </a>
                    </div>-->
                    <!--<div class="metro-nav-block nav-block-blue double">
                        <a data-original-title="" href="#">
                            <i class="icon-eye-open"></i>
                            <div class="info">+897</div>
                            <div class="status">Unique Visitor</div>
                        </a>
                    </div>-->
                    <!--<div class="metro-nav-block nav-block-red">
                        <a data-original-title="" href="#">
                            <i class="icon-bar-chart"></i>
                            <div class="info">+288</div>
                            <div class="status">Update</div>
                        </a>
                    </div>-->
                </div>
                <!--<div class="metro-nav">
                    <div class="metro-nav-block nav-block-blue">
                        <a data-original-title="" href="#">
                            <i class="icon-shopping-cart"></i>
                            <div class="info">29</div>
                            <div class="status">New Order</div>
                        </a>
                    </div>
                    <div class="metro-nav-block nav-block-green double">
                        <a data-original-title="" href="#">
                            <i class="icon-tasks"></i>
                            <div class="info">$37624</div>
                            <div class="status">Stock</div>
                        </a>
                    </div>
                    <div class="metro-nav-block nav-block-orange">
                        <a data-original-title="" href="#">
                            <i class="icon-envelope"></i>
                            <div class="info">123</div>
                            <div class="status">Messages</div>
                        </a>
                    </div>
                    <div class="metro-nav-block nav-block-purple">
                        <a data-original-title="" href="#">
                            <i class="icon-remove-sign"></i>
                            <div class="info">34</div>
                            <div class="status">Cancelled</div>
                        </a>
                    </div>
                    <div class="metro-nav-block nav-block-grey ">
                        <a data-original-title="" href="#">
                            <i class="icon-external-link"></i>
                            <div class="info">$53412</div>
                            <div class="status">Total Profit</div>
                        </a>
                    </div>
                </div>-->
                <div class="space10"></div>
                <!--END METRO STATES-->
            </div>
            <div class="row-fluid">
                 <div class="span6">
                    <!--BEGIN GENERAL STATISTICS-->
                    <div class="widget blue">
                        <div class="widget-title">
                            <h4><i class="icon-tasks"></i> General Statistics </h4>
                         <span class="tools">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
                         </span>
                            <!--<div class="update-btn">
                                <a href="javascript:;" class="btn update-easy-pie-chart"><i class="icon-repeat"></i> Update</a>
                            </div>-->
                        </div>
                        <div class="widget-body" id="chart">
                            <div class="text-center">
                                <div class="easy-pie-chart">
                                    <div class="percentage success" data-percent="<?php echo $percntLogUser?>"><span><?php echo $percntLogUser?></span>%</div>
                                    <div class="title">Online User</div>
                                </div>
                                <div class="easy-pie-chart">
                                    <div class="percentage2" data-percent="<?php echo $percntLogoutUser?>"><span><?php echo $percntLogoutUser?></span>%</div>
                                    <div class="title">Offline User</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!--END GENERAL STATISTICS-->
                </div>
                 <div class="span6">
                     <!-- BEGIN CHAT PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4><i class="icon-user"></i> Users</h4>
									<span class="tools">
									<a href="javascript:;" class="icon-chevron-down"></a>
									<a href="javascript:;" class="icon-remove"></a>
									</span>
                         </div>
                         <div class="widget-body" style="overflow-y: auto;height: 408px;">
                             <div class="timeline-messages" id="extra">
                                 <!-- Comment -->
                                 <?php
                                 //print_r($all_user);
                                 foreach($all_user as $user)
                                 {
                                 ?>
                                 <div class="msg-time-chat">
                                    
                                     <div class="message-body msg-in">
                                         <span class="arrow"></span>
                                         <div class="text">
                                             <p class="attribution"><a href="#"><?php if(isset($user['User']['name']) && $user['User']['name']!='') {echo $user['User']['name'];} ?></a> &nbsp;&nbsp;<b><?php if(isset($user['User']['email']) && $user['User']['email']!='') {echo $user['User']['email'];} ?></b></p>
                                            
                                             <p><?php if(isset($user['User']['is_logged_in'])) {if($user['User']['is_logged_in']=='1') { echo 'Logged In';} else { echo 'Offline';}}?></p>
                                             <p><?php 
                                             if(isset($user['User']['is_logged_in']) && isset($user['User']['login_status'])) 
                                             {
                                               if($user['User']['is_logged_in']=='1') 
                                                { 
                                                 if(isset($user['User']['login_status'])) 
                                                   {
                                                     if($user['User']['login_status']=='1') 
                                                        { ?>
                                                         <img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;INSTANT 
                                                        <?php } 
                                                         if($user['User']['login_status']=='2') 
                                                         {?>
                                                           <img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;ONLINE 
                                                         <?php } 
                                                           if($user['User']['login_status']=='3') 
                                                             { ?>
                                                             <img src="<?php echo $this->webroot;?>img/away.png" title="online" width="10" height="12">&nbsp;AWAY
                                                              
                                                            <?php } 
                                                               if($user['User']['login_status']=='4')
                                                               {?> 
                                                               <img src="<?php echo $this->webroot;?>img/offline.png" title="offline" width="10" height="12">&nbsp;OFFLINE
                                                               <?php }
                                                               }
                                                              }
                                                            }
                                                          ?></p>
                                             
                                             <p><b>Last Login:&nbsp;<?php if(isset($user['User']['last_login'])) { echo $user['User']['last_login'];}?></b></p>
                                         </div>
                                         
                                     </div>
                                     
                                 </div>
                                 <?php
                                 }
                                 ?>
                                 <!-- /comment -->

                                 
                             </div>
                             
                         </div>
                     </div>
                     <!-- END CHAT PORTLET-->
                 </div>
             </div>
            <div class="row-fluid">
                <div class="span7 responsive" data-tablet="span7 fix-margin" data-desktop="span7">
                   
                  
                </div>
                <div class="span5">
                </div>
            </div>

           
         </div>
        
      </div>
		


	
	
