<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'ProHelp');
?>
<!DOCTYPE html>
<html ng-app="prohelp">
<head>
      <title>Welcome</title>
      <link href="<?php echo($this->webroot)?>css/bootstrap.css" rel='stylesheet' type='text/css' />
      <link href="<?php echo($this->webroot)?>css/bootstrap.min.css" rel="stylesheet" />
      <link href="<?php echo($this->webroot)?>css/ladda-themeless.min.css" rel="stylesheet" />
      <link href="<?php echo($this->webroot)?>css/ladda.min.css" rel="stylesheet" />
      <link href="<?php echo($this->webroot)?>ng/css/ng-tags-input.min.css" rel="stylesheet" />
     
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="<?php echo($this->webroot)?>ng/js/jquery.min.js"></script>
      <!-- Custom Theme files -->
      <!---- animated-css ---->
      <link href="<?php echo($this->webroot)?>css/animate.css" rel="stylesheet" type="text/css" media="all">
      <script src="<?php echo($this->webroot)?>js/wow.min.js"></script>
      <script>
        setTimeout(function()
        { 
          new WOW().init(); 
        }, 1000);
      </script>
      <!---- animated-css ---->
      <!---- start-smoth-scrolling---->
      <script type="text/javascript" src="<?php echo($this->webroot)?>js/move-top.js"></script>
      <script type="text/javascript" src="<?php echo($this->webroot)?>js/easing.js"></script>
      <script src="<?php echo($this->webroot)?>js/bootstrap.min.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
         	$(".scroll").click(function(event){		
         		event.preventDefault();
         		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
         	});
         });
      </script>
      <!---- start-smoth-scrolling---->
      <link href="<?php echo($this->webroot)?>css/style.css" rel='stylesheet' type='text/css' />
      
      <!-- Custom Theme files -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
     
      <!----start-top-nav-script---->
      <script>
         $(function() {
         	var pull 		= $('#pull');
         		menu 		= $('nav ul');
         		menuHeight	= menu.height();
         	$(pull).on('click', function(e) {
         		e.preventDefault();
         		menu.slideToggle();
         	});
         	$(window).resize(function(){
               		var w = $(window).width();
               		if(w > 320 && menu.is(':hidden')) {
               			menu.removeAttr('style');
               		}
           	});
         });
      </script>
      
      <!----//End-top-nav-script---->
      
      
      
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular/angular.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular/angular-sanitize.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular-animate/angular-animate.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular-route/angular-route.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular-resource/angular-resource.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular/angular-cookies.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/lib/angular/ng-tags-input.min.js"></script>
        <script src="<?php echo($this->webroot)?>js/jstz-1.0.4.min.js"></script>

        <script src="<?php echo($this->webroot)?>ng/js/app/util-module.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/app/frontendapp.js"></script>
        <script src="<?php echo($this->webroot)?>ng/js/app/auth-front-module.js"></script>

<script src="<?php echo($this->webroot)?>js/spin.js"></script>
      <script src="<?php echo($this->webroot)?>js/ladda.js"></script>
     <script src="<?php echo($this->webroot)?>ng/js/bower_components/angular-ladda/dist/angular-ladda.min.js"></script> 
      
      
   </head>
<body>	
	 <!--NAV SECTION -->
      <div ng-controller="headerController">
       <header id="header-nav" class="positon-static" >
         <div id="home" class="header wow bounceInDown" data-wow-delay="0.4s">
            <div class="top-header">
               <div class="container">
                  <div class="logo">
                     <a href="#"><img src="images/logo.png" title="dreams" /></a>
                  </div>
                  <!----start-top-nav---->
                  <nav class="top-nav">
                     <ul class="top-nav">
                        <li>
                           <div class="sarch_box"><span><input type="text"></span><span style="float: right;"><input type="button" class="search_btn"></span></div>
                        </li>
                        <li class="user-profile" ng-show="loggedin">
                            <div class="dropdown">
                                <div class="s-round"><img src="{{siteurl}}upload/user_images/{{loggedindetails.image!=''?loggedindetails.image:'noimage.png'}}" style="cursor:pointer;" ng-click="showdropdownmenu();"></div>
                                <a id="profile-click" style="cursor:pointer;" ng-click="showdropdownmenu();">{{loggedindetails.username}}</a>
                                <!--<ul id="#profile-details-ul" class="">
                                    <li>123</li>
                                </ul>-->
                                <ul class="dropdown-menu">
                                 <li class="item">
                                  <a href="javascript:void(0);" style="font-size:15px;padding: 0 20px;text-align:left;">
                                   <img src="{{siteurl}}/img/user.png" title="menu" width='16' height="16"/>&nbsp; My Profile
                                  </a>
                                 </li>
                                 <li class="item">
                                  <a href="javascript:void(0);" style="font-size:15px;padding: 0 20px;text-align:left;">
                                   <img src="{{siteurl}}/img/settings.png" title="menu" width='16' height="16"/>&nbsp;Settings
                                  </a>
                                 </li>
                                 <li class="item">
                                  <a href="javascript:void(0);" style="font-size:15px;padding: 0 20px;text-align:left;" ng-click="logout();">
                                    <img src="{{siteurl}}/img/logout.png" title="menu" width='16' height="16"/>&nbsp;Logout
                                  </a>
                                 </li>
                                </ul>
                            </div>
                        </li>
                        <li ng-show="loggedin"><a href="javascript:void(0);"  ng-click="logout();">Logout</a></li>
                        <li ng-show="notloggedin"><a href="javascript:void(0);"  ng-click="openModal('myModal-2');">Join</a></li>
                        <li ng-show="notloggedin"><a href="javascript:void(0);" ng-click="openModal('myModal');">Sign In</a></li>
                     </ul>
                     <a href="#" id="pull"><img src="images/menu-icon.png" title="menu" /></a>
                  </nav>
                  <div class="clearfix"> </div>
               </div>
            </div>
         </div>
      </header>
      <div class="custom-modal modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Sign in</h4>
               </div>
               <div class="modal-body">
                 <div ng-show="loginalertmessage" class="{{alert.altclass}}">
                  <div id="flash_notice">{{alert.msg}}</div>
                 </div>
                  <form>
                     <div class="form-group">
                        <label class="col-sm-1 control-label"><span class="glyphicon glyphicon-user"></span></label>
                        <div class="col-sm-11">
                           <input type="text" placeholder="Username" class="form-control" ng-model="user_username"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-1 control-label"><span class="glyphicon glyphicon-lock"></span></label>
                        <div class="col-sm-11">
                           <input type="password" placeholder="Password" class="form-control" ng-model="user_password"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12">
                           <div class="checkbox">
                              <label>
                              <input type="checkbox"> Remember Me
                              </label>
                               <div class="forgot"><a href="javascript:void(0);" ng-click="openModal('myModal-3');">Forgot Password?</a></div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-12">
                           <button ng-click="login();" data-style="zoom-in"  type="submit" ladda="loading" data-spinner-size="25" class="btn btn-cust-new">Submit</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
       </form>
      </div>
      
      <div class="custom-modal modal fade" id="myModal-2"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <form  method="POST"> 
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Sign Up and Get Help Now</h4>
               </div>
               <div class="modal-body extraspace">
                <div ng-show="signupalertmessage" class="{{alert.altclass}}">
                  <div id="flash_notice">{{alert.msg}}</div>
                </div>
                  <div class="col-md-5">
                     <div class="left-modal">
                        <h2>LiveHelpout</h2>
                        <p>your instant 1:1 expert mentor helping you in real time.</p>
                        <p>Have an account? <a href="javascript:void(0);" ng-click="openModal('myModal');">Sign In</a></p>
                        <p><a href="javascript:void(0);">Want to become a Mentor?</a></p>
                     </div>
                  </div>
                  <div class="col-md-7">
                     <form>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Email address</label>
                           <input type="text" class="form-control" ng-model='email' id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <p>By clicking the button, you agree to the <a href="javascript:void(0);">terms</a></p>
                        <div class="form-group">
                            <button ng-click="signup();" data-style="zoom-in"  type="submit" ladda="loading" data-spinner-size="25" class="btn btn-cust-new">Submit</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
       </form>
      </div>
          
        <div class="custom-modal modal fade" id="myModal-3"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Forgot your Password?</h4>
               </div>
               <div class="modal-body">
                 <div ng-show="forgetpasswordalertmessage" class="{{alert.altclass}}">
                  <div id="flash_notice">{{alert.msg}}</div>
                 </div>
                  <form>
                     <div class="form-group">
                        <label class="col-sm-1 control-label"><span class="glyphicon glyphicon-user"></span></label>
                        <div class="col-sm-11">
                           <input type="text" placeholder="Email address" class="form-control" ng-model="forgotemail"/>
                        </div>
                     </div>
                   
                     <div class="form-group">
                        <div class="col-sm-12">
                           <button ng-click="forgotpassword();" data-style="zoom-in"  type="submit" ladda="loading" data-spinner-size="25" class="btn btn-cust-new">Submit</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
       </form>
      </div>

    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
        <footer class="footer_section">
         <div class="container">
            <div class="row">
               <div class="col-md-9 col-sm-6">
                  <div class="copyright wow bounceIn" data-wow-delay="0.4s">© 2015 LiveHelpout. All rights reserved.</div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="social">
                     <ul>
                        <li class="wow bounceInDown" data-wow-delay="0.4s"><img src="images/social-1.png" width="30" height="32"  alt=""/></li>
                        <li class="wow bounceInUp" data-wow-delay="0.4s"><img src="images/social-2.png" width="30" height="32"  alt=""/></li>
                        <li class="wow bounceInDown" data-wow-delay="0.4s"><img src="images/social-3.png" width="30" height="32"  alt=""/></li>
                        <li class="wow bounceInUp" data-wow-delay="0.4s"><img src="images/social-4.png" width="30" height="32"  alt=""/></li>
                        <li class="wow bounceInDown" data-wow-delay="0.4s"><img src="images/social-6.png" width="30" height="32"  alt=""/></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </footer>
     </div>
	<?php #echo $this->element('sql_dump'); ?>
</body>
</html>
