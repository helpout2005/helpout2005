<?php //echo $this->Html->script('ckeditor/ckeditor');?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
       <h3 class="page-title">
        Edit Group
       </h3>
       <ul class="breadcrumb">
           <li>
               <a href="#">Home</a>
               <span class="divider">/</span>
           </li>
           
           <li class="active">
              Edit Group
           </li>
           <!--<li class="pull-right search-wrap">
               <form action="search_result.html" class="hidden-phone">
                   <div class="input-append search-input-area">
                       <input class="" id="appendedInputButton" type="text">
                       <button class="btn" type="button"><i class="icon-search"></i> </button>
                   </div>
               </form>
           </li>-->
       </ul>
       <!-- END PAGE TITLE & BREADCRUMB-->
   </div>
</div>
<!-- END PAGE HEADER-->

 <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> <?php echo __('Edit Group'); ?></h4>
                <span class="tools">
                <a href="javascript:;" class="icon-chevron-down"></a>
                <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
             
            <div class="widget-body">
                <?php 
                    echo $this->Form->create('Group'); 
                    echo $this->Form->input('id',array('type'=>'hidden'));
                    //echo $this->Form->input('group_id',array('type'=>'hidden'));
                ?>
                    
                        
                        <div class="control-group">
                           
                            <div class="controls">
                                <?php echo $this->Form->input('title',array('required'=>'required')); ?>
                            </div>
                        </div>
			
                        <div class="control-group">
                            <label class="control-label">Users Group</label>
                        </div>
                        <?php 
                        $group = explode(',',$this->request->data['Group']['is_group']);
                        foreach($user as $use):
                        if($use['User']['name']!=''){
                        ?>
                        <div class="control-group">
                            <label class="control-label"><input type="checkbox" name="data[Group][is_group][]" value="<?php echo $use['User']['id'];?>" <?php echo (in_array($use['User']['id'], $group)?'checked':'');?>><?php echo $use['User']['name'] ;?></label>
                        </div>
			<?php }?>
                        <?php endforeach;?>
                        
                        <div class="control-group">
                            
                            <div class="controls">
                              <?php echo $this->Form->input('is_active',array('type'=>'checkbox','style'=>'  float: right;margin-right: -26px;')); ?>
 
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                 <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>