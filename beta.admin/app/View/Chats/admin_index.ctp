<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          Users
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <!--<li>
                <a href="#">Metro Lab</a>
                <span class="divider">/</span>
            </li>-->
            <li class="active">
                Users
            </li>
            <!--<li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>-->
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    <!-- BEGIN EXAMPLE TABLE widget-->
    <div class="widget red">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i> Users</h4>
                <!--<span class="tools">
                    <?php #echo $this->Html->link(__('Add New User'), array('controller' => 'users', 'action' => 'add')); ?>
                </span>-->
        </div>
        <div class="widget-body">
            <table class="table table-striped table-bordered" id="sample_1">
                <thead>
                <tr>
                    <th>From User</th>
                    <th>To User</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
	pr($chats);                    
                    foreach ($chats as $chat):?>
                        <tr>
                            <td>
                            <?php echo (!empty($chat['Chat']['from']['User']['name'])?$chat['Chat']['from']['User']['name']:''); ?>
                            </td>
                            <td>
                            <?php echo (!empty($chat['Chat']['to']['User']['name'])?$chat['Chat']['to']['User']['name']:''); ?>
                            </td>
                            <td>
                            <?php echo (!empty($chat['Chat']['date'])?$chat['Chat']['date']:''); ?>
                            </td>
                            <th>
                                <a href="" class="btn btn-primary">View</a>
                            </th>
                        </tr>
                   
                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul>
<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
