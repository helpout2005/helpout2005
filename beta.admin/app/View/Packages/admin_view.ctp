<div class="users index">
<div class="span9" id="content">
	<div class="row-fluid">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left"><?php echo __('View Package'); ?></div>
			</div>
			<div class="users view">
                            <dl>
				
                                <dt><?php echo __('Title'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['title']); ?>
                                        &nbsp;
                                </dd>
                                
                                <dt><?php echo __('Amount ($)'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['amount']); ?>
                                        &nbsp;
                                </dd>
                                
                                <dt><?php echo __('Credits'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['credits']); ?>
                                        &nbsp;
                                </dd>
                                
                                <dt><?php echo __('Credit Label'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['credit_label']); ?>
                                        &nbsp;
                                </dd>
                                
                                <dt><?php echo __('Slogan'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['slogan']); ?>
                                        &nbsp;
                                </dd>
                            

                                <dt><?php echo __('Description'); ?></dt>
                                <dd>
                                        <?php echo h($category['Package']['description']); ?>
                                        &nbsp;
                                </dd>	
                            </dl>
			</div>
			
		</div>
	</div>
</div>
</div>
<?php echo $this->element('admin_sidebar');?>
