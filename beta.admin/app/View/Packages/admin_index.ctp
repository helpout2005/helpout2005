<?php ?>
<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          Credit Packages
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <!--<li>
                <a href="#">Metro Lab</a>
                <span class="divider">/</span>
            </li>-->
            <li class="active">
                Packages
            </li>
            <!--<li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>-->
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    <!-- BEGIN EXAMPLE TABLE widget-->
    <div class="widget red">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i> Packages</h4>
                <span class="tools">
                    <?php #echo $this->Html->link(__('Add New Package'), array('controller' => 'packages', 'action' => 'add')); ?>
                </span>
        </div>
        <div class="widget-body">
            <table class="table table-striped table-bordered" id="sample_1">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('title'); ?></th>
                    <th><?php echo $this->Paginator->sort('amount','Amount ($)'); ?></th>
                    <th><?php echo $this->Paginator->sort('credits'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $category): ?>
                        <tr>
                            <td>
                                <?php echo $category['Package']['title'];?> 
                            </td>
                            <td>
                                <?php echo $category['Package']['amount'];?>
                            </td>
                            <td><?php echo $category['Package']['credits'];?></td>
                            <td class="actions actionbtn">
                             <a href="<?php echo $this->webroot;?>admin/packages/addsubcategory/<?php echo $category['Package']['id'];?>"><img src="<?php echo $this->webroot;?>img/subcat_add.png" title="Add Sub Package" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/packages/edit/<?php echo $category['Package']['id'];?>"><img src="<?php echo $this->webroot;?>img/edit.png" title="Edit Package" width="22" height="21"></a>

                             <!--<a href="<?php echo $this->webroot;?>admin/packages/delete/<?php echo $category['Package']['id'];?>" onclick="return confirm('Are you sure to delete this package?')"><img src="<?php echo $this->webroot;?>img/delete.png" title="Delete Package" width="24" height="24"></a>-->
                            </td>
                        </tr>
                   
                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul>
<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
