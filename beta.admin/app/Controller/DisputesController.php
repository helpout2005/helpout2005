<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class DisputesController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Dispute','Chat','Package','User','Payment','Token','Transfer','Notification','Help');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */
	
     
    public function save(){
		$rarray = array();
		if(!empty($_POST))
		{
			$dispute['Dispute']['help_id'] = $_POST['help_id'];
			$dispute['Dispute']['user_id'] = $_POST['user_id'];
                        
                        //$dispute['Dispute']['transfer_id'] = $_POST['transfer_id'];
                        $transfer = $this->Transfer->find('first',array('conditions' => array('Transfer.help_id' => $dispute['Dispute']['help_id'])));
			$dispute['Dispute']['message'] = $_POST['message'];
			$dispute['Dispute']['date'] = date('Y-m-d H:i:s');
			$dispute['Dispute']['is_resolved'] = 0;
			if($this->Dispute->save($dispute))
			{
                            $hlp['Help']['id'] = $dispute['Dispute']['help_id'];
                            $hlp['Help']['is_disputed'] = 1;
                            $this->Help->save($hlp);
                            
                            
                            $help_details = $this->Help->find('first',array('conditions' => array('Help.id' => $dispute['Dispute']['help_id'])));
                            if(!empty($transfer))
                            {
                                /***************** Send Notification *************/
                                $datetime = date('Y-m-d H:i:s');
                                $notification['Notification']['helpid'] = $dispute['Dispute']['help_id'];
                                $notification['Notification']['touserid'] = $transfer['Transfer']['receiverid'];
                                $notification['Notification']['fromuserid'] = $dispute['Dispute']['user_id'];
                                $notification['Notification']['date'] = $datetime;
                                $notification['Notification']['type'] = 'disputed';
                                $notification['Notification']['is_read'] = 0;
                                $notification['Notification']['notificationmsg'] = 'Disputed : '.$help_details['Help']['title'];
                                $notification['Notification']['parent_table'] = 'Transfer';
                                $notification['Notification']['parent_table_id'] = $transfer['Transfer']['id'];
                                 //pr($notification);
                                $this->Notification->create();
                                $this->Notification->save($notification);  
                            }
				$rarray = array('status' => 'success', 'message' => 'Disputed Successfully.');
			}
			else
			{
				$rarray = array('status' => 'danger', 'message' => 'Internal error. Please try again later.');
			}
			echo json_encode($rarray);
		}
		exit;
	}
    
}
