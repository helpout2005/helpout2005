<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class ChatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Chat','Package','User','Payment','Token');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */
	
     public function admin_index()
     {
        $userid = $this->Session->read('userid');
        if(!isset($userid) && $userid=='')
        {
                $this->redirect('/controlpanel');
        }
        $title_for_layout = "Conversation History";
        $this->Chat->recursive = 0;
        $this->Paginator->settings = array(
         'limit' =>15,
         /*'order' => array(
            'User.id' => 'desc'
         )*/
         'group' => array('Chat.from_user')
        );
        $chats = $this->Paginator->paginate('Chat');
        $chats = array_map(function($t){
            $t['Chat']['from'] = $this->User->find('first',array('conditions' => array('User.id' => $t['Chat']['from_user'])));
            $t['Chat']['to'] = $this->User->find('first',array('conditions' => array('User.id' => $t['Chat']['to_user'])));
            return $t;
        },$chats);
        
        $this->set(compact('title_for_layout','chats'));
     }
     
     public function admin_view()
     {
        $userid = $this->Session->read('userid');
        if(!isset($userid) && $userid=='')
        {
                $this->redirect('/controlpanel');
        }
        #$chat_history = $this->Chat->find('all',array('conditions' => array(array('User.from_user' => array('$in' => '569e1a42254092891773603a','User.to_user' => '569dee3d2540929e16736035'))));
        #$this->set(compact('chat_history'));
     }


     public function save()
     {
         if(!empty($_POST))
         {
             $data['Chat']['from_user'] = $_POST['from_user'];
             $data['Chat']['to_user'] = $_POST['to_user'];
             $data['Chat']['message'] = $_POST['message'];
             $data['Chat']['date'] = date('Y-m-d H:i:s');
             $this->Chat->save($data);
         }
         exit;
     }
     
     public function get_history(){
         if(!empty($_POST['from_user']) && !empty($_POST['to_user']))
         {
             $from_user = $_POST['from_user'];
             $to_user = $_POST['to_user'];
             $chats = $this->Chat->find('all',array('conditions' => 
                                                array('Chat.from_user' => array('$in'=>array($from_user,$to_user)),
                                                   'Chat.to_user' => array('$in'=>array($from_user,$to_user))
                                                 ),
                                            'order'=>array('modified'=>1)));
             $chats = array_map(function($t) use($from_user){
                 $temp = array();
                 $temp['message'] = $t['Chat']['message'];
                 if(($t['Chat']['from_user'] == $from_user))
                 {
                     $temp['my'] = true;
                 }
                 else
                 {
                     $temp['my'] = false;
                 }  
                 return $temp;
             },$chats);
             echo json_encode(array('status' => 'success', 'messages' => $chats));            
         } 
         exit;
     }
	
}
