<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class PackagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */
	public function admin_index() {	
		$title_for_layout = 'Package List';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Package->recursive = 0;
                $this->Paginator->settings = array(
                 'limit' =>15,
                 'order' => array(
                    'Package.id' => 'desc'
                 )
               );
		$this->set('categories', $this->Paginator->paginate('Package'));
		$this->set(compact('title_for_layout'));
	}

	

        public function getAll()
        {
           $packages =  $this->Package->find('all');
           echo json_encode($packages);
           exit;
        }
	


	public function catdetails(){
	$options = array('conditions' => array('Package.id' => $_REQUEST['id']));
		$categoryname = $this->Package->find('first', $options);
		if($categoryname['Package']['parent_id']==0){
			$image=$categoryname['Package']['category_image'];
		}else{
			$image='';
		}

	echo json_encode(array('id'=>$categoryname['Package']['id'],'parent_id'=>$categoryname['Package']['parent_id'],'name'=>$categoryname['Package']['name'],'is_active'=>$categoryname['Package']['is_active'],'image'=>$image));
	exit;

	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_view($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$title_for_layout = 'Package View';
		if (!$this->Package->exists($id)) {
			throw new NotFoundException(__('Invalid Package'));
		}
		$options = array('conditions' => array('Package.' . $this->Package->primaryKey => $id));
		$category = $this->Package->find('first', $options);
		
		$this->set(compact('title_for_layout','category'));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {			
		$title_for_layout = 'Package Add';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if ($this->request->is('post')) {
                    if ($this->Package->save($this->request->data)) {
                            $this->Session->setFlash('The package has been saved.', 'default', array('class' => 'success'));
                            return $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Session->setFlash(__('The package could not be saved. Please, try again.'));
                    }
		}
		$this->set(compact('title_for_layout'));
	}


	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */


	
	
	public function admin_edit($id = null) {
		//echo $id;
		//echo '&nbsp;&nbsp;';
		//echo $_SESSION['tmp_id']=$id;exit;
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->Package->exists($id)) {
			throw new NotFoundException(__('Invalid Package'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Package->save($this->request->data)) {
                              $this->Session->setFlash('The email template has been saved.', 'default', array('class' => 'success'));
                               return $this->redirect(array('action' => 'index'));
                              
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Package.' . $this->Package->primaryKey => $id));
			$this->request->data = $this->Package->find('first', $options);
                        //pr($this->request->data);
                        //exit;
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function deletesubcat(){
		$id=$_REQUEST['id'];
		if ($this->Package->delete($id)) {
			echo json_encode(array('error'=>0,'msg'=>'The category has been deleted.'));
			
		} else {
			echo json_encode(array('error'=>1,'msg'=>'The category could not be deleted. Please, try again.'));
		}
		exit;
	}

	public function deletecat(){
		$id=$_REQUEST['id'];
		$options1 = array('conditions' => array('Package.parent_id' => $id));
		$subcat = $this->Package->find('list', $options1);
		if($subcat){
			foreach($subcat as $k1=>$v1){
				$this->Package->delete($k1);
			}
		}
		
		$options2 = array('conditions' => array('Package.id' => $id));
		$maincat = $this->Package->find('first', $options2);
		
		if ($this->Package->delete($id)) {
			echo json_encode(array('error'=>0,'msg'=>'The category has been deleted.','image'=>$maincat['Package']['category_image']));
		} else {
			echo json_encode(array('error'=>1,'msg'=>'The category could not be deleted. Please, try again.'));
		}
exit;
	}
	
	public function admin_delete($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Package->id = $id;
		if (!$this->Package->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
	
		if ($this->Package->delete($id)) {
			$this->Session->setFlash('The category has been deleted.', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
