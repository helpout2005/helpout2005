<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'autoload', array('file' => 'opentok' . DS . 'autoload.php'));
use OpenTok\OpenTok;
//App::uses('OpenTok\OpenTok');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class OpentoksController extends AppController {

       
/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Opentok','Package','User','Payment','Token','Transfer');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */

    public function request_video()
    {
        $rarray = array();
        $opentok_api = Configure::read('OPENTOK_APIKEY');
        $opentok_secret = Configure::read('OPENTOK_SECRET');
        
        
       
        $opentok = new OpenTok($opentok_api, $opentok_secret);
        $session = $opentok->createSession();
        $sessionId = $session->getSessionId();
        $token = $opentok->generateToken($sessionId);
        
        $data['Opentok']['fromid'] = $_POST['fromid'];
        $data['Opentok']['toid'] = $_POST['toid'];
        $data['Opentok']['callstarted'] = $_POST['callstarted'];
        $data['Opentok']['sessionid'] = $sessionId;
        $data['Opentok']['tokenid'] = $token;
        if($this->Opentok->save($data))
        {
            $data['Opentok']['opentok_id'] = $this->Opentok->getLastInsertId();
            $data['Opentok']['fromname'] = $_POST['fromname'];
            $rarray = array('status' => 'success', 'data' => $data['Opentok']);
        }
        else
        {
            $rarray = array('status' => 'danger','message' => 'Internal Error. Please try again later.');
        }
        echo json_encode($rarray);
        exit;
    }
    
    public function get_details()
    {
        if(!empty($_POST['opentok_id']))
        {
            $opentok_id = $_POST['opentok_id'];
            $details = $this->Opentok->find('first',array('conditions' => array('Opentok.id' => $opentok_id)));
            if(!empty($details))
            {
                echo json_encode(array('status' => 'success','details' => $details['Opentok']));
            }
            else
            {
                echo json_encode(array('status' => 'danger','message' => 'Chat details not found'));
            }            
        }
        exit;
    }
}
