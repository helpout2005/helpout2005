<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class PaypalsController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Package','User','Payment','Token','Transfer');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */

    public function buy_credit()
    {
        $rarray = array();
        App::uses('Paypal', 'Paypal.Lib');

        $this->Paypal = new Paypal(array(
            'sandboxMode' => Configure::read('PAYPAL_SANDBOXMODE'),
            'nvpUsername' => Configure::read('PAYPAL_USERNAME'),
            'nvpPassword' => Configure::read('PAYPAL_PASSWORD'),
            'nvpSignature' => Configure::read('PAYPAL_SIGNATURE')
        ));
        $site_url = Configure::read('APP_SITE_URL');

        $packade_id = $_POST['package_id'];
        $user_id = $_POST['user_id'];
        $package_details = $this->Package->find('first',array('conditions' => array('Package.'.$this->Package->primaryKey => $packade_id)));
            
        if(!empty($package_details))
        {
            if(!empty($package_details['Package']['amount']) && $package_details['Package']['amount']>0)
            {
                try {
                    /*************** Saving Payment Details **************/
                    $payment_data['Payment']['userid'] = $user_id;
                    $payment_data['Payment']['packageid'] = $package_details['Package']['id'];
                    $payment_data['Payment']['amount'] = $package_details['Package']['amount'];
                    $payment_data['Payment']['credit_tokens'] = $package_details['Package']['credits'];
                    $payment_data['Payment']['type'] = 'paypal';
                    $payment_data['Payment']['status'] = false;
                    $saved = $this->Payment->save($payment_data);
                    $order = array(
                        'description' => 'Your purchase with LiveHelpOut',
                        'currency' => 'USD',
                        'return' => $site_url.'frontend/payment_return',
                        'cancel' => $site_url.'frontend/payment_return',
                        'custom' => $saved['Payment']['id'].'##'.$user_id,
                        'items' => array(
                            0 => array(
                                'name' => $package_details['Package']['title'],
                                'description' => $package_details['Package']['description'],
                                'subtotal' => $package_details['Package']['amount'],
                                'qty' => 1,
                            )
                        )
                    );
                            
                    $link =  $this->Paypal->setExpressCheckout($order);
                    $rarray = array('status' => 'success', 'message' => 'Please wait, we are redirecting you to Paypal...','link' => $link);
                   //$this->redirect($link);
                } catch (Exception $e) {
                    $rarray = array('status' => 'danger','message' => $e->getMessage());            
                }  
            }
            else {

            }
        }
        else {
            $rarray = array('status' => 'danger','message' => 'Invalid package');
        }
        echo json_encode($rarray);
        exit;
    }
    
    public function payment_details(){
        $rarray = array();
        App::uses('Paypal', 'Paypal.Lib');

        $token = $_POST['token'];
        $this->Paypal = new Paypal(array(
            'sandboxMode' => Configure::read('PAYPAL_SANDBOXMODE'),
            'nvpUsername' => Configure::read('PAYPAL_USERNAME'),
            'nvpPassword' => Configure::read('PAYPAL_PASSWORD'),
            'nvpSignature' => Configure::read('PAYPAL_SIGNATURE')
        ));
        try {
            $details = $this->Paypal->getExpressCheckoutDetails($token);
            if(!empty($details))
            {
                //pr($details);
                //exit;
                if($details['ACK'] == 'Success')
                {
                    
                    
                    $custom = explode('##',$details['CUSTOM']);
                    $payment_id = $custom['0'];
                    $user_id = $custom['1'];
                    $payment_details = $this->Payment->find('first',array('conditions' => array('Payment.'.$this->Payment->primaryKey => $payment_id)));
                    if(isset($payment_details['Payment']['status']) && $payment_details['Payment']['status']==false)
                    {
                        $user_details = $this->User->find('first', array('conditions' => array('User.'.$this->User->primaryKey => $user_id)));


                        /***************** Updating Payment Details*****************/
                        $payment_data['Payment']['id'] = $payment_id;
                        $payment_data['Payment']['status'] = true;
                        $this->Payment->save($payment_data);


                        /**************** Updating User Tokens *******************/
                        $user_data['User']['id'] = $user_id;
                        if(!empty($user_details['User']['credit_tokens']))
                        {
                            $user_data['User']['credit_tokens'] = $user_details['User']['credit_tokens'] + $payment_details['Payment']['credit_tokens'];
                        }
                        else
                        {
                            $user_data['User']['credit_tokens'] = $payment_details['Payment']['credit_tokens'];
                        }
                        $this->User->save($user_data);

                        /************** Saving Token Status *************/
                        $token_data['Token']['userid'] = $user_id;
                        $token_data['Token']['before'] = (!empty($user_details['User']['credit_tokens'])?$user_details['User']['credit_tokens']:0);
                        $token_data['Token']['after'] = $user_data['User']['credit_tokens'];
                        $token_data['Token']['tokens'] = $payment_details['Payment']['credit_tokens'];
                        $token_data['Token']['type'] = 'credit';
                        $this->Token->save($token_data);
                        $rarray = array('status' => 'success', 'message' => 'Payment successfull','user_id'=>$user_id);
                    }
                    else
                    {
                        $rarray = array('status' => 'danger','message' => 'Payment Unsuccessfull');
                    }
                }
                else
                {
                    $rarray = array('status' => 'danger','message' => 'Payment Unsuccessfull');
                }
            }
        } catch (Exception $e) {
            // $e->getMessage();
            $rarray = array('status' => 'danger','message' => 'Payment Unsuccessfull');
        }   
        echo json_encode($rarray);
        exit;
    }

    public function withdraw()
    {
        $rarray = array();
        App::import('Vendor', 'autoload', array('file' => 'stripe' . DS . 'autoload.php'));
        \Stripe\Stripe::setApiKey(Configure::read('STRIPE_API_KEY'));
        $api_id = Configure::read('STRIPE_API_KEY');

        $token = $_POST['token'];
        $amount = $_POST['amount']*100;
        $userid = $_POST['user_id'];
        $bank_id = $_POST['bank_id'];
        
        $camount = $_POST['amount'];
        
        try{
            $user_details = $this->User->find('first',array('conditions' => array('User.id' => $userid)));
            //pr($user_details);
            //echo $user_details['User']['credit_tokens'].' - '.$camount;
            if(!empty($user_details['User']['credit_tokens']) && $user_details['User']['credit_tokens']>$camount)
            {
                $recipient = \Stripe\Recipient::create(array(
                    "name" => "John Doe",
                    "type" => "individual",
                    "bank_account" => $token)
                ); 

                $url = 'https://api.stripe.com/v1/transfers'; 
                $ch = curl_init($url);  
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Bearer '.$api_id));
                curl_setopt($ch, CURLOPT_POSTFIELDS, 'amount='.$amount.'&currency=USD&recipient='.$recipient->id.'&destination='.$bank_id); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); 
                curl_close($ch);


                /*******************Deduct User credits*************/
                $deducted_amount = $user_details['User']['credit_tokens'] - $camount;
                $user_data['User']['id'] = $user_details['User']['id'];
                $user_data['User']['credit_tokens'] = $deducted_amount; 
                $this->User->save($user_data);
                $rarray = array('status' => 'success','message' => 'Withdraw successfull.');
            }
            else
            {
                $rarray = array('status' => 'danger','message' => "You dont have so much amount to withdraw.");
            }
            
            
            //echo 'success';
        }
        catch(\Stripe\Error\Card $e) {
           $rarray = array('status' => 'danger','message' => 'Invalid bank details');
           
         // The card has been declined
       }
       echo json_encode($rarray);
       exit;
    }
}
