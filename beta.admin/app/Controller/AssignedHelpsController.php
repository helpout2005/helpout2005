<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class AssignedHelpsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array('AssignedHelp','Interest','Notification','Help','User','EmailTemplate');

    /**
     * index method
     *
     * @return void
     */
   
   public function assign()
   {
       $rarray = array();
       $data['AssignedHelp']['helpeeid'] = $_REQUEST['helpeeid'];
       $data['AssignedHelp']['helperid'] = $_REQUEST['helperid'];
       $data['AssignedHelp']['helpid'] = $_REQUEST['helpid'];
       $interest_id = $_REQUEST['interest_id'];
       if($this->AssignedHelp->save($data))
       {
           
           $helper_details = $this->User->find('first',array('conditions' => array('User.id' => $_REQUEST['helperid'])));
           $help_details = $this->Help->find('first', array('conditions' => array('Help.id' => $_REQUEST['helpid'])));
           /************* Update Help Status In Progress & Helper ***************/
           $help_data['Help']['id'] = $help_details['Help']['id'];
           $help_data['Help']['status'] = 3;
           $help_data['Help']['helperid'] = $_REQUEST['helperid'];
           $this->Help->save($help_data);
           
           /************* Update Declined *****************/
           $allinterests = $this->Interest->find('all',array('conditions' => array('Interest.help_id' => $_REQUEST['helpid'])));
            
           foreach($allinterests as $val)
           {
                $find_interest['Interest']['id'] = $val['Interest']['id'];
                $find_interest['Interest']['status'] = 2;
                $this->Interest->save($find_interest);
           }
          
           /************* Update Accepted *************/
           $find_interest['Interest']['id'] = $interest_id;
           $find_interest['Interest']['status'] = 1;
           $this->Interest->save($find_interest);
           
           /***************** Send Notification *************/
            $datetime = date('Y-m-d H:i:s');
            $notification['Notification']['helpid'] = $_REQUEST['helpid'];
            $notification['Notification']['touserid'] = $_REQUEST['helperid'];
            $notification['Notification']['fromuserid'] = $_REQUEST['helpeeid'];
            $notification['Notification']['date'] = $datetime;
            $notification['Notification']['type'] = 'interestaccepted';
            $notification['Notification']['is_read'] = 0;
            $notification['Notification']['parent_table'] = 'Interest';
            $notification['Notification']['parent_table_id'] = $interest_id;
            //pr($notification);
            $this->Notification->create();
            $this->Notification->save($notification);
            
            /***************** Send Email **************/
            $siteurl = Configure::read('APP_SITE_URL');
            $REGISTRATION_EMAIL_TEMPLATE = Configure::read('SELECTEDHELP_EMAIL_TEMPLATE');
            $username = $helper_details['User']['name'];
            $help_title = $help_details['Help']['title'];
            $help_details = $help_details['Help']['details'];
            
            $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
            $mail_body =str_replace(array('[USER]','[REQUEST_TITLE]','[REQUEST_DESCRIPTION]'),array($username,$help_title,$help_details),$EmailTemplate['EmailTemplate']['content']);
            $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$helper_details['User']['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);
            
           $rarray = array('status' => 'success');
       }
       else {
           $rarray = array('status' => 'error');
       }
       echo json_encode($rarray);
       exit;
   }
    
    
    public function getAssignedOfHelpee($helpeeid)
    {
    	$rarray = array();
		$assigns = $this->AssignedHelp->find('all',array('conditions' => array("AssignedHelp.helpeeid" => $helpeeid)));
		if(!empty($assigns))
		{
			foreach($assigns as $assign)
			{
				$helper = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helperid'])));
				$temp =array();
				if(!empty($helper))
				{
                                        $temp['helper_id'] = $helper['User']['id'];
					$temp['helper_name'] = (!empty($helper['User']['name'])?$helper['User']['name']:'');
					$temp['helper_email'] = (!empty($helper['User']['email'])?$helper['User']['email']:'');
					$temp['helper_username'] = (!empty($helper['User']['username'])?$helper['User']['username']:'');
					$temp['helper_expertise'] = (!empty($helper['User']['expertise'])?$helper['User']['expertise']:'');
					$temp['helper_image'] = (!empty($helper['User']['image'])?$helper['User']['image']:'');
					$temp['helper_gpid'] = (!empty($helper['User']['gpid'])?$helper['User']['gpid']:'');
					$temp['helper_fbid'] = (!empty($helper['User']['fbid'])?$helper['User']['fbid']:'');
					$temp['helper_lnid'] = (!empty($helper['User']['lnid'])?$helper['User']['lnid']:'');
				}
				
				$help = $this->Help->find('first',array('conditions' => array('Help.id' => $assign['AssignedHelp']['helpid'])));
				if(!empty($help))
				{
                                        $temp['help_id'] = $help['Help']['id'];
					$temp['help_title'] = $help['Help']['title'];
					$temp['help_details'] = $help['Help']['details'];
					$temp['help_categories'] = $help['Help']['selectedmaincats'];
					$temp['help_budget'] = $help['Help']['budget'];
                                        $temp['help_paidamount'] = (!empty($help['Help']['paidamount'])?$help['Help']['paidamount']:0);
                                        $temp['help_escrowedamount'] = (!empty($help['Help']['escrowedamount'])?$help['Help']['escrowedamount']:0);
				}
				$rarray[] = $temp;				
			}
		}
		//pr($rarray);
		if(!empty($rarray))
		{
			echo json_encode(array('status' => 'success','data' => $rarray));
		}
		else
		{
			echo json_encode(array('status' => 'error','message' => 'No assigned helps found.'));
		}
		exit;
	}
        
    public function getAssignedOfHelper($helperid)
    {
        $rarray = array();
        $assigns = $this->AssignedHelp->find('all',array('conditions' => array("AssignedHelp.helperid" => $helperid)));
        
        if(!empty($assigns))
	{
            foreach($assigns as $assign)
            {
                $helper = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helpeeid'])));
                $temp =array();
                if(!empty($helper))
                {
                    $temp['helpee_id'] = $helper['User']['id'];
                    $temp['helpee_name'] = $helper['User']['name'];
                    $temp['helpee_email'] = $helper['User']['email'];
                    $temp['helpee_username'] = $helper['User']['username'];
                    //$temp['helpee_expertise'] = $helper['User']['expertise'];
                    $temp['helpee_image'] = $helper['User']['image'];
                    $temp['helpee_gpid'] = (!empty($helper['User']['gpid'])?$helper['User']['gpid']:'');
                    $temp['helpee_fbid'] = (!empty($helper['User']['fbid'])?$helper['User']['fbid']:'');
                    $temp['helpee_lnid'] = (!empty($helper['User']['lnid'])?$helper['User']['lnid']:'');
                }
                
                $help = $this->Help->find('first',array('conditions' => array('Help.id' => $assign['AssignedHelp']['helpid'])));
                if(!empty($help))
                {
                    $temp['help_id'] = $help['Help']['id'];
                    $temp['help_title'] = $help['Help']['title'];
                    $temp['help_details'] = $help['Help']['details'];
                    $temp['help_categories'] = $help['Help']['selectedmaincats'];
                    $temp['help_budget'] = $help['Help']['budget'];
                    $temp['help_paidamount'] = (!empty($help['Help']['paidamount'])?$help['Help']['paidamount']:0);
                    $temp['help_escrowedamount'] = (!empty($help['Help']['escrowedamount'])?$help['Help']['escrowedamount']:0);
                }
                $rarray[] = $temp;
            }
        }
        
        if(!empty($rarray))
        {
                echo json_encode(array('status' => 'success','data' => $rarray));
        }
        else
        {
                echo json_encode(array('status' => 'error','message' => 'No assigned helps found.'));
        }
        exit;
    }   
    
    public function assigned_help_details($help_id)
    {
        $rarray = array();
        $assign = $this->AssignedHelp->find('first',array('conditions' => array("AssignedHelp.helpid" => $help_id)));
        
        if(!empty($assign))
	{
            $helpee = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helpeeid'])));
            $temp =array();
            if(!empty($helpee))
            {
                $temp['helpee_id'] = $helpee['User']['id'];
                $temp['helpee_name'] = $helpee['User']['name'];
                $temp['helpee_email'] = $helpee['User']['email'];
                $temp['helpee_username'] = $helpee['User']['username'];
                //$temp['helpee_expertise'] = $helper['User']['expertise'];
                $temp['helpee_image'] = $helpee['User']['image'];
                $temp['helpee_gpid'] = (!empty($helpee['User']['gpid'])?$helpee['User']['gpid']:'');
                $temp['helpee_fbid'] = (!empty($helpee['User']['fbid'])?$helpee['User']['fbid']:'');
                $temp['helpee_lnid'] = (!empty($helpee['User']['lnid'])?$helpee['User']['lnid']:'');
            }
            
            $helper = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helperid'])));
            //$temp =array();
            if(!empty($helper))
            {
                $temp['helper_id'] = $helper['User']['id'];
                $temp['helper_name'] = $helper['User']['name'];
                $temp['helper_email'] = $helper['User']['email'];
                $temp['helper_username'] = $helper['User']['username'];
                //$temp['helpee_expertise'] = $helper['User']['expertise'];
                $temp['helper_image'] = $helper['User']['image'];
                $temp['helper_gpid'] = (!empty($helper['User']['gpid'])?$helper['User']['gpid']:'');
                $temp['helper_fbid'] = (!empty($helper['User']['fbid'])?$helper['User']['fbid']:'');
                $temp['helper_lnid'] = (!empty($helper['User']['lnid'])?$helper['User']['lnid']:'');
            }

            $help = $this->Help->find('first',array('conditions' => array('Help.id' => $assign['AssignedHelp']['helpid'])));
            if(!empty($help))
            {
                $temp['help_id'] = $help['Help']['id'];
                $temp['help_title'] = $help['Help']['title'];
                $temp['help_details'] = $help['Help']['details'];
                $temp['help_categories'] = $help['Help']['selectedmaincats'];
                $temp['help_budget'] = $help['Help']['budget'];
                $temp['help_paidamount'] = (!empty($help['Help']['paidamount'])?$help['Help']['paidamount']:0);
                $temp['help_escrowedamount'] = (!empty($help['Help']['escrowedamount'])?$help['Help']['escrowedamount']:0);
            }
            $rarray = $temp;
            
        }
        
        if(!empty($rarray))
        {
                echo json_encode(array('status' => 'success','data' => $rarray));
        }
        else
        {
                echo json_encode(array('status' => 'error','message' => 'No assigned helps found.'));
        }
        exit;
    }


    public function my_helps($user_id){
        $rarray = array();
        $helper_assigns = $this->AssignedHelp->find('all',array('conditions' => array("AssignedHelp.helperid" => $user_id)));
        $helpee_assigns = $this->AssignedHelp->find('all',array('conditions' => array("AssignedHelp.helpeeid" => $user_id)));
       
        $assigns = array_merge($helper_assigns,$helpee_assigns);
        
        
        if(!empty($assigns))
	{
             foreach($assigns as $assign)
            {
                $helpee = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helpeeid'])));
                $temp =array();
                if(!empty($helpee))
                {
                    $temp['helpee_id'] = $helpee['User']['id'];
                    $temp['helpee_name'] = $helpee['User']['name'];
                    $temp['helpee_email'] = $helpee['User']['email'];
                    $temp['helpee_username'] = $helpee['User']['username'];
                    //$temp['helpee_expertise'] = $helper['User']['expertise'];
                    $temp['helpee_image'] = $helpee['User']['image'];
                    $temp['helpee_gpid'] = (!empty($helpee['User']['gpid'])?$helpee['User']['gpid']:'');
                    $temp['helpee_fbid'] = (!empty($helpee['User']['fbid'])?$helpee['User']['fbid']:'');
                    $temp['helpee_lnid'] = (!empty($helpee['User']['lnid'])?$helpee['User']['lnid']:'');
                }
                $helper = $this->User->find('first',array('conditions' => array('User.id' => $assign['AssignedHelp']['helperid'])));
                if(!empty($helper))
                {
                    $temp['helper_id'] = $helper['User']['id'];
                    $temp['helper_name'] = (!empty($helper['User']['name'])?$helper['User']['name']:'');
                    $temp['helper_email'] = (!empty($helper['User']['email'])?$helper['User']['email']:'');
                    $temp['helper_username'] = (!empty($helper['User']['username'])?$helper['User']['username']:'');
                    $temp['helper_expertise'] = (!empty($helper['User']['expertise'])?$helper['User']['expertise']:'');
                    $temp['helper_image'] = (!empty($helper['User']['image'])?$helper['User']['image']:'');
                    $temp['helper_gpid'] = (!empty($helper['User']['gpid'])?$helper['User']['gpid']:'');
                    $temp['helper_fbid'] = (!empty($helper['User']['fbid'])?$helper['User']['fbid']:'');
                    $temp['helper_lnid'] = (!empty($helper['User']['lnid'])?$helper['User']['lnid']:'');
                }                
                
                $help = $this->Help->find('first',array('conditions' => array('Help.id' => $assign['AssignedHelp']['helpid'])));
                if(!empty($help))
                {
                    $temp['help_id'] = $help['Help']['id'];
                    $temp['help_title'] = $help['Help']['title'];
                    $temp['help_details'] = $help['Help']['details'];
                    $temp['help_categories'] = $help['Help']['selectedmaincats'];
                    $temp['help_budget'] = $help['Help']['budget'];
                    $temp['help_paidamount'] = (!empty($help['Help']['paidamount'])?$help['Help']['paidamount']:0);
                    $temp['help_escrowedamount'] = (!empty($help['Help']['escrowedamount'])?$help['Help']['escrowedamount']:0);
                }
                $rarray[] = $temp;
            }
        }
        if(!empty($rarray))
        {
                echo json_encode(array('status' => 'success','data' => $rarray));
        }
        else
        {
                echo json_encode(array('status' => 'error','message' => 'No assigned helps found.'));
        }
        exit;
    }
    
}
