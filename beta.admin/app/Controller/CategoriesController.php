<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


     public function getcategoriesall(){
        $type=  $_REQUEST['type'];
        if(isset($_REQUEST['parent_id']) && $_REQUEST['parent_id']!='')
        {
          $options = array('conditions' => array('Category.parent_id' => $_REQUEST['parent_id']));
        }
        else
        {
          if($type=='parent')
          {
            $options = array('conditions' => array('Category.parent_id' => 0));
          }
          else if($type=='child')
          {
            $options = array('conditions' => array('Category.parent_id' => array('$ne' => 0)));
          }
        }
        $category=$this->Category->find('all', $options);
        if(!empty($category)){
            //shuffle($category);
            echo json_encode(array("is_categories_exist"=>1,"msg"=>'Category Listing.','allcategories'=>$category));  
        }else{
           echo json_encode(array("is_categories_exist"=>0,"msg"=>'No category found.'));  
        }
        exit;
    }


     public function getcategories(){
        $type=  $_REQUEST['type'];
        if(isset($_REQUEST['parent_id']) && $_REQUEST['parent_id']!='')
        {
          $options = array('conditions' => array('Category.parent_id' => $_REQUEST['parent_id'], 'Category.is_active' => '1'));
        }
        else
        {
          if($type=='parent')
          {
            $options = array('conditions' => array('Category.parent_id' => 0, 'Category.is_active' => '1'));
          }
          else if($type=='child')
          {
            $options = array('conditions' => array('Category.parent_id' => array('$ne' => 0), 'Category.is_active' => '1'));
          }
        }
        $category=$this->Category->find('all', $options);
        if(!empty($category)){
            shuffle($category);
            $category = array_map(function($t){
                $subcats = $this->Category->find('all',array('conditions' => array('Category.parent_id' => $t['Category']['id'])));
                $t['Category']['subcats'] = $subcats;
                return $t;
            },$category);
            echo json_encode(array("is_categories_exist"=>1,"msg"=>'Category Listing.','allcategories'=>$category));  
        }else{
           echo json_encode(array("is_categories_exist"=>0,"msg"=>'No category found.'));  
        }
        exit;
    }
    
    public function getcategorieswithsubcats(){
        $resultcategories=array();
        $options = array('conditions' => array('Category.parent_id' => 0, 'Category.is_active' => '1'));
        $category=$this->Category->find('all', $options);
        if (!empty($category)) {
            foreach ($category as $v) 
            {
                $allsubcategories = array();
                $optionssubcategories = array('conditions' => array('Category.parent_id' => $v['Category']['id'],'Category.is_active' => '1'));
                $subcategories = $this->Category->find('all', $optionssubcategories);
                if(!empty($subcategories))
                {
                  foreach ($subcategories as $subcategory) 
                  {
                    $allsubcategories[]=array('id'=>$subcategory['Category']['id'],'name'=>$subcategory['Category']['name']);
                  }
                  $resultcategories[] =array('id'=>$v['Category']['id'],'name'=>$v['Category']['name'],'subcategories'=>$allsubcategories);
                }
            }
        }
        
        if(!empty($resultcategories)){
            //shuffle($resultcategories);
            echo json_encode(array("is_categories_exist"=>1,"msg"=>'Category Listing.','allcategorieswithsub'=>$resultcategories));  
        }else{
           echo json_encode(array("is_categories_exist"=>0,"msg"=>'No category found.'));  
        }
        exit;
    }
    
    public function getfilteredsubcategories(){
        $type=  $_REQUEST['type'];
        if(isset($_REQUEST['parent_ids']) && $_REQUEST['parent_ids']!='')
        {
          $parent_ids_exp=explode(',',trim($_REQUEST['parent_ids'],','));
        }
        else
        {
          $parent_ids_exp=array();
        }
        if(isset($_REQUEST['childids']) && $_REQUEST['childids']!='')
        {
          $child_ids_exp=explode(',',trim($_REQUEST['childids'],','));
        }
        else
        {
          $child_ids_exp=array();
        }
        if($type=='child')
        {
          $options = array('conditions' => array('Category.id' => array('$nin' => $child_ids_exp),'Category.parent_id' => array('$in' => $parent_ids_exp), 'Category.is_active' => '1'));
        }
        else if($type=='parent')
        {
          $options = array('conditions' => array('Category.id' => array('$nin' => $parent_ids_exp),'Category.parent_id'=>0, 'Category.is_active' => '1'));
        }
        $category=$this->Category->find('all', $options);
        if(!empty($category)){
            shuffle($category);
            
            echo json_encode(array("is_categories_exist"=>1,"msg"=>'Category Listing.','allcategories'=>$category));  
        }else{
           echo json_encode(array("is_categories_exist"=>0,"msg"=>'No category found.'));  
        }
        exit;
    }
    
    public function getsubcategories($key=null)
    {
        $options = array('conditions' => array('Category.parent_id'=>array('$ne' => 0),'Category.name'  => new MongoRegex("/^".$key."/i"), 'Category.is_active' => '1'));
        $category=$this->Category->find('all', $options);
        $autocomplete=array();
        if(!empty($category)){
            foreach($category as $cat) {
              $autocomplete[]=array("text"=>$cat['Category']['name']);
            }
        }else{
           
        }
        echo json_encode($autocomplete);  
        exit;
    }
    
    public function getselectedsubcats(){
        $type=  $_REQUEST['type'];
        $keyword=  $_REQUEST['keyword'];
        $blank=array();
        if($type=='parent')
        {
            $options = array('conditions' => array('Category.parent_id' => 0, 'Category.is_active' => '1','Category.name'  => new MongoRegex("/^".$keyword."/i")));
        }
        else if($type=='child')
        {
            if(isset($_REQUEST['parent_ids']) && $_REQUEST['parent_ids']!='')
            { 
              $parent_ids_exp=explode(',',trim($_REQUEST['parent_ids'],','));
            }
            else
            {
              $parent_ids_exp=array();
            }
            $options = array('conditions' => array('Category.parent_id' => array('$in' => $parent_ids_exp), 'Category.is_active' => '1','Category.name'  => new MongoRegex("/^".$keyword."/i")));
        }
        $category=$this->Category->find('all', $options);
        if(!empty($category)){
            echo json_encode(array("is_categories_exist"=>1,"msg"=>'Category Listing.','allcategories'=>$category));  
        }else{
           echo json_encode(array("is_categories_exist"=>0,"msg"=>'No category found.','allcategories'=>$blank));  
        }
        exit;
    }



/**
 * index method
 *
 * @return void
 */
	public function admin_index() {	
		$title_for_layout = 'Category List';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Category->recursive = 0;
                $this->Paginator->settings = array(
                 'limit' =>15,
                 'order' => array(
                    'Category.id' => 'desc'
                 )
               );
		$this->set('categories', $this->Paginator->paginate('Category', array('Category.parent_id' => 0)));
		$this->set(compact('title_for_layout'));
	}

	public function admin_subcategories($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
                $cat_id = $id;
		$title_for_layout = 'Sub Category List';
		$options = array('conditions' => array('Category.id' => $id));
		$categoryname = $this->Category->find('list', $options);
		if($categoryname){
			$categoryname = $categoryname[$id];
		} else {
			$categoryname = '';
		}
		$this->Category->recursive = 0;
                $this->Paginator->settings = array(
			        'limit' =>15,
			        'order' => array(
				        'Category.id' => 'desc'
			        )
                );
		$this->set('categories', $this->Paginator->paginate('Category', array('Category.parent_id' => $id)));
		$this->set(compact('title_for_layout','categoryname','cat_id'));
	}


	public function listsubcategories() {
		$parent=$_REQUEST['catid'];
		$title_for_layout = 'Sub Category List';
		$options = array('conditions' => array('Category.id' => $parent));
		$categoryname = $this->Category->find('list', $options);
		if($categoryname){
			$categoryname = $categoryname[$parent];
		} else {
			$categoryname = '';
		}
		$this->Category->recursive = 0;
                $this->Paginator->settings = array(
			        'limit' =>15,
			        'order' => array(
				        'Category.id' => 'desc'
			        )
                );
		$options = array('conditions' => array('Category.parent_id' => $parent));
		$subcategories = $this->Category->find('all', $options);
		$arr=array();
		foreach($subcategories as $subcategory){
		   $arr[]=array('id'=>$subcategory['Category']['id'],'name'=>$subcategory['Category']['name']);
		}

		if(!empty($arr)){
		echo json_encode(array("is_categories_exist"=>1,'parent_name'=>$categoryname,"msg"=>'Category Listing.','allsubcategories'=>$arr));	
		}else{
		echo json_encode(array("is_categories_exist"=>0,'parent_name'=>$categoryname,"msg"=>'Category Listing.','allsubcategories'=>$arr));
		}	
		exit;
	}


	public function catdetails(){
	$options = array('conditions' => array('Category.id' => $_REQUEST['id']));
		$categoryname = $this->Category->find('first', $options);
		if($categoryname['Category']['parent_id']==0){
			$image=$categoryname['Category']['category_image'];
		}else{
			$image='';
		}

	echo json_encode(array('id'=>$categoryname['Category']['id'],'parent_id'=>$categoryname['Category']['parent_id'],'name'=>$categoryname['Category']['name'],'is_active'=>$categoryname['Category']['is_active'],'image'=>$image));
	exit;

	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_view($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$title_for_layout = 'Category View';
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid Category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$category = $this->Category->find('first', $options);
		if($category){
			$options = array('conditions' => array('Category.id' => $category['Category']['parent_id']));
			$categoryname = $this->Category->find('list', $options);
			if($categoryname){
				$categoryname = $categoryname[$category['Category']['parent_id']];
			} else {
				$categoryname = '';
			}
		}
		$this->set(compact('title_for_layout','category','categoryname'));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {			
		$title_for_layout = 'Category Add';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if ($this->request->is('post')) {
			$options = array('conditions' => array('Category.name'  => $this->request->data['Category']['name'],'Category.parent_id'=>0));
			$name = $this->Category->find('first', $options);
			if(!$name){
			      if(isset($this->request->data['Category']['category_image']['name']) && $this->request->data['Category']['category_image']['name']!=''){
			                $ext = explode('.',$this->request->data['Category']['category_image']['name']);
			                if($ext){
				                $uploadFolder = "upload/category_images";
				                $uploadPath = WWW_ROOT . $uploadFolder;
				                $extensionValid = array('jpg','jpeg','png','gif');
				                if(in_array($ext[1],$extensionValid)){
					                $imageName = time().'_'.(strtolower(trim($this->request->data['Category']['category_image']['name'])));
					                $full_image_path = $uploadPath . '/' . $imageName;
					                move_uploaded_file($this->request->data['Category']['category_image']['tmp_name'],$full_image_path);
					                $this->request->data['Category']['category_image'] = $imageName;
				                }
				                else
				                {
				                  $this->request->data['Category']['category_image'] ='';
				                }
			                }
			                else
			                {
			                  $this->request->data['Category']['category_image'] ='';
			                }
		                }
		                else
		                {
		                  $this->request->data['Category']['category_image'] ='';
		                }
		                $this->request->data['Category']['parent_id']=0;
				if ($this->Category->save($this->request->data)) {
					$this->Session->setFlash('The category has been saved.', 'default', array('class' => 'success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
				}

			} else {
				$this->Session->setFlash(__('The category name already exists. Please, try again.'));
			}
		}
		$this->set(compact('title_for_layout'));
	}


	public function admin_addsubcategory($id = null) {	
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$id=$id;
		$title_for_layout = 'Sub Category Add';
		if ($this->request->is('post')) 
		{
			$options = array('conditions' => array('Category.name'=>$this->request->data['Category']['name'],'Category.parent_id'=>$this->request->data['Category']['parent_id']));
			$name=$this->Category->find('first',$options);
			if(!$name){
					$this->request->data['Category']['category_image'] ='';
					if ($this->Category->save($this->request->data)) {
					  $this->Session->setFlash('The sub category has been saved.', 'default', array('class' => 'success'));
					  return $this->redirect(array('action' => 'subcategories',$id));
					} 
					else {
					  $this->Session->setFlash(__('The sub category could not be saved. Please, try again.'));
					}
				} 
                           else 
			   {
				$this->Session->setFlash(__('The sub category name already exists. Please, try again.'));
			   }
		}

		$options = array('conditions' => array('Category.id' => $id));
		$categoryname = $this->Category->find('list', $options);
                
		if($categoryname){
			$categoryname = $categoryname[$id];
		} else {
			$categoryname = '';
		}
                                
		$this->set(compact('title_for_layout','categoryname','id'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */


	public function editsubcategory() 
	{
		$parent_id=$_REQUEST['parent_id'];
		$id=$_REQUEST['id'];
		$name=$_REQUEST['name'];
		$is_active=$_REQUEST['is_active'];
		if(isset($_REQUEST['image']) && $_REQUEST['image']!=''){
		$image=$_REQUEST['image'];
		}else{
		$image='';
		}

		if((int)$parent_id==0)
		     {
			$options = array('conditions' => array('Category.'. $this->Category->primaryKey=>array('$ne' => $id),'Category.parent_id'=>0,'Category.name'  => new MongoRegex("/".$name."/i")));
		     }
		     else
		     {
		       $options = array('conditions' => array('Category.'. $this->Category->primaryKey=>array('$ne' => $id),'Category.parent_id'=>$parent_id,'Category.name'  => new MongoRegex("/".$name."/i")));
		     }		  

		       //$options = array('conditions' => array('Category.'. $this->Category->primaryKey=>array('$ne' => $id),'Category.parent_id'=>$parent_id,'Category.name'  => new MongoRegex("/".$name."/i")));
		     
                        $namechk = $this->Category->find('all', $options);
	
			if(!$namechk)
			{
				if($id!=''){
                                $categoryupdate['Category']['id']=$id;
				}else{
					if($parent_id=='0'){
						$categoryupdate['Category']['parent_id']=(int)$parent_id;
					}else{
						$categoryupdate['Category']['parent_id']=$parent_id;
					}

				}
				
				$categoryupdate['Category']['category_image']=$image;
                                $categoryupdate['Category']['name']=$name;
                                $categoryupdate['Category']['is_active']=$is_active;
				if($id==''){
					$this->Category->create();
				}

				if ($this->Category->save($categoryupdate)) 
				{                    
				  echo json_encode(array('error'=>0,'msg'=>'The category has been saved.'));

				   
				} 
                                else 
				{
					
					echo json_encode(array('error'=>1,'msg'=>'The category could not be saved. Please, try again.'));
				}
			} 
                        else 
			{
				
				echo json_encode(array('error'=>1,'msg'=>'The category already exists. Please, try again.'));
			}
               
		exit;
	}
	
	public function admin_edit1() 
	{
		
		  if($this->request->data['Category']['parent_id']==0)
		     {
			$options = array('conditions' => array('Category.'. $this->Category->primaryKey=>array('$ne' => $id),'Category.parent_id'=>0,'Category.name'  => new MongoRegex("/".$this->request->data['Category']['name']."/i")));
		     }
		     else
		     {
		       $options = array('conditions' => array('Category.'. $this->Category->primaryKey=>array('$ne' => $id),'Category.parent_id'=>$this->request->data['Category']['parent_id'],'Category.name'  => new MongoRegex("/".$this->request->data['Category']['name']."/i")));
		     }
                        $name = $this->Category->find('all', $options);
	
			if(!$name)
			{
				$options3=array('conditions' => array('Category.'. $this->Category->primaryKey => $id));
				$cat=$this->Category->find('first', $options3);
                                if($cat['Category']['parent_id']!=0)
				{
					$redirectid=$cat['Category']['parent_id'];
				}
				else
				{
                                         $redirectid='';
				}
				$categoryupdate['Category']['id']=$id;
				
				if($cat['Category']['parent_id']==0)
				{
				  if(isset($this->request->data['Category']['new_category_image']['name']) && $this->request->data['Category']['new_category_image']['name']!=''){
			                $ext = explode('.',$this->request->data['Category']['new_category_image']['name']);
			                if($ext){
				                $uploadFolder = "upload/category_images";
				                $uploadPath = WWW_ROOT . $uploadFolder;
				                $extensionValid = array('jpg','jpeg','png','gif');
				                if(in_array($ext[1],$extensionValid)){
					                $imageName = time().'_'.(strtolower(trim($this->request->data['Category']['new_category_image']['name'])));
					                $full_image_path = $uploadPath . '/' . $imageName;
					                move_uploaded_file($this->request->data['Category']['new_category_image']['tmp_name'],$full_image_path);
					                $categoryupdate['Category']['category_image'] = $imageName;
					                
					                $unlink_image_path = $uploadPath . '/' . $cat['Category']['category_image'];
					                unlink($unlink_image_path);
				                }
				                else
				                {
				                   $categoryupdate['Category']['category_image'] = $this->request->data['Category']['hid_category_image'];
				                }
			                }
			                else
			                {
			                   $categoryupdate['Category']['category_image'] = $this->request->data['Category']['hid_category_image'];
			                }
		                  }
		                  else
		                  {
		                    $categoryupdate['Category']['category_image'] = $this->request->data['Category']['hid_category_image'];
		                  }
		                }
		                else
		                {
		                  $categoryupdate['Category']['category_image']='';
		                }
		                
		                if($cat['Category']['parent_id']!=0)
				{
					$categoryupdate['Category']['parent_id']=$this->request->data['Category']['parent_id'];
				}
				else
				{
                                         $categoryupdate['Category']['parent_id']=0;
				}
                                
                                $categoryupdate['Category']['name']=$this->request->data['Category']['name'];
                                $categoryupdate['Category']['is_active']=$this->request->data['Category']['is_active'];
				if ($this->Category->save($categoryupdate)) 
				{                    
				  //echo json_encode('error'=>0,'msg'=>'The category has been saved.');

				   
				} 
                                else 
				{
					
					//echo json_encode('error'=>1,'msg'=>'The category could not be saved. Please, try again.');
				}
			} 
                        else 
			{
				
				//echo json_encode('error'=>1,'msg'=>'The category already exists. Please, try again.');
			}
               
		exit;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function deletesubcat(){
		$id=$_REQUEST['id'];
		if ($this->Category->delete($id)) {
			echo json_encode(array('error'=>0,'msg'=>'The category has been deleted.'));
			
		} else {
			echo json_encode(array('error'=>1,'msg'=>'The category could not be deleted. Please, try again.'));
		}
		exit;
	}

	public function deletecat(){
		$id=$_REQUEST['id'];
		$options1 = array('conditions' => array('Category.parent_id' => $id));
		$subcat = $this->Category->find('list', $options1);
		if($subcat){
			foreach($subcat as $k1=>$v1){
				$this->Category->delete($k1);
			}
		}
		
		$options2 = array('conditions' => array('Category.id' => $id));
		$maincat = $this->Category->find('first', $options2);
		
		if ($this->Category->delete($id)) {
			echo json_encode(array('error'=>0,'msg'=>'The category has been deleted.','image'=>$maincat['Category']['category_image']));
		} else {
			echo json_encode(array('error'=>1,'msg'=>'The category could not be deleted. Please, try again.'));
		}
exit;
	}
	
	public function admin_delete($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		
		$options1 = array('conditions' => array('Category.parent_id' => $id));
		$subcat = $this->Category->find('list', $options1);
		if($subcat){
			foreach($subcat as $k1=>$v1){
				$this->Category->delete($k1);
			}
		}
		
		$options2 = array('conditions' => array('Category.id' => $id));
		$maincat = $this->Category->find('first', $options2);
		if($maincat)
		{
		   $uploadFolder = "upload/category_images";
		   $uploadPath = WWW_ROOT . $uploadFolder;
		   $imageName=$maincat['Category']['category_image'];
		   $full_image_path = $uploadPath . '/' . $imageName;
		   unlink($full_image_path);
		}
	
		if ($this->Category->delete($id)) {
			$this->Session->setFlash('The category has been deleted.', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
