<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class TransfersController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Transfer','Package','User','Payment','Token','Help','SiteSetting','EmailTemplate','Notification');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */

    
    public function escrow_credit()
    {
        $rarray = array();
        $payer_id = $_POST['payer_id'];
        $receiver_id = $_POST['receiver_id'];
        $amount = $_POST['amount'];
        $help_id = $_POST['help_id'];
        
        $payer_details = $this->User->find('first',array('conditions' => array('User.id' => $payer_id)));
        $help_details = $this->Help->find('first', array('conditions' => array('Help.id' => $help_id)));
        $reciever_details = $this->User->find('first',array('conditions' => array('User.id' => $receiver_id)));
        $site_wallet_details = $this->SiteSetting->find('first');
        if(!empty($site_wallet_details['SiteSetting']['amount']))
        {
            $site_balance = $site_wallet_details['SiteSetting']['amount'];
        }
        else
        {
            $site_balance = 0;
        }
        if(!empty($help_details))
        {
            if(!empty($payer_details))
            {
                if((!empty($payer_details['User']['credit_tokens']) && $payer_details['User']['credit_tokens']>0 && $payer_details['User']['credit_tokens']>=$amount) || (!empty($payer_details['User']['stripe_id'])))
                {
                    try
                    {
                        if(!empty($payer_details['User']['stripe_id']))
                        {
                            /*********** Creating Charge From Saved card *************/
                            App::import('Vendor', 'autoload', array('file' => 'stripe' . DS . 'autoload.php'));
                            \Stripe\Stripe::setApiKey(Configure::read('STRIPE_API_KEY'));

                            $charge = \Stripe\Charge::create(array(
                                "amount" => $amount * 100, // amount in cents, again
                                "currency" => "usd",
                                "customer" => $payer_details['User']['stripe_id']
                              ));
                             /*************** Saving Payment Details of Payer **************/
                            $payment_data['Payment']['userid'] = $payer_id;
                            $payment_data['Payment']['packageid'] = 'From Card';
                            $payment_data['Payment']['amount'] = $amount;
                            $payment_data['Payment']['credit_tokens'] = $amount;
                            $payment_data['Payment']['chargeid'] = $charge->id;
                            $payment_data['Payment']['txnid'] = $charge->balance_transaction;
                            $payment_data['Payment']['type'] = 'stripe';
                            $payment_data['Payment']['status'] = true;
                            $this->Payment->save($payment_data);

                        }
                        /************************** Adding Transfer ********************/
                        $transfer_data['Transfer']['payerid'] = $payer_id;
                        $transfer_data['Transfer']['receiverid'] = $receiver_id;
                        $transfer_data['Transfer']['help_id'] = $help_id;
                        $transfer_data['Transfer']['amount'] = $amount;
                        $transfer_data['Transfer']['status'] = 'escrowed'; //escrowed,released,canceled
                        $transfer_data['Transfer']['date'] = date('Y-m-d H:i:s');
                        $save_transfer = $this->Transfer->save($transfer_data);
                        if($save_transfer)
                        {
                            if(empty($payer_details['User']['stripe_id']))
                            {
                                /*******************Deduct Payer credits*************/
                                $deducted_amount = $payer_details['User']['credit_tokens'] - $amount;
                                $user_data['User']['id'] = $payer_id;
                                $user_data['User']['credit_tokens'] = $deducted_amount; 
                                $this->User->save($user_data);

                                /********************* Saving Token Status ************/
                                $token_data['Token']['userid'] = $payer_id;
                                $token_data['Token']['before'] = $payer_details['User']['credit_tokens'];
                                $token_data['Token']['after'] = $deducted_amount;
                                $token_data['Token']['tokens'] = $amount;
                                $token_data['Token']['type'] = 'debit';
                                $this->Token->create();
                                $this->Token->save($token_data);
                                
                            }

                            /********************* Saving Site Wallet ****************/
                            $site_data['SiteSetting']['id'] =  $site_wallet_details['SiteSetting']['id'];
                            $site_data['SiteSetting']['amount'] = $site_balance + $amount;
                            $this->SiteSetting->save($site_data);
                            
                            /********************* Saving Token Status Of Site *********/
                            $site_token['Token']['userid'] = 'site';
                            $site_token['Token']['before'] = $site_balance;
                            $site_token['Token']['after'] = $site_balance + $amount;
                            $site_token['Token']['tokens'] = $amount;
                            $site_token['Token']['type'] = 'credit';
                            $this->Token->create();
                            $this->Token->save($site_token);

                            /****************** Saving help escrowed amount ******************/
                            $oldescrowed_amount = (!empty($help_details['Help']['escrowedamount'])?$help_details['Help']['escrowedamount']:0);
                            $help_data['Help']['id'] = $help_id;
                            $help_data['Help']['escrowedamount'] = $oldescrowed_amount + $amount;
                            $this->Help->save($help_data);
                            
                            /***************** Send Notification *************/
                            $datetime = date('Y-m-d H:i:s');
                            $notification['Notification']['helpid'] = $help_id;
                            $notification['Notification']['touserid'] = $receiver_id;
                            $notification['Notification']['fromuserid'] = $payer_id;
                            $notification['Notification']['date'] = $datetime;
                            $notification['Notification']['type'] = 'escrowed';
                            $notification['Notification']['is_read'] = 0;
                            $notification['Notification']['notificationmsg'] = '$'.$amount.' escrowed for request: '.$help_details['Help']['title'];
                            $notification['Notification']['parent_table'] = 'Transfer';
                            $notification['Notification']['parent_table_id'] = $this->Transfer->getInsertID();
                             //pr($notification);
                            $this->Notification->create();
                            $this->Notification->save($notification);
                            
                            
                            /***************** Send Email **************/
                            $siteurl = Configure::read('APP_SITE_URL');
                            $REGISTRATION_EMAIL_TEMPLATE = Configure::read('ESCROWED_EMAIL_TEMPLATE');
                            $username = $reciever_details['User']['name'];
                            $help_title = $help_details['Help']['title'];                           

                            $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
                            $mail_body =str_replace(array('[USER]','[REQUEST_TITLE]','[AMOUNT]'),array($username,$help_title,$amount),$EmailTemplate['EmailTemplate']['content']);
                            $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$reciever_details['User']['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);
            
            
                            
                           
                            

                            $rarray = array('status' => 'success','message' => 'Your amount transfered successfully.');
                        }
                        else
                        {
                            $rarray = array('status' => 'danger','message' => 'Internal error please try again later.');
                        }    
                    }
                    catch (\Stripe\Error\Card $e){
                        $rarray = array('status' => 'danger','message' => 'Invalid card details.');
                    }

                }
                else
                {
                    $rarray = array('status' => 'danger','message' => 'No credit tokens available.','redirect'=>'buy_credit');
                }
            }
            else {
                $rarray = array('status' => 'danger','message' => 'User not found.');
            }
        }
        else
        {
            $rarray = array('status' => 'danger','message' => 'Help not found.');
        }
        echo json_encode($rarray);
        exit;
    }
    
    
    
    public function gethelptransfers($help_id)
    {
        $rarray = array();
        $transfers = $this->Transfer->find('all',array('conditions' => array('Transfer.help_id' => $help_id)));
        if(!empty($transfers))
        {
            $transfers = array_map(function($t){return $t['Transfer'];},$transfers);
            $rarray = array('status' => 'success','data' => $transfers);
        }
        else
        {
            $rarray = array('status' => 'danger','message' => 'No transfers found.');
        }
        echo json_encode($rarray);
        exit;
        
    }
    
    public function gethelptransfer($help_id)
    {
        $rarray = array();
        $transfers = $this->Transfer->find('first',array('conditions' => array('Transfer.help_id' => $help_id)));
        if(!empty($transfers))
        {
            $transfer = $transfers['Transfer'];
            $rarray = array('status' => 'success','data' => $transfer);
        }
        else
        {
            $rarray = array('status' => 'danger','message' => 'No transfers found.');
        }
        echo json_encode($rarray);
        exit;
        
    }
    
    
    public function request_release($transfer_id)
    {
        $rarray = array();
        $transfer_details = $this->Transfer->find('first',array('conditions' => array('Transfer.id' => $transfer_id)));
        if(!empty($transfer_id))
        {
            //pr($transfer_details);
            $payer_details = $this->User->find('first',array('conditions' => array('User.id' => $transfer_details['Transfer']['payerid'])));
            
            $receiver_details = $this->User->find('first',array('conditions' => array('User.id' => $transfer_details['Transfer']['receiverid'])));
            
            $help_details = $this->Help->find('first', array('conditions' => array('Help.id' => $transfer_details['Transfer']['help_id'])));
            if(!empty($payer_details) && (!empty($receiver_details)))
            {
                if(!empty($help_details))
                {
                    /********************* Sending Notification ****************/
                    $datetime = date('Y-m-d H:i:s');
                    $notification['Notification']['helpid'] = $transfer_details['Transfer']['help_id'];
                    $notification['Notification']['touserid'] = $transfer_details['Transfer']['payerid'];
                    $notification['Notification']['fromuserid'] = $transfer_details['Transfer']['receiverid'];
                    $notification['Notification']['date'] = $datetime;
                    $notification['Notification']['type'] = 'request_release';
                    $notification['Notification']['is_read'] = 0;
                    $notification['Notification']['notificationmsg'] = $receiver_details['User']['name'].' is requested to relaese the escrowed amount.';
                    $notification['Notification']['parent_table'] = 'Transfer';
                    $notification['Notification']['parent_table_id'] = $transfer_id;
                     //pr($notification);
                    $this->Notification->create();
                    $this->Notification->save($notification);
                    
                    
                    /***************** Send Email **************/
                    $siteurl = Configure::read('APP_SITE_URL');
                    $REGISTRATION_EMAIL_TEMPLATE = Configure::read('RELEASEREQUEST_EMAIL_TEMPLATE');
                    $username = $payer_details['User']['name'];
                    $helper_name = $receiver_details['User']['name'];
                    $help_title = $help_details['Help']['title'];                           

                    $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
                    $mail_body =str_replace(array('[USER]','[REQUEST_TITLE]','[HELPER]'),array($username,$help_title,$helper_name),$EmailTemplate['EmailTemplate']['content']);
                    $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$payer_details['User']['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);
                    
                    $rarray = array('status' => 'success','message' => 'Your request has been sent.');
                    
                }
                else {
                    $rarray = array('status' => 'danger','message' => 'Request not found.');
                }
            }
            else {
                $rarray = array('status' => 'danger','message' => 'Users not found.');
            }
        }
        else {
            $rarray = array('status' => 'danger','message' => 'No transfers found.');
        }
        echo json_encode($rarray);
        exit;
    }
    
    public function release_escrow()
    {
        /*$data['SiteSetting']['siteid'] = "1";
        $data['SiteSetting']['site_percentage'] = "5";
        $data['SiteSetting']['amount'] = "1655";
        $s = $this->SiteSetting->save($data);
        var_dump($s);
        exit;*/
        $rarray = array();
        $transfer_id = (!empty($_POST['transfer_id'])?$_POST['transfer_id']:'');
		$help_id = (!empty($_POST['help_id'])?$_POST['help_id']:'');
        $site_settings = $this->SiteSetting->find('first');
		if(!empty($help_id))
		{
			$transfer_details = $this->Transfer->find('first',array('conditions' => array('Transfer.help_id' => $help_id)));
			$transfer_id = $transfer_details['Transfer']['id'];
		}
		else
		{
        $transfer_details = $this->Transfer->find('first',array('conditions' => array('Transfer.id' => $transfer_id)));
		}
        if(!empty($transfer_id))
        {
            $payer_details = $this->User->find('first',array('conditions' => array('User.id' => $transfer_details['Transfer']['payerid'])));
            
            $receiver_details = $this->User->find('first',array('conditions' => array('User.id' => $transfer_details['Transfer']['receiverid'])));
            
            $help_details = $this->Help->find('first', array('conditions' => array('Help.id' => $transfer_details['Transfer']['help_id'])));
            if(!empty($payer_details) && (!empty($receiver_details)))
            {
                if(!empty($help_details))
                {
                    $amount = $transfer_details['Transfer']['amount'];
                    /************ Deduct Site Percentage ***********/
                    $deducted_amount = ($amount - (($amount*$site_settings['SiteSetting']['site_percentage'])/100));
                    //echo $amount.' - '.$deducted_amount;
                    /************************** Updating Transfer ********************/
                    $transfer_data['Transfer']['id'] = $transfer_id;
                    $transfer_data['Transfer']['status'] = 'released'; //escrowed,released,canceled
                    $save_transfer = $this->Transfer->save($transfer_data);
                    if($save_transfer)
                    {
                        /************************ Update Receiver Credit *************/
                        $receiver_amount = (!empty($receiver_details['User']['credit_tokens'])?$receiver_details['User']['credit_tokens']:0);
                        $receiver_data['User']['id'] = $receiver_details['User']['id'];
                        $receiver_data['User']['credit_tokens'] = $receiver_amount + $deducted_amount;
                        $this->User->save($receiver_data);
                        
                        /*********************** Saving Receiver Token Status *************/
                        $token_data['Token']['userid'] = $receiver_details['User']['id'];
                        $token_data['Token']['before'] = $receiver_amount;
                        $token_data['Token']['after'] = $receiver_amount + $deducted_amount;
                        $token_data['Token']['tokens'] = $deducted_amount;
                        $token_data['Token']['type'] = 'credit';
                        $this->Token->create();
                        $this->Token->save($token_data);
                        
                        
                        /********************** Updating Site Wallet *********************/
                        $site_data['SiteSetting']['id'] =  $site_settings['SiteSetting']['id'];
                        $site_data['SiteSetting']['amount'] = $site_settings['SiteSetting']['amount'] - $deducted_amount;
                        $this->SiteSetting->save($site_data);
                        
                        /********************* Saving Token Status Of Site *********/
                        $site_token['Token']['userid'] = 'site';
                        $site_token['Token']['before'] = $site_settings['SiteSetting']['amount'];
                        $site_token['Token']['after'] = $site_settings['SiteSetting']['amount'] - $deducted_amount;
                        $site_token['Token']['tokens'] = $deducted_amount;
                        $site_token['Token']['type'] = 'debit';
                        $this->Token->create();
                        $this->Token->save($site_token);
                        
                        /****************** Saving help escrowed released amount ******************/
                        $oldescrowed_amount = (!empty($help_details['Help']['escrowedamount'])?$help_details['Help']['escrowedamount']:0);
                        $oldpaid_amount = (!empty($help_details['Help']['paidamount'])?$help_details['Help']['paidamount']:0);
                        $help_data['Help']['id'] = $help_details['Help']['id'];
                        $help_data['Help']['escrowedamount'] = $oldescrowed_amount - $amount;
                        $help_data['Help']['paidamount'] = $oldpaid_amount + $amount;
                        $this->Help->save($help_data);
                        
                        /***************** Send Notification *************/
                        $datetime = date('Y-m-d H:i:s');
                        $notification['Notification']['helpid'] = $help_details['Help']['id'];
                        $notification['Notification']['touserid'] = $receiver_details['User']['id'];
                        $notification['Notification']['fromuserid'] = $payer_details['User']['id'];
                        $notification['Notification']['date'] = $datetime;
                        $notification['Notification']['type'] = 'payment_released';
                        $notification['Notification']['is_read'] = 0;
                        $notification['Notification']['notificationmsg'] = 'Your amount has been released: '.$help_details['Help']['title'];
                        $notification['Notification']['parent_table'] = 'Transfer';
                        $notification['Notification']['parent_table_id'] = $transfer_id;
                         //pr($notification);
                        $this->Notification->create();
                        $this->Notification->save($notification);
                        
                        /***************** Send Email **************/
                        /*$siteurl = Configure::read('APP_SITE_URL');
                        $REGISTRATION_EMAIL_TEMPLATE = Configure::read('RELEASEREQUEST_EMAIL_TEMPLATE');
                        $username = $payer_details['User']['name'];
                        $helper_name = $receiver_details['User']['name'];
                        $help_title = $help_details['Help']['title'];                           

                        $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
                        $mail_body =str_replace(array('[USER]','[REQUEST_TITLE]','[HELPER]'),array($username,$help_title,$helper_name),$EmailTemplate['EmailTemplate']['content']);
                        $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$payer_details['User']['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);*/
                        
						/******************* Mark As Solved ******************/
						$help_solve['Help']['id'] = $help_details['Help']['id'];
						$help_solve['Help']['status'] = 1;
						$help_solve['Help']['completed_datetime'] = date('Y-m-d H:i:s');
						$this->Help->save($help_solve);
				
				
                        $rarray = array('status' => 'success','message' => 'Payment released successfully.');
                        
                    }
                }
                else {
                    $rarray = array('status' => 'danger','message' => 'Request not found.');
                }
            }
            else {
                $rarray = array('status' => 'danger','message' => 'Users not found.');
            }
        }
        else {
            $rarray = array('status' => 'danger','message' => 'No transfers found.');
        }
        echo json_encode($rarray);
        exit;
    }
    
    public function my_tranfers($userid)
    {
        $rarray = array();
        $transfers = $this->Token->find('all',array('conditions' => array('Token.userid' => $userid),'order'=>array('modified'=>-1)));
        if(!empty($transfers))
        {
            $transfers = array_map(function($t){
                $t['Token']['date'] = date('m/d/Y',  $t['Token']['created']->sec);
                return $t['Token'];
            }, $transfers);
            $rarray = array('status' => 'success', 'data' => $transfers);
        }
        else {
            $rarray = array('status' => 'danger', 'message' => 'No transactions found.');
        }
        echo json_encode($rarray);
        exit;
    }
	
	public function get_my_escrowed_total($userid) 
	{
		$escrowed_amount = 0;
		$transfers = $this->Transfer->find('all',array('conditions' => array('Transfer.payerid' => $userid,'Transfer.status' => 'escrowed')));
		foreach($transfers as $transfer)
		{
			if(!empty($transfer['Transfer']['amount']))
			{
				$escrowed_amount += $transfer['Transfer']['amount'];
			}
		}
		echo number_format($escrowed_amount,2);
		exit;
	}
    
}
