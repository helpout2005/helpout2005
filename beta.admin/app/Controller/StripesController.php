<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 */
class StripesController extends AppController {

/**
 * Components
 *
 * @var array
 */
     public $components = array('Paginator');
     public $uses = array('Package','User','Payment','Token');
        
     public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Categories.order_rank' => 'desc'
           )
     ); 


/**
 * index method
 *
 * @return void
 */
	public function admin_index() {	
		$title_for_layout = 'Package List';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Package->recursive = 0;
                $this->Paginator->settings = array(
                 'limit' =>15,
                 'order' => array(
                    'Package.id' => 'desc'
                 )
               );
		$this->set('categories', $this->Paginator->paginate('Package'));
		$this->set(compact('title_for_layout'));
	}

	

       

        public function buy_credit()
        {
            $rarray = array();
            App::import('Vendor', 'autoload', array('file' => 'stripe' . DS . 'autoload.php'));
            \Stripe\Stripe::setApiKey(Configure::read('STRIPE_API_KEY'));

            // Get the credit card details submitted by the form
            $token = $_POST['stripeToken'];            
            $packade_id = $_POST['package_id'];
            $user_id = $_POST['user_id'];
            
            $user_details = $this->User->find('first', array('conditions' => array('User.'.$this->User->primaryKey => $user_id)));

            $package_details = $this->Package->find('first',array('conditions' => array('Package.'.$this->Package->primaryKey => $packade_id)));
            
            if(!empty($package_details))
            {
                if(!empty($package_details['Package']['amount']) && $package_details['Package']['amount']>0)
                {
                    try {
                        $charge = \Stripe\Charge::create(array(
                            "amount" => $package_details['Package']['amount'] * 100, // amount in cents, again
                            "currency" => "usd",
                            "source" => $token,
                            "description" => $package_details['Package']['title'].' by '.$user_details['User']['name']
                        ));
                        if(!empty($charge))
                        {
                            /************* Updating User Tokens **************/
                            $user_data['User']['id'] = $user_id;
                            if(!empty($user_details['User']['credit_tokens']))
                            {
                                $user_data['User']['credit_tokens'] = $user_details['User']['credit_tokens'] + $package_details['Package']['credits'];
                            }
                            else
                            {
                                $user_data['User']['credit_tokens'] = $package_details['Package']['credits'];
                            }
                            $this->User->save($user_data);
                            
                            /*************** Saving Payment Details **************/
                            $payment_data['Payment']['userid'] = $user_id;
                            $payment_data['Payment']['packageid'] = $package_details['Package']['id'];
                            $payment_data['Payment']['amount'] = $package_details['Package']['amount'];
                            $payment_data['Payment']['credit_tokens'] = $package_details['Package']['credits'];
                            $payment_data['Payment']['chargeid'] = $charge->id;
                            $payment_data['Payment']['txnid'] = $charge->balance_transaction;
                            $payment_data['Payment']['type'] = 'stripe';
                            $payment_data['Payment']['status'] = true;
                            $this->Payment->save($payment_data);
                            
                            /************** Saving Token Status *************/
                            $token_data['Token']['userid'] = $user_id;
                            $token_data['Token']['before'] = (!empty($user_details['User']['credit_tokens'])?$user_details['User']['credit_tokens']:0);
                            $token_data['Token']['after'] = $user_data['User']['credit_tokens'];
                            $token_data['Token']['tokens'] = $package_details['Package']['credits'];
                            $token_data['Token']['type'] = 'credit';
                            $this->Token->save($token_data);
                        }
                        $rarray = array('status' => 'success', 'message' => 'Payment successfull','data' => $charge);
                    } catch(\Stripe\Error\Card $e) {
                        $rarray = array('status' => 'danger','message' => 'Invalid Card Details');
                    }
                }
                else
                {
                    try
                    {
                        $customer = \Stripe\Customer::create(array(
                                "email" => $user_details['User']['email'],
                                "description" => $user_details['User']['name'],
                                "source" => $token // obtained with Stripe.js
                              ));
                        if(!empty($customer))
                        {
                            /**************** Charging $1 *****************/
                            $charge = \Stripe\Charge::create(array(
                                "amount" => 100, // amount in cents, again
                                "currency" => "usd",
                                "customer" => $customer->id)
                            );
                            
                            /*************** Saving Payment Details **************/
                            $payment_data['Payment']['userid'] = $user_id;
                            $payment_data['Payment']['packageid'] = $package_details['Package']['id'];
                            $payment_data['Payment']['amount'] = (!empty($package_details['Package']['amount'])?$package_details['Package']['amount']:0);
                            $payment_data['Payment']['credit_tokens'] = (!empty($package_details['Package']['credits'])?$package_details['Package']['credits']:0);
                            $payment_data['Payment']['chargeid'] = $charge->id;
                            $payment_data['Payment']['txnid'] = $charge->balance_transaction;
                            $payment_data['Payment']['type'] = 'stripe';
                            $payment_data['Payment']['status'] = true;
                            $this->Payment->save($payment_data);
                            
                            /******************** Saving Stripe ID *****************/
                            $user_data['User']['id'] = $user_id;
                            $user_data['User']['stripe_id'] = $customer->id;
                            $this->User->save($user_data);
                        }
                        $rarray = array('status' => 'success', 'message' => 'Your card saved successfully.');
                    }
                    catch(\Stripe\Error\Card $e) {
                        $rarray = array('status' => 'danger','message' => 'Invalid Card Details');
                    }
                }
            }
            else
            {
                $rarray = array('status' => 'danger','message' => 'Invalid package');
            }
            echo json_encode($rarray);
            exit;

        }

	
}
